-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2022 at 03:43 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_undangan`
--

-- --------------------------------------------------------

--
-- Table structure for table `acara`
--

CREATE TABLE `acara` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tanggal_akad` varchar(50) NOT NULL,
  `jam_akad` varchar(50) NOT NULL,
  `tempat_akad` varchar(100) NOT NULL,
  `alamat_akad` text NOT NULL,
  `tanggal_resepsi` varchar(50) NOT NULL,
  `jam_resepsi` varchar(50) NOT NULL,
  `tempat_resepsi` varchar(100) NOT NULL,
  `alamat_resepsi` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acara`
--

INSERT INTO `acara` (`id`, `id_user`, `tanggal_akad`, `jam_akad`, `tempat_akad`, `alamat_akad`, `tanggal_resepsi`, `jam_resepsi`, `tempat_resepsi`, `alamat_resepsi`, `created_at`, `updated_at`) VALUES
(1, 1, '2022/01/14', '08.00 WIB', 'Dsn Munjul', 'Dsn. Munjul Kaler Rt.028 Rw.005 Ds.Curug Kec.Klari Kab.Karawang', '2022/01/14', '08.00 WIB', 'Dsn Munjul', 'Dsn. Munjul Kaler Rt.028 Rw.005 Ds.Curug Kec.Klari Kab.Karawang', '2022-01-02 06:46:37', '2022-01-02 13:46:37');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `email`, `nama_lengkap`, `created_at`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'begather.business@gmail.com', 'begather.id', '2020-08-26 21:38:43');

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `album` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `id_user`, `album`) VALUES
(1, 1, 'album1'),
(2, 1, 'album2'),
(3, 1, 'album3'),
(4, 1, 'album4'),
(5, 1, 'album5'),
(6, 1, 'album6'),
(7, 1, 'album7'),
(8, 1, 'album8'),
(9, 1, 'album9'),
(10, 1, 'album10');

-- --------------------------------------------------------

--
-- Table structure for table `cerita`
--

CREATE TABLE `cerita` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tanggal_cerita` varchar(50) NOT NULL,
  `judul_cerita` text NOT NULL,
  `isi_cerita` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `foto_cover` int(11) NOT NULL DEFAULT 0,
  `foto_pria` varchar(50) NOT NULL DEFAULT '0',
  `foto_wanita` varchar(50) NOT NULL DEFAULT '0',
  `maps` text DEFAULT NULL,
  `video` varchar(100) NOT NULL,
  `kunci` varchar(100) NOT NULL,
  `salam_pembuka` text NOT NULL,
  `quote` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`id`, `id_user`, `foto_cover`, `foto_pria`, `foto_wanita`, `maps`, `video`, `kunci`, `salam_pembuka`, `quote`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '0', '0', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15862.788204149749!2d107.2778722!3d-6.3034773!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x255602cda524ab18!2sResinda%20Hotel!5e0!3m2!1sid!2sid!4v1641395549010!5m2!1sid!2sid\" width=\"400\" height=\"300\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\"></iframe>', '', '9bccb585ef787e15ae4bdeb6ffda8306', 'Assalamu\'alaikum warahmatullahi wabarakatuh\nDengan memohon rahmat dan ridho Allah SWT, Kami akan menyelenggarakan resepsi pernikahan Putra-Putri kami :\n			', '\"Seindah-indah perhiasan adalah wanita sholehah dan semulia-mulia laki-laki adalah yang memuliakan wanita Seraya menengadahkan jemari dalam harapan kasih Ilahi teriring Asma Allah yang Maha Pengasih dan Maha Penyayang\" ', '2022-01-02 06:46:37', '2022-01-16 09:34:20');

-- --------------------------------------------------------

--
-- Table structure for table `dompet`
--

CREATE TABLE `dompet` (
  `id` int(11) NOT NULL,
  `bank` varchar(100) NOT NULL,
  `nomor` varchar(100) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dompet`
--

INSERT INTO `dompet` (`id`, `bank`, `nomor`, `nama`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'BCA', '081918191819', 'Fahmi Nurcahya', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `komen`
--

CREATE TABLE `komen` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_komentar` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi_komentar` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `kehadiran` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `komen`
--

INSERT INTO `komen` (`id`, `id_user`, `nama_komentar`, `isi_komentar`, `kehadiran`, `created_at`, `updated_at`) VALUES
(1, 1, 'Fahmi', 'Selamat Aldio', 1, '2022-01-02 06:59:30', '2022-01-02 13:59:30'),
(2, 1, 'Fahmi 2', 'Selamat Aldio', 0, '2022-01-02 06:59:57', '2022-01-02 13:59:57'),
(3, 1, 'fahmi', '', 1, '2022-01-16 01:37:09', '2022-01-16 08:37:09'),
(4, 1, 'fahmii', 'adadad', 1, '2022-01-16 01:37:28', '2022-01-16 08:37:28'),
(5, 1, 'adadad', 'adadad', 1, '2022-01-16 01:39:02', '2022-01-16 08:39:02'),
(6, 1, 'adadad', '', 1, '2022-01-16 01:39:28', '2022-01-16 08:39:28'),
(7, 1, 'aaa', 'aa', 1, '2022-01-16 01:40:24', '2022-01-16 08:40:24');

-- --------------------------------------------------------

--
-- Table structure for table `mempelai`
--

CREATE TABLE `mempelai` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_pria` varchar(50) NOT NULL,
  `nama_panggilan_pria` varchar(50) NOT NULL,
  `nama_ibu_pria` varchar(50) NOT NULL,
  `nama_ayah_pria` varchar(50) NOT NULL,
  `nama_wanita` varchar(50) NOT NULL,
  `nama_panggilan_wanita` varchar(50) NOT NULL,
  `nama_ibu_wanita` varchar(50) NOT NULL,
  `nama_ayah_wanita` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mempelai`
--

INSERT INTO `mempelai` (`id`, `id_user`, `nama_pria`, `nama_panggilan_pria`, `nama_ibu_pria`, `nama_ayah_pria`, `nama_wanita`, `nama_panggilan_wanita`, `nama_ibu_wanita`, `nama_ayah_wanita`, `created_at`, `updated_at`) VALUES
(1, 1, 'Muhamad Aldio Mataufani, S.Kom', 'Aldio', 'Ibu. Endah Jubaedah', 'Bpk. Ahmad Taopik Hadi Ismanto', 'Yanti Apriyani, S.M', 'Yanti', 'Ibu. Awis (Almh)', 'Bpk. Karnadi (Alm)', '2022-01-02 06:46:37', '2022-01-02 13:46:37');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `domain` varchar(50) NOT NULL,
  `theme` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `id_user`, `domain`, `theme`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'yantialdio', '1', 1, '2022-01-02 06:46:37', '2022-01-02 13:48:45');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `invoice` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_bank` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bukti` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `id_user`, `invoice`, `nama_lengkap`, `nama_bank`, `bukti`, `status`, `total`, `created_at`) VALUES
(1, 1, '2201180', 'Aldio', 'BRI', '2201180.png', 2, 0, '2022-01-02 06:46:37');

-- --------------------------------------------------------

--
-- Table structure for table `pengunjung`
--

CREATE TABLE `pengunjung` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_pengunjung` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addr` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengunjung`
--

INSERT INTO `pengunjung` (`id`, `id_user`, `nama_pengunjung`, `addr`, `created_at`, `updated_at`) VALUES
(1, 1, 'Unknown', '::1', '2022-01-02 06:48:48', '2022-01-02 13:48:48'),
(2, 1, 'Unknown', '::1', '2022-01-02 06:49:04', '2022-01-02 13:49:04'),
(3, 1, 'Unknown', '::1', '2022-01-02 06:56:40', '2022-01-02 13:56:40'),
(4, 1, 'Fahmi', '::1', '2022-01-02 06:58:04', '2022-01-02 13:58:04'),
(5, 1, 'Fahmi', '::1', '2022-01-02 06:59:23', '2022-01-02 13:59:23'),
(6, 1, 'Fahmi', '::1', '2022-01-02 06:59:43', '2022-01-02 13:59:43'),
(7, 1, 'Unknown', '::1', '2022-01-05 13:38:28', '2022-01-05 20:38:28'),
(8, 1, 'Unknown', '::1', '2022-01-05 13:38:35', '2022-01-05 20:38:35'),
(9, 1, 'Unknown', '::1', '2022-01-05 13:47:57', '2022-01-05 20:47:57'),
(10, 1, 'Unknown', '::1', '2022-01-05 14:01:37', '2022-01-05 21:01:37'),
(11, 1, 'Unknown', '::1', '2022-01-05 14:01:46', '2022-01-05 21:01:46'),
(12, 1, 'Unknown', '::1', '2022-01-05 14:02:08', '2022-01-05 21:02:08'),
(13, 1, 'Unknown', '::1', '2022-01-05 14:02:30', '2022-01-05 21:02:30'),
(14, 1, 'Unknown', '::1', '2022-01-05 14:05:44', '2022-01-05 21:05:44'),
(15, 1, 'Unknown', '::1', '2022-01-05 14:10:12', '2022-01-05 21:10:12'),
(16, 1, 'Unknown', '::1', '2022-01-05 14:10:24', '2022-01-05 21:10:24'),
(17, 1, 'Unknown', '::1', '2022-01-05 14:10:31', '2022-01-05 21:10:31'),
(18, 1, 'Unknown', '::1', '2022-01-05 14:10:46', '2022-01-05 21:10:46'),
(19, 1, 'Unknown', '::1', '2022-01-05 14:11:04', '2022-01-05 21:11:04'),
(20, 1, 'Unknown', '::1', '2022-01-05 14:11:45', '2022-01-05 21:11:45'),
(21, 1, 'Unknown', '::1', '2022-01-05 14:11:54', '2022-01-05 21:11:54'),
(22, 1, 'Unknown', '::1', '2022-01-05 14:12:16', '2022-01-05 21:12:16'),
(23, 1, 'Unknown', '::1', '2022-01-05 14:12:58', '2022-01-05 21:12:58'),
(24, 1, 'Unknown', '::1', '2022-01-05 14:15:16', '2022-01-05 21:15:16'),
(25, 1, 'imhaf', '::1', '2022-01-05 14:15:29', '2022-01-05 21:15:29'),
(26, 1, 'imhaf', '::1', '2022-01-05 14:18:40', '2022-01-05 21:18:40'),
(27, 1, 'imhaf', '::1', '2022-01-05 14:18:58', '2022-01-05 21:18:58'),
(28, 1, 'Unknown', '::1', '2022-01-05 14:19:09', '2022-01-05 21:19:09'),
(29, 1, 'Unknown', '::1', '2022-01-05 15:11:04', '2022-01-05 22:11:04'),
(30, 1, 'Unknown', '::1', '2022-01-05 15:11:16', '2022-01-05 22:11:16'),
(31, 1, 'Unknown', '::1', '2022-01-05 15:14:52', '2022-01-05 22:14:52'),
(32, 1, 'Unknown', '::1', '2022-01-08 00:41:43', '2022-01-08 07:41:43'),
(33, 1, 'Unknown', '::1', '2022-01-08 00:42:16', '2022-01-08 07:42:16'),
(34, 1, 'Unknown', '::1', '2022-01-08 00:43:51', '2022-01-08 07:43:51'),
(35, 1, 'Unknown', '::1', '2022-01-08 00:49:42', '2022-01-08 07:49:42'),
(36, 1, 'Unknown', '::1', '2022-01-08 00:50:42', '2022-01-08 07:50:42'),
(37, 1, 'Unknown', '::1', '2022-01-08 00:53:42', '2022-01-08 07:53:42'),
(38, 1, 'Unknown', '::1', '2022-01-08 00:55:14', '2022-01-08 07:55:14'),
(39, 1, 'Unknown', '::1', '2022-01-08 06:17:33', '2022-01-08 13:17:33'),
(40, 1, 'Unknown', '::1', '2022-01-08 06:22:52', '2022-01-08 13:22:52'),
(41, 1, 'Unknown', '::1', '2022-01-08 06:27:04', '2022-01-08 13:27:04'),
(42, 1, 'Unknown', '::1', '2022-01-08 06:40:09', '2022-01-08 13:40:09'),
(43, 1, 'Unknown', '::1', '2022-01-08 06:40:37', '2022-01-08 13:40:37'),
(44, 1, 'Unknown', '::1', '2022-01-08 06:41:06', '2022-01-08 13:41:06'),
(45, 1, 'Unknown', '::1', '2022-01-08 06:43:09', '2022-01-08 13:43:09'),
(46, 1, 'Unknown', '::1', '2022-01-08 06:43:23', '2022-01-08 13:43:23'),
(47, 1, 'Unknown', '::1', '2022-01-08 06:43:54', '2022-01-08 13:43:54'),
(48, 1, 'Unknown', '::1', '2022-01-08 06:45:51', '2022-01-08 13:45:51'),
(49, 1, 'Unknown', '::1', '2022-01-08 06:46:30', '2022-01-08 13:46:30'),
(50, 1, 'Unknown', '::1', '2022-01-08 06:46:44', '2022-01-08 13:46:44'),
(51, 1, 'Unknown', '::1', '2022-01-08 06:46:59', '2022-01-08 13:46:59'),
(52, 1, 'Unknown', '::1', '2022-01-08 07:02:37', '2022-01-08 14:02:37'),
(53, 1, 'Unknown', '::1', '2022-01-08 07:05:14', '2022-01-08 14:05:14'),
(54, 1, 'Unknown', '::1', '2022-01-08 07:05:44', '2022-01-08 14:05:44'),
(55, 1, 'Unknown', '::1', '2022-01-08 07:06:02', '2022-01-08 14:06:02'),
(56, 1, 'Unknown', '::1', '2022-01-08 07:06:16', '2022-01-08 14:06:16'),
(57, 1, 'Unknown', '::1', '2022-01-08 07:11:15', '2022-01-08 14:11:15'),
(58, 1, 'Unknown', '::1', '2022-01-08 07:12:38', '2022-01-08 14:12:38'),
(59, 1, 'Unknown', '::1', '2022-01-08 07:13:13', '2022-01-08 14:13:13'),
(60, 1, 'Unknown', '::1', '2022-01-08 07:13:32', '2022-01-08 14:13:32'),
(61, 1, 'Unknown', '::1', '2022-01-08 07:13:50', '2022-01-08 14:13:50'),
(62, 1, 'Unknown', '::1', '2022-01-08 07:14:05', '2022-01-08 14:14:05'),
(63, 1, 'Unknown', '::1', '2022-01-08 07:15:25', '2022-01-08 14:15:25'),
(64, 1, 'Unknown', '::1', '2022-01-08 07:15:55', '2022-01-08 14:15:55'),
(65, 1, 'Unknown', '::1', '2022-01-08 07:17:30', '2022-01-08 14:17:30'),
(66, 1, 'Unknown', '::1', '2022-01-08 07:21:10', '2022-01-08 14:21:10'),
(67, 1, 'Unknown', '::1', '2022-01-08 07:21:35', '2022-01-08 14:21:35'),
(68, 1, 'Unknown', '::1', '2022-01-08 07:21:37', '2022-01-08 14:21:37'),
(69, 1, 'Unknown', '::1', '2022-01-08 07:21:48', '2022-01-08 14:21:48'),
(70, 1, 'Unknown', '::1', '2022-01-08 07:23:17', '2022-01-08 14:23:17'),
(71, 1, 'Unknown', '::1', '2022-01-08 07:28:35', '2022-01-08 14:28:35'),
(72, 1, 'Unknown', '::1', '2022-01-08 07:29:41', '2022-01-08 14:29:41'),
(73, 1, 'Unknown', '::1', '2022-01-08 07:29:57', '2022-01-08 14:29:57'),
(74, 1, 'Unknown', '::1', '2022-01-08 07:30:08', '2022-01-08 14:30:08'),
(75, 1, 'Unknown', '::1', '2022-01-08 07:36:18', '2022-01-08 14:36:18'),
(76, 1, 'Unknown', '::1', '2022-01-08 07:37:16', '2022-01-08 14:37:16'),
(77, 1, 'Unknown', '::1', '2022-01-08 07:37:47', '2022-01-08 14:37:47'),
(78, 1, 'Unknown', '::1', '2022-01-08 07:40:08', '2022-01-08 14:40:08'),
(79, 1, 'Unknown', '::1', '2022-01-08 07:41:36', '2022-01-08 14:41:36'),
(80, 1, 'Unknown', '::1', '2022-01-08 07:41:59', '2022-01-08 14:41:59'),
(81, 1, 'Unknown', '::1', '2022-01-08 10:01:13', '2022-01-08 17:01:13'),
(82, 1, 'Unknown', '::1', '2022-01-15 10:11:00', '2022-01-15 17:11:00'),
(83, 1, 'Unknown', '::1', '2022-01-15 10:11:12', '2022-01-15 17:11:12'),
(84, 1, 'Unknown', '::1', '2022-01-15 10:12:28', '2022-01-15 17:12:28'),
(85, 1, 'Unknown', '::1', '2022-01-15 10:14:10', '2022-01-15 17:14:10'),
(86, 1, 'Unknown', '::1', '2022-01-15 10:14:29', '2022-01-15 17:14:29'),
(87, 1, 'Unknown', '::1', '2022-01-15 10:16:26', '2022-01-15 17:16:26'),
(88, 1, 'Unknown', '::1', '2022-01-15 10:17:06', '2022-01-15 17:17:06'),
(89, 1, 'Unknown', '::1', '2022-01-15 10:17:44', '2022-01-15 17:17:44'),
(90, 1, 'Unknown', '::1', '2022-01-15 10:23:24', '2022-01-15 17:23:24'),
(91, 1, 'Unknown', '::1', '2022-01-15 10:23:43', '2022-01-15 17:23:43'),
(92, 1, 'Unknown', '::1', '2022-01-15 10:24:14', '2022-01-15 17:24:14'),
(93, 1, 'Unknown', '::1', '2022-01-15 10:24:25', '2022-01-15 17:24:25'),
(94, 1, 'Unknown', '::1', '2022-01-15 10:28:04', '2022-01-15 17:28:04'),
(95, 1, 'Unknown', '::1', '2022-01-15 10:29:32', '2022-01-15 17:29:32'),
(96, 1, 'Unknown', '::1', '2022-01-15 10:35:05', '2022-01-15 17:35:05'),
(97, 1, 'Unknown', '::1', '2022-01-15 10:35:29', '2022-01-15 17:35:29'),
(98, 1, 'Unknown', '::1', '2022-01-15 10:38:18', '2022-01-15 17:38:18'),
(99, 1, 'Unknown', '::1', '2022-01-15 10:39:00', '2022-01-15 17:39:00'),
(100, 1, 'Unknown', '::1', '2022-01-15 10:39:11', '2022-01-15 17:39:11'),
(101, 1, 'Unknown', '::1', '2022-01-15 10:39:56', '2022-01-15 17:39:56'),
(102, 1, 'Unknown', '::1', '2022-01-15 10:40:04', '2022-01-15 17:40:04'),
(103, 1, 'Unknown', '::1', '2022-01-15 10:40:40', '2022-01-15 17:40:40'),
(104, 1, 'Unknown', '::1', '2022-01-15 10:40:53', '2022-01-15 17:40:53'),
(105, 1, 'Unknown', '::1', '2022-01-15 10:42:56', '2022-01-15 17:42:56'),
(106, 1, 'Unknown', '::1', '2022-01-15 10:43:13', '2022-01-15 17:43:13'),
(107, 1, 'Unknown', '::1', '2022-01-15 10:44:11', '2022-01-15 17:44:11'),
(108, 1, 'Unknown', '::1', '2022-01-15 10:52:20', '2022-01-15 17:52:20'),
(109, 1, 'Unknown', '::1', '2022-01-15 10:53:20', '2022-01-15 17:53:20'),
(110, 1, 'Unknown', '::1', '2022-01-15 10:53:43', '2022-01-15 17:53:43'),
(111, 1, 'Unknown', '::1', '2022-01-15 10:53:56', '2022-01-15 17:53:56'),
(112, 1, 'Unknown', '::1', '2022-01-15 10:54:20', '2022-01-15 17:54:20'),
(113, 1, 'Unknown', '::1', '2022-01-15 10:55:34', '2022-01-15 17:55:34'),
(114, 1, 'Unknown', '::1', '2022-01-15 10:55:56', '2022-01-15 17:55:56'),
(115, 1, 'Unknown', '::1', '2022-01-15 10:56:06', '2022-01-15 17:56:06'),
(116, 1, 'Unknown', '::1', '2022-01-15 10:56:23', '2022-01-15 17:56:23'),
(117, 1, 'Unknown', '::1', '2022-01-15 10:56:57', '2022-01-15 17:56:57'),
(118, 1, 'Unknown', '::1', '2022-01-15 10:57:38', '2022-01-15 17:57:38'),
(119, 1, 'Unknown', '::1', '2022-01-15 10:58:37', '2022-01-15 17:58:37'),
(120, 1, 'Unknown', '::1', '2022-01-15 10:58:51', '2022-01-15 17:58:51'),
(121, 1, 'Unknown', '::1', '2022-01-15 10:59:29', '2022-01-15 17:59:29'),
(122, 1, 'Unknown', '::1', '2022-01-15 10:59:42', '2022-01-15 17:59:42'),
(123, 1, 'Unknown', '::1', '2022-01-15 10:59:53', '2022-01-15 17:59:53'),
(124, 1, 'Unknown', '::1', '2022-01-15 11:00:42', '2022-01-15 18:00:42'),
(125, 1, 'Unknown', '::1', '2022-01-15 11:02:56', '2022-01-15 18:02:56'),
(126, 1, 'Unknown', '::1', '2022-01-15 11:03:27', '2022-01-15 18:03:27'),
(127, 1, 'Unknown', '::1', '2022-01-15 11:40:31', '2022-01-15 18:40:31'),
(128, 1, 'Unknown', '::1', '2022-01-16 00:44:14', '2022-01-16 07:44:14'),
(129, 1, 'Unknown', '::1', '2022-01-16 00:44:29', '2022-01-16 07:44:29'),
(130, 1, 'Unknown', '::1', '2022-01-16 00:46:28', '2022-01-16 07:46:28'),
(131, 1, 'Unknown', '::1', '2022-01-16 00:47:09', '2022-01-16 07:47:09'),
(132, 1, 'Unknown', '::1', '2022-01-16 00:47:22', '2022-01-16 07:47:22'),
(133, 1, 'Unknown', '::1', '2022-01-16 00:47:31', '2022-01-16 07:47:31'),
(134, 1, 'Unknown', '::1', '2022-01-16 00:48:44', '2022-01-16 07:48:44'),
(135, 1, 'Unknown', '::1', '2022-01-16 00:50:06', '2022-01-16 07:50:06'),
(136, 1, 'Unknown', '::1', '2022-01-16 00:50:35', '2022-01-16 07:50:35'),
(137, 1, 'Unknown', '::1', '2022-01-16 00:51:15', '2022-01-16 07:51:15'),
(138, 1, 'Unknown', '::1', '2022-01-16 00:51:34', '2022-01-16 07:51:34'),
(139, 1, 'Unknown', '::1', '2022-01-16 00:51:43', '2022-01-16 07:51:43'),
(140, 1, 'Unknown', '::1', '2022-01-16 00:51:55', '2022-01-16 07:51:55'),
(141, 1, 'Unknown', '::1', '2022-01-16 00:52:06', '2022-01-16 07:52:06'),
(142, 1, 'Unknown', '::1', '2022-01-16 00:52:34', '2022-01-16 07:52:34'),
(143, 1, 'Unknown', '::1', '2022-01-16 00:52:41', '2022-01-16 07:52:41'),
(144, 1, 'Unknown', '::1', '2022-01-16 00:52:53', '2022-01-16 07:52:53'),
(145, 1, 'Unknown', '::1', '2022-01-16 00:53:17', '2022-01-16 07:53:17'),
(146, 1, 'Unknown', '::1', '2022-01-16 00:53:34', '2022-01-16 07:53:34'),
(147, 1, 'Unknown', '::1', '2022-01-16 00:55:10', '2022-01-16 07:55:10'),
(148, 1, 'Unknown', '::1', '2022-01-16 00:55:33', '2022-01-16 07:55:33'),
(149, 1, 'Unknown', '::1', '2022-01-16 00:56:01', '2022-01-16 07:56:01'),
(150, 1, 'Unknown', '::1', '2022-01-16 00:56:16', '2022-01-16 07:56:16'),
(151, 1, 'Unknown', '::1', '2022-01-16 00:56:29', '2022-01-16 07:56:29'),
(152, 1, 'Unknown', '::1', '2022-01-16 00:56:42', '2022-01-16 07:56:42'),
(153, 1, 'Unknown', '::1', '2022-01-16 00:57:01', '2022-01-16 07:57:01'),
(154, 1, 'Unknown', '::1', '2022-01-16 00:57:32', '2022-01-16 07:57:32'),
(155, 1, 'Unknown', '::1', '2022-01-16 00:57:46', '2022-01-16 07:57:46'),
(156, 1, 'Unknown', '::1', '2022-01-16 00:58:07', '2022-01-16 07:58:07'),
(157, 1, 'Unknown', '::1', '2022-01-16 01:00:46', '2022-01-16 08:00:46'),
(158, 1, 'Unknown', '::1', '2022-01-16 01:01:16', '2022-01-16 08:01:16'),
(159, 1, 'Unknown', '::1', '2022-01-16 01:01:36', '2022-01-16 08:01:36'),
(160, 1, 'Unknown', '::1', '2022-01-16 01:01:57', '2022-01-16 08:01:57'),
(161, 1, 'Unknown', '::1', '2022-01-16 01:02:28', '2022-01-16 08:02:28'),
(162, 1, 'Unknown', '::1', '2022-01-16 01:02:46', '2022-01-16 08:02:46'),
(163, 1, 'Unknown', '::1', '2022-01-16 01:03:05', '2022-01-16 08:03:05'),
(164, 1, 'Unknown', '::1', '2022-01-16 01:03:16', '2022-01-16 08:03:16'),
(165, 1, 'Unknown', '::1', '2022-01-16 01:03:27', '2022-01-16 08:03:27'),
(166, 1, 'Unknown', '::1', '2022-01-16 01:03:43', '2022-01-16 08:03:43'),
(167, 1, 'Unknown', '::1', '2022-01-16 01:03:55', '2022-01-16 08:03:55'),
(168, 1, 'Unknown', '::1', '2022-01-16 01:04:04', '2022-01-16 08:04:04'),
(169, 1, 'Unknown', '::1', '2022-01-16 01:04:14', '2022-01-16 08:04:14'),
(170, 1, 'Unknown', '::1', '2022-01-16 01:04:35', '2022-01-16 08:04:35'),
(171, 1, 'Unknown', '::1', '2022-01-16 01:04:45', '2022-01-16 08:04:45'),
(172, 1, 'Unknown', '::1', '2022-01-16 01:04:58', '2022-01-16 08:04:58'),
(173, 1, 'Unknown', '::1', '2022-01-16 01:05:33', '2022-01-16 08:05:33'),
(174, 1, 'Unknown', '::1', '2022-01-16 01:06:05', '2022-01-16 08:06:05'),
(175, 1, 'Unknown', '::1', '2022-01-16 01:16:31', '2022-01-16 08:16:31'),
(176, 1, 'Unknown', '::1', '2022-01-16 01:17:06', '2022-01-16 08:17:06'),
(177, 1, 'Unknown', '::1', '2022-01-16 01:18:15', '2022-01-16 08:18:15'),
(178, 1, 'Unknown', '::1', '2022-01-16 01:20:17', '2022-01-16 08:20:17'),
(179, 1, 'Unknown', '::1', '2022-01-16 01:21:07', '2022-01-16 08:21:07'),
(180, 1, 'Unknown', '::1', '2022-01-16 01:21:33', '2022-01-16 08:21:33'),
(181, 1, 'Unknown', '::1', '2022-01-16 01:21:51', '2022-01-16 08:21:51'),
(182, 1, 'Unknown', '::1', '2022-01-16 01:24:00', '2022-01-16 08:24:00'),
(183, 1, 'Unknown', '::1', '2022-01-16 01:24:34', '2022-01-16 08:24:34'),
(184, 1, 'Unknown', '::1', '2022-01-16 01:26:27', '2022-01-16 08:26:27'),
(185, 1, 'Unknown', '::1', '2022-01-16 01:27:32', '2022-01-16 08:27:32'),
(186, 1, 'Unknown', '::1', '2022-01-16 01:27:56', '2022-01-16 08:27:56'),
(187, 1, 'Unknown', '::1', '2022-01-16 01:28:51', '2022-01-16 08:28:51'),
(188, 1, 'Unknown', '::1', '2022-01-16 01:29:43', '2022-01-16 08:29:43'),
(189, 1, 'Unknown', '::1', '2022-01-16 01:36:17', '2022-01-16 08:36:17'),
(190, 1, 'Unknown', '::1', '2022-01-16 01:36:53', '2022-01-16 08:36:53'),
(191, 1, 'Unknown', '::1', '2022-01-16 01:37:16', '2022-01-16 08:37:16'),
(192, 1, 'Unknown', '::1', '2022-01-16 01:37:30', '2022-01-16 08:37:30'),
(193, 1, 'Unknown', '::1', '2022-01-16 01:38:49', '2022-01-16 08:38:49'),
(194, 1, 'Unknown', '::1', '2022-01-16 01:39:10', '2022-01-16 08:39:10'),
(195, 1, 'Unknown', '::1', '2022-01-16 01:39:49', '2022-01-16 08:39:49'),
(196, 1, 'Unknown', '::1', '2022-01-16 01:40:08', '2022-01-16 08:40:08'),
(197, 1, 'Unknown', '::1', '2022-01-16 01:40:35', '2022-01-16 08:40:35'),
(198, 1, 'Unknown', '::1', '2022-01-16 02:28:37', '2022-01-16 09:28:37'),
(199, 1, 'Unknown', '::1', '2022-01-16 02:29:29', '2022-01-16 09:29:29'),
(200, 1, 'Unknown', '::1', '2022-01-16 02:30:13', '2022-01-16 09:30:13'),
(201, 1, 'Unknown', '::1', '2022-01-16 02:31:28', '2022-01-16 09:31:28'),
(202, 1, 'Unknown', '::1', '2022-01-16 02:33:30', '2022-01-16 09:33:30'),
(203, 1, 'Unknown', '::1', '2022-01-16 02:34:28', '2022-01-16 09:34:28'),
(204, 1, 'Unknown', '::1', '2022-01-16 02:35:17', '2022-01-16 09:35:17'),
(205, 1, 'Unknown', '::1', '2022-01-16 02:35:44', '2022-01-16 09:35:44'),
(206, 1, 'Unknown', '::1', '2022-01-16 02:36:07', '2022-01-16 09:36:07'),
(207, 1, 'Unknown', '::1', '2022-01-16 02:40:23', '2022-01-16 09:40:23'),
(208, 1, 'Unknown', '::1', '2022-01-16 02:40:46', '2022-01-16 09:40:46'),
(209, 1, 'Unknown', '::1', '2022-01-16 02:41:10', '2022-01-16 09:41:10'),
(210, 1, 'Unknown', '::1', '2022-01-16 02:41:43', '2022-01-16 09:41:43');

-- --------------------------------------------------------

--
-- Table structure for table `rules`
--

CREATE TABLE `rules` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `sampul` int(11) NOT NULL,
  `mempelai` int(11) NOT NULL,
  `acara` int(11) NOT NULL,
  `komen` int(11) NOT NULL,
  `gallery` int(11) NOT NULL,
  `cerita` int(11) NOT NULL,
  `lokasi` int(11) NOT NULL,
  `akad` int(11) NOT NULL DEFAULT 1,
  `resepsi` int(11) NOT NULL DEFAULT 1,
  `quote` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rules`
--

INSERT INTO `rules` (`id`, `id_user`, `sampul`, `mempelai`, `acara`, `komen`, `gallery`, `cerita`, `lokasi`, `akad`, `resepsi`, `quote`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2022-01-02 06:46:37', '2022-01-16 09:41:38');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `harga` double NOT NULL,
  `nama_bank` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `norek` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_pemilik` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `harga`, `nama_bank`, `norek`, `nama_pemilik`, `img`, `created_at`) VALUES
(1, 0, '', '1234567890', 'begether.id', '', '2022-01-01 05:18:41');

-- --------------------------------------------------------

--
-- Table structure for table `tamu`
--

CREATE TABLE `tamu` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_tamu` varchar(255) NOT NULL,
  `url_tamu` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tamu`
--

INSERT INTO `tamu` (`id`, `id_user`, `nama_tamu`, `url_tamu`) VALUES
(1, 1, 'Fahmi', 'https://begather.id/yantialdio?to=Fahmi'),
(2, 1, 'Nurcahya aa', 'https://begather.id/yantialdio?to=Nurcahya+aa');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` int(11) NOT NULL,
  `nama_theme` varchar(50) NOT NULL,
  `kode_theme` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `nama_theme`, `kode_theme`, `created_at`, `updated_at`) VALUES
(1, 'BG001', 'BG001', '2021-12-26 09:09:04', '2021-12-26 16:09:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `hp` varchar(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_unik` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `hp`, `email`, `username`, `password`, `id_unik`, `created_at`, `updated_at`) VALUES
(1, '081234567890', 'aldio@gmail.com', 'aldio@gmail.com', '081234567890', '2201180', '2022-01-02 06:46:36', '2022-01-02 13:46:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acara`
--
ALTER TABLE `acara`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cerita`
--
ALTER TABLE `cerita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dompet`
--
ALTER TABLE `dompet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_bank_id` (`user_id`);

--
-- Indexes for table `komen`
--
ALTER TABLE `komen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mempelai`
--
ALTER TABLE `mempelai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengunjung`
--
ALTER TABLE `pengunjung`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rules`
--
ALTER TABLE `rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tamu`
--
ALTER TABLE `tamu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acara`
--
ALTER TABLE `acara`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cerita`
--
ALTER TABLE `cerita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dompet`
--
ALTER TABLE `dompet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `komen`
--
ALTER TABLE `komen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `mempelai`
--
ALTER TABLE `mempelai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pengunjung`
--
ALTER TABLE `pengunjung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;

--
-- AUTO_INCREMENT for table `rules`
--
ALTER TABLE `rules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tamu`
--
ALTER TABLE `tamu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dompet`
--
ALTER TABLE `dompet`
  ADD CONSTRAINT `user_id_bank_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
