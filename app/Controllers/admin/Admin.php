<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;
use CodeIgniter\Controller;
use App\Models\admin\AdminModel;
use App\Models\pengguna\OrderModel;

class Admin extends BaseController
{
    protected $request;

    public function __construct()
    {
        //mengisi variable global dengan data
        $this->session = session();
        $this->AdminModel = new AdminModel();
        $this->order = new OrderModel();  //load OrderModel
        $this->db  = \Config\Database::connect(); //untuk melakukan CRUD ke databse


        $this->request = \Config\Services::request(); //memanggil class request
        $this->uri = $this->request->uri; //class request digunakan untuk request uri/url
        // $this->session->set('uname_admin','mantab');
        // $this->session->set('id_admin','1');
    }

    public function index()
    {
        echo 'URL Tidak Valid / Kurang Lengkap';
    }

    public function login()
    {
        if (session()->has('odAdmin')) {
            return redirect()->to(base_url('admin/dashboard'));
        }
        $data['title'] = 'Selamat Datang';
        $data['view'] = 'admin/auth/login';
        return view('admin/auth/layout', $data);
    }

    public function dashboard()
    {
        if (!session()->has('odAdmin')) {
            return redirect()->to(base_url('login'));
        }

        $data['view'] = 'admin/index';
        $data['join'] = $this->AdminModel->get_all_join();
        $data['totalPending'] = $this->AdminModel->get_total_pending();
        $data['totalPemasukan'] = $this->AdminModel->get_total_pemasukan();
        $data['title'] = 'Admin Dashboard';
        $data['view'] = 'admin/dashboard';
        return view('admin/layout', $data);
    }

    public function do_auth()
    {

        $data['email'] = $this->request->getPost('email');
        $data['password'] = md5($this->request->getPost('password'));
        $hasil = $this->AdminModel->get_admin($data);

        if (count($hasil) > 0) {
            // set session
            $sess_data = array('odAdmin' => TRUE, 'uname_admin' => $hasil[0]->username, 'id_admin' => $hasil[0]->id);
            $this->session->set($sess_data);
            return redirect()->to(base_url('admin/dashboard'));
            exit();
        } else {
            $this->session->setFlashdata('errors', ['Password Salah']);
            return redirect()->to(base_url('/login'));
        }
    }

    public function do_unauth()
    {

        $this->session->destroy();
        return redirect()->to(base_url('/login'));
    }

    public function pengguna()
    {
        $data['title'] = 'Data Pengguna';
        $data['view'] = 'admin/pengguna/pengguna';
        $data['join'] = $this->AdminModel->get_all_join();
        return view('admin/layout', $data);
    }

    public function editPengguna()
    {
        $data['title'] = 'Data Pengguna';
        $data['view'] = 'admin/pengguna/pengguna';
        $data['join'] = $this->AdminModel->get_all_join();
        return view('admin/layout', $data);
    }



    public function do_konfirmasi()
    {

        $id = $this->request->getPost('id');

        $update = $this->AdminModel->konfirmasi_user($id);
        if ($update) {
            echo 'sukses';
        } else {
            echo 'gagal';
        }
    }

    public function do_user()
    {

        $data['id'] = $this->request->getPost('id_user');
        $hasil = $this->AdminModel->get_pengguna($data);

        if (count($hasil) > 0) {
            // set session
            $sess_data = array('odUser' => TRUE, 'uname' => $hasil[0]->username, 'id' => $hasil[0]->id);
            $this->session->set($sess_data);
            return redirect()->to(base_url('admin/dashboard'));
            exit();
        } else {
            return redirect()->to(base_url('admin/dashboard'));
        }
    }

    public function tambahPengguna()
    {

        $domain = $this->request->getPost('domain');
        $email = $this->request->getPost('email');
        $password = $this->request->getPost('hp');
        $hp = $this->request->getPost('hp');

        $cekDomain = $this->order->cek_domain($domain);
        if (!empty($cekDomain->getResult())) {
            $arr = array('error' => true, 'message' => 'Nama Domain Sudah Terpakai');
            return json_encode($arr);
        }
        $dataUser = [
            'email' => $email,
            'hp' => $hp,
            'username' => $email,
            'password' => $password,
            'id_unik' => '',
        ];
        $saveUser = $this->order->save_user($dataUser);
        if (!$saveUser) {
            $arr = array('error' => true, 'message' => 'Terjadi Kesalahan');
            return json_encode($arr);
        }

        $id_user = $this->db->insertID(); //ambil id user
        $today = date('ym');
        $kode = $today . $id_user . rand(10, 99); //dijadikan invoice sekaligus kode unik user. Formatnya ( 2 digit tahun, 2 digit bulan, id user, random 2 angka)
        $this->order->update_kode($kode, $id_user);


        $dataOrder = [
            'id_user' => $id_user,
            'domain' => $domain,
            'theme' => '1',
            'status' => '0'
        ];


        //mempelai
        $nama_lengkap_pria = $this->request->getPost('nama_lengkap_pria');
        $nama_panggilan_pria = $this->request->getPost('nama_panggilan_pria');
        $nama_ibu_pria = $this->request->getPost('nama_ibu_pria');
        $nama_ayah_pria = $this->request->getPost('nama_ayah_pria');

        $nama_lengkap_wanita = $this->request->getPost('nama_lengkap_wanita');
        $nama_panggilan_wanita = $this->request->getPost('nama_panggilan_wanita');
        $nama_ibu_wanita = $this->request->getPost('nama_ibu_wanita');
        $nama_ayah_wanita = $this->request->getPost('nama_ayah_wanita');

        $dataMempelai = [
            'id_user' => $id_user,
            'nama_pria' => $nama_lengkap_pria,
            'nama_panggilan_pria' => $nama_panggilan_pria,
            'nama_ibu_pria' => $nama_ibu_pria,
            'nama_ayah_pria' => $nama_ayah_pria,
            'nama_wanita' => $nama_lengkap_wanita,
            'nama_panggilan_wanita' => $nama_panggilan_wanita,
            'nama_ibu_wanita' => $nama_ibu_wanita,
            'nama_ayah_wanita' => $nama_ayah_wanita,
        ];


        $tanggal_akad = $this->request->getPost('tanggal_akad');
        $waktu_akad = $this->request->getPost('waktu_akad');
        $lokasi_akad = $this->request->getPost('lokasi_akad');
        $alamat_akad = $this->request->getPost('alamat_akad');

        $tanggal_resepsi = $this->request->getPost('tanggal_resepsi');
        $waktu_resepsi = $this->request->getPost('waktu_resepsi');
        $lokasi_resepsi = $this->request->getPost('lokasi_resepsi');
        $alamat_resepsi = $this->request->getPost('alamat_resepsi');

        $dataAcara = [
            'id_user' => $id_user,
            'tanggal_akad' => $tanggal_akad,
            'jam_akad' => $waktu_akad,
            'tempat_akad' => $lokasi_akad,
            'alamat_akad' => $alamat_akad,
            'tanggal_resepsi' => $tanggal_resepsi,
            'jam_resepsi' => $waktu_resepsi,
            'tempat_resepsi' => $lokasi_resepsi,
            'alamat_resepsi' => $alamat_resepsi
        ];

        $video = '';

        $foto_pria = "0";
        $foto_wanita = "0";

        $kunci = md5($id_user . $domain);

        $dataData = [
            'id_user' => $id_user,
            'foto_pria' => $foto_pria,
            'foto_wanita' => $foto_wanita,
            'video' => $video,
            'kunci' => $kunci,
            'maps' => '',
            'salam_pembuka' => "Assalamu'alaikum warahmatullahi wabarakatuh\nDengan memohon rahmat dan ridho Allah SWT, Kami akan menyelenggarakan resepsi pernikahan Putra-Putri kami :
			",
        ];

        //rule
        $dataRules = [
            'id_user' => $id_user,
            'sampul' => 1,
            'mempelai' => 1,
            'acara' => 1,
            'komen' => 1,
            'gallery' => 1,
            'cerita' => 1,
            'lokasi' => 1,
        ];

        $getSetting = $this->AdminModel->get_setting();
        $harga = $getSetting[0]->harga;

        //pembayaran
        $dataPembayaran = [
            'id_user' => $id_user,
            'invoice' => $kode,
            'status' => '0',
            'total' => $harga
        ];

        $savePembayaran = $this->order->save_pembayaran($dataPembayaran);
        $saveRules = $this->order->save_rules($dataRules);
        $saveData = $this->order->save_data($dataData);
        $saveAcara = $this->order->save_acara($dataAcara);
        $saveOrder = $this->order->save_order($dataOrder);
        $saveUser = $this->order->save_mempelai($dataMempelai);

        if ($saveUser) {
            $arr = array('error' => false, 'message' => 'Sukses');
            return json_encode($arr);
        } else {
            $arr = array('error' => true, 'message' => 'Gagal');
            return json_encode($arr);
        }
    }

    public function deletePengguna()
    {
        $id_user = $this->request->getPost('id');
        $prepareDelete = $this->AdminModel->prepare_delete($id_user);
        // dd($prepareDelete[0]->email);
        if (!$prepareDelete) {
            $arr = array('error' => true, 'message' => 'Gagal Saat Prepare');
            return json_encode($arr);
        }
        $dataCustomer = [
            'email' => $prepareDelete[0]->email,
            'no_hp' => $prepareDelete[0]->hp,
            'id_partner' => $prepareDelete[0]->id_partner,
            'tgl_order' => $prepareDelete[0]->created_at,
            'tgl_hapus' => date("Y-m-d H:i:s"),
            'kunci' => $prepareDelete[0]->kunci,
        ];
        $insertHist = $this->AdminModel->insert_cust_hist($dataCustomer);
        if (!$insertHist) {
            $arr = array('error' => true, 'message' => 'Gagal Saat Insert');
            return json_encode($arr);
        }

        $deleteUser = $this->AdminModel->delete_user($id_user);
        if ($deleteUser) {
            $arr = array('error' => false, 'message' => 'Sukses');
            return json_encode($arr);
        } else {
            $arr = array('error' => true, 'message' => 'Gagal');
            return json_encode($arr);
        }
    }

    public function template()
    {
        $data['title'] = 'Data Template';
        $data['view'] = 'admin/template/template';
        $data['template'] = $this->AdminModel->get_all_themes()->getResult();
        return view('admin/layout', $data);
    }

    public function setting()
    {
        $data['title'] = 'Setting';
        $data['view'] = 'admin/setting/setting';
        $data['setting'] = $this->AdminModel->get_setting()[0];
        $data['table_db'] = $this->AdminModel->count_all_row();

        return view('admin/layout', $data);
    }

    public function updateHarga()
    {
        $harga = $this->request->getPost('harga');
        $data = [
            "harga" => $harga
        ];
        $this->AdminModel->update_setting($data);
        $arr = array('error' => false, 'message' => 'Sukses');
        return json_encode($arr);
    }


    public function partner()
    {
        $data['title'] = 'Data Partner';
        $data['view'] = 'admin/partner/partner';
        $data['join'] = $this->AdminModel->get_all_partner();
        return view('admin/layout', $data);
    }

    public function getPaketPartner()
    {
        $data = $this->AdminModel->get_all_paket_partner();
        return json_encode($data);
    }


    public function editPartner()
    {
        $data['title'] = 'Data Partner';
        $data['view'] = 'admin/partner/partner';
        $data['join'] = $this->AdminModel->get_all_join();
        return view('admin/layout', $data);
    }

    public function tambahPartner()
    {
        $nama_partner = $this->request->getPost('nama_partner');
        $email = strtolower($this->request->getPost('email'));
        $password = $this->request->getPost('hp');
        $no_hp = $this->request->getPost('hp');
        $alamat = $this->request->getPost('alamat');
        $cekEmail = $this->AdminModel->cek_email($email);
        if (!empty($cekEmail->getResult())) {
            $arr = array('error' => true, 'message' => 'Email Sudah Terdaftar');
            return json_encode($arr);
        }
        $dataPartner = [
            'email' => $email,
            'no_hp' => $no_hp,
            'password' => $password,
            'nama_partner' => $nama_partner,
            'alamat_partner' => $alamat,
        ];

        $savePartner = $this->AdminModel->save_partner($dataPartner);
        if (!$savePartner) {
            $arr = array('error' => true, 'message' => 'Terjadi Kesalahan');
            return json_encode($arr);
        } else {
            $arr = array('error' => false, 'message' => 'Partner Telah Ditambahkan');
            return json_encode($arr);
        }
    }

    public function ubahStatusPartner()
    {
        $is_aktif = $this->request->getPost('is_aktif');
        $id = $this->request->getPost('id');

        $dataPartner = [
            'id' => $id,
            'is_aktif' => $is_aktif
        ];

        $ubahStatus = $this->AdminModel->ubah_status_partner($dataPartner);
        if (!$ubahStatus) {
            $arr = array('error' => true, 'message' => 'Terjadi Kesalahan');
            return json_encode($arr);
        } else {
            $arr = array('error' => false, 'message' => 'Status Telah Ubah');
            return json_encode($arr);
        }
    }

    public function deletePartner()
    {
        $id = $this->request->getPost('id');
        $deletePartner = $this->AdminModel->delete_partner($id);
        if ($deletePartner) {
            $arr = array('error' => false, 'message' => 'Sukses');
            return json_encode($arr);
        } else {
            $arr = array('error' => true, 'message' => 'Gagal');
            return json_encode($arr);
        }
    }

    public function deleteFolder()
    {
        $path = $this->request->getPost('path');
        if ($path != '' || $path == null) {
            $dir = 'assets/' . $path;
            if (!file_exists($dir)) {
                $arr = array('error' => true, 'message' => 'Folder tidak ditemukan');
                return json_encode($arr);
            }

            if (!is_dir($dir)) {
                $arr = array('error' => true, 'message' => 'Folder tidak ditemukan');
                return json_encode($arr);
            }


            foreach (scandir($dir) as $item) {

                if ($item == '.' || $item == '..') {
                    continue;
                }

                $dir2 = $dir . '/' . $item;
                if (is_dir($dir2)) {
                    foreach (scandir($dir2) as $item2) {
                        if ($item2 == '.' || $item2 == '..') {
                            continue;
                        }

                        unlink($dir2 . DIRECTORY_SEPARATOR . $item2);
                    }
                    rmdir($dir2);
                } else {
                    unlink($dir . DIRECTORY_SEPARATOR . $item);
                }

                // dd($dir . DIRECTORY_SEPARATOR . $item);
            }

            rmdir($dir);
            mkdir('assets/' . $path, 0777, true);
            $arr = array('error' => false, 'message' => 'Sukses');
            return json_encode($arr);
        }
    }


    public function truncateAllTable()
    {
        $table = TABLE_DB;
        foreach ($table as $table) {
            $this->AdminModel->truncate_table($table);
        }
        $arr = array('error' => false, 'message' => 'Sukses');
        return json_encode($arr);
    }

    public function truncateTable()
    {
        $table = $this->request->getPost('table');
        $this->AdminModel->truncate_table($table);
        $arr = array('error' => false, 'message' => 'Sukses');
        return json_encode($arr);
    }
}
