<?php

namespace App\Controllers\api;

use App\Controllers\BaseController;
use App\Models\history\CustomerModel;



class Job extends BaseController
{
    protected $request;

    public function __construct()
    {
        //mengisi variable global dengan data
        $this->CustomerModel = new CustomerModel();
        $this->request = \Config\Services::request(); //memanggil class request
        $this->uri = $this->request->uri; //class request digunakan untuk request uri/url
        // $this->session->set('uname_admin','mantab');
        // $this->session->set('id_admin','1');
    }

    public function index()
    {
        echo 'URL Tidak Valid / Kurang Lengkap';
    }


    public function jobDeleteFile()
    {
        header("Content-type: application/json");
        $data = $this->CustomerModel->get_data();
        if ($data) {
            foreach ($data as $data) {
                $this->deleteFIle($data->kunci);
            }
            $arr = array('error' => false, 'message' => 'Sukses Delete Data', 'data' => $data);
            echo json_encode($arr);
        } else {
            $arr = array('error' => true, 'message' => 'Data tidak ditemukan', 'data' => $data);
            echo json_encode($arr);
        }
        die;
    }

    public function deleteFIle($kunci)
    {

        $path = 'assets/users/' . $kunci;
        header("Content-type: application/json");

        if ($kunci == null || $kunci == "") {
            $arr = array('error' => true, 'message' => 'Folder tidak ditemukan', 'path' => $path);
            log_message('warning', json_encode($arr));
        }
        //cek folder e
        if (!file_exists($path)) {
            $arr = array('error' => true, 'message' => 'Folder tidak ditemukan', 'path' => $path);
            log_message('warning', json_encode($arr));
        } else {
            rmdir($path);
            $arr = array('error' => false, 'message' => 'Folder berhasil di delete', 'path' => $path);
            log_message('info', json_encode($arr));
        }
        return;
    }
}
