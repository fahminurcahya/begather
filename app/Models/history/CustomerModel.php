<?php

namespace App\Models\history;

use CodeIgniter\Model;

class CustomerModel extends Model
{
    public function __construct()
    {
        parent::__construct();
        $dbHist  = \Config\Database::connect("hist");
        $this->customer = $dbHist->table('customer');
    }

    public function get_data()
    {
        $builder = $this->customer;
        $where = "date(tgl_hapus) = CURDATE()- INTERVAL 1 DAY";
        $builder->where($where);
        $query = $builder->get();
        return $query->getResult();
    }
}
