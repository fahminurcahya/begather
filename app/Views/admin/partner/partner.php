<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Partner
            <small>begather.id</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.row -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Partner</h3>
                        <p style="color: orange;">Menu ini masih dalam tahap pengembangan</p>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div style="padding-bottom: 10px;">
                            <button id="tambahPartner" type="button" class="btn btn-success" onclick="return tambahPartner()" disabled><i class="fa fa-plus-circle"></i> Tambah Partner</button>
                        </div>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Partner</th>
                                    <th>Status</th>
                                    <th>Masa Aktif</th>
                                    <th>Domain Aktif</th>
                                    <th>Total Domain</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $limit = 0;
                                foreach ($join as $row) { ?>
                                    <?php $limit++; ?>
                                    <tr>
                                        <td><?= $row->nama_partner ?></td>
                                        <?php if ($row->is_aktif == '1') { ?>
                                            <td>Aktif</td>
                                        <?php } else { ?>
                                            <td>Tidak Aktif</td>
                                        <?php } ?>
                                        <td><?= $row->expired_date ?></td>
                                        <td>3</td>
                                        <td>10</td>
                                        <td>
                                            <div class="btn-group mb-1">

                                                <?php if ($row->is_aktif == '1') { ?>
                                                    <button type="button" class="btn btn-warning btn-sm" onclick="konfirmasiUbah(<?= $row->id ?>, 0)" disabled>
                                                        Nonaktifkan
                                                    </button> <?php } else { ?>
                                                    <button type="button" class="btn btn-success btn-sm" onclick="konfirmasiUbah(<?= $row->id ?>, 1)" disabled>
                                                        Aktifkan
                                                    </button> <?php } ?>
                                            </div>
                                            <div class="btn-group mb-1">
                                                <button type="button" id="btnHapus" class="konfirmasiHapus btn btn-danger btn-sm" onclick="konfirmasiDelete(<?= $row->id ?>)" disabled>
                                                    Hapus
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>


                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>


        <!-- Modal -->
        <div id="addPartner" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Tambah</h5>
                    </div>
                    <div id="addPartnerBody" class="modal-body">
                        <form id="create-new-partner" class="mb-3" autocomplete="off">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="nama_partner" class="control-label">Nama Partner</label>
                                    <input id="nama_partner" name="nama_partner" class="form-control" placeholder="nama partner" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="email" class="control-label">Email</label>
                                    <input id="email" type="email" name="email" class="form-control" placeholder="email" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="hp" class="control-label">Nomor HP</label>
                                    <input id="hp" name="hp" class="form-control" placeholder="Nomor Hp" onkeypress='validate(event)' autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="paket" class="control-label">Paket Partner</label>
                                    <select class="form-control form-select-sm" id="paket" name="paket" aria-label="Default select example">
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="alamat" class="control-label">Alamat</label>
                                    <textarea name="alamat" type="text" class="form-control" placeholder="Contoh : JL. Ahmad Yani No.1" required></textarea>
                                </div>
                            </div>
                            <button id="submitPartner" class="btn btn-primary" type="submit">
                                <span id="spinner" class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true" hidden></span>
                                Tambah
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="statusModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ubah Status</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Yakin ingin mengubah status patner?</p>
                        <input value="" type="hidden" id="idPartner">
                        <input value="" type="hidden" id="is_aktif">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" id="ubahStatus">Ubah Status</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal" id="deleteModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ubah Status</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Yakin ingin menghapus Partner?</p>
                        <input value="" type="hidden" id="idPartner">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" id="deletePartner">Hapus</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <script src="<?= base_url('assets/adminlte/bower_components/jquery/dist/jquery.min.js') ?>"></script>
        <script src="<?php echo base_url() ?>/assets/base/js/pikaday.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.all.min.js"></script>


        <!-- jQuery UI 1.11.4 -->
        <script>
            var arr_temp = new Array();

            function tambahPartner() {
                arr_temp = new Array()
                loadPaketPartner()
                $('#addPartner').modal('show');

            }

            function loadPaketPartner() {
                $.ajax({
                    url: "<?= base_url('admin/get_paket_partner') ?>",
                    method: "POST",
                    async: true,
                    dataType: 'json',
                    success: function($hasil) {
                        var i;
                        for (i = 0; i < $hasil.length; i++) {
                            arr_temp.push({
                                id: $hasil[i].id_paket,
                                descrip: $hasil[i].deskripsi
                            })
                        }
                        setPaketPartner(arr_temp)
                    },
                    error: function(error) {
                        alert('Sistem Error');
                    }
                });
                return false;

            }

            function setPaketPartner(arr) {
                var divOptProd = document.getElementById('paket');
                var htmlString = '<option value="" selected hidden disabled>Pilih Paket : </option>';
                if (arr.length > 0) {
                    for (i = 0; i < arr.length; i++) {
                        htmlString = htmlString + '<option value="' + arr[i]['id'] + '">' + arr[i]['descrip'] + '</option>';
                    }
                    divOptProd.innerHTML = htmlString;
                } else {
                    htmlString = htmlString + '<option value="" disabled>--- no available ---</option>'
                    divOptProd.innerHTML = htmlString;
                }
            }

            function validate(evt) {
                var theEvent = evt || window.event;

                // Handle paste
                if (theEvent.type === 'paste') {
                    key = event.clipboardData.getData('text/plain');
                } else {
                    // Handle key press
                    var key = theEvent.keyCode || theEvent.which;
                    key = String.fromCharCode(key);
                }
                var regex = /[0-9]|\./;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();
                }
            }

            $("#create-new-partner").submit(function() {
                let data = $("#create-new-partner").serialize();
                $.ajax({
                    url: "<?= base_url('admin/tambah_partner') ?>",
                    method: "POST",
                    data: data,
                    async: true,
                    dataType: 'json',
                    success: function($hasil) {
                        if (!$hasil.error) {
                            Swal.fire(
                                $hasil.message,
                                '',
                                'success'
                            )
                            location.reload();
                        } else {
                            alert($hasil.message)
                        }
                    },
                    error: function(error) {
                        alert('Sistem Error');
                    }
                });

                return false

            });

            function nospaces(t) {
                if (t.value.match(/\s/g)) {
                    t.value = t.value.replace(/\s/g, '');
                }
            }

            function konfirmasiUbah(id, is_aktif) {
                $('#statusModal').modal('show')
                $('.modal-body #idPartner').val(id);
                $('.modal-body #is_aktif').val(is_aktif);
            };

            $('#ubahStatus').on('click', function(event) {
                var id = $('#idPartner').val();
                var is_aktif = $('#is_aktif').val();
                $.ajax({
                    url: "<?= base_url('admin/ubah_status') ?>",
                    method: "POST",
                    data: {
                        id: id,
                        is_aktif: is_aktif

                    },
                    async: true,
                    dataType: 'json',
                    success: function($hasil) {
                        if (!$hasil.error) {
                            Swal.fire(
                                $hasil.message,
                                '',
                                'success'
                            )
                            location.reload();
                        } else {
                            alert($hasil.message)
                        }
                    }
                });
            });


            function konfirmasiDelete(id) {
                $('#deleteModal').modal('show')
                $('.modal-body #idPartner').val(id);
            };

            $('#deletePartner').on('click', function(event) {
                var id = $('#idPartner').val();
                $.ajax({
                    url: "<?= base_url('admin/delete_partner') ?>",
                    method: "POST",
                    data: {
                        id: id,
                    },
                    async: true,
                    dataType: 'json',
                    success: function($hasil) {
                        if (!$hasil.error) {
                            Swal.fire(
                                $hasil.message,
                                '',
                                'success'
                            )
                            location.reload();
                        } else {
                            alert($hasil.message)
                        }
                    }
                });
            });
        </script>

        <!-- /.row -->
    </section>
</div>