<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Template
            <small>begather.id</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.row -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Template</h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Tema</th>
                                    <th>Kode Tema</th>
                                    <th>Preview</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $limit = 0;
                                foreach ($template as $row) { ?>
                                    <?php $limit++; ?>
                                    <tr>
                                        <td><?= $limit ?></td>
                                        <td><?= $row->nama_theme ?></td>
                                        <td><?= $row->kode_theme ?></td>
                                        <td> <img alt="image" width="150px" src="<?php echo base_url() ?>/assets/themes/<?= $row->nama_theme ?>/preview.png">
                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>


                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
</div>
