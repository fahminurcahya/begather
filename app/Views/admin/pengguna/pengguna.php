<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pengguna
            <small>begather.id</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.row -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Pengguna</h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div style="padding-bottom: 10px;">
                            <button id="tambahPengguna" type="button" class="btn btn-success" onclick="return tambahPengguna()"><i class="fa fa-plus-circle"></i> Tambah Pengguna</button>
                        </div>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No Invoice</th>
                                    <th>Pengguna</th>
                                    <th>No Hp</th>
                                    <th>Domain</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $limit = 0;
                                foreach ($join as $row) { ?>
                                    <?php $limit++; ?>
                                    <tr>
                                        <td>#<?= $row->invoice ?></td>
                                        <td><?= $row->username ?></td>
                                        <td><?= $row->hp ?></td>
                                        <td><a href="<?= SITE_UNDANGAN . '/' . $row->domain ?>"><?= $row->domain ?></a></td>

                                        <?php if ($row->statusPembayaran == '1') { ?>
                                            <td><span class="badge alert-warning">Menunggu Konfirmasi</span></td>
                                        <?php } else if ($row->statusPembayaran == '2') { ?>
                                            <td><span class="badge alert-success">Lunas</span></td>
                                        <?php } else { ?>
                                            <td><span class="badge alert-secondary">Belum Lunas</span></td>
                                        <?php } ?>

                                        <td>
                                            <div class="btn-group mb-1">
                                                <button type="button" id="btnLihat-<?= $limit ?>" onclick="lihat(<?= $limit ?>)" class="btn btn-primary btn-sm" data-nama="<?= $row->nama_lengkap ?>" data-bank="<?= $row->nama_bank ?>" data-invoice="<?= $row->invoice ?>">
                                                    Lihat
                                                </button>
                                                <?php if ($row->statusPembayaran == '1') { ?>
                                                    <button style="margin-left: 10px;" type="button" class="konfirmasiBtn btn btn-success btn-sm" data-id="<?= $row->id_user ?>" data-toggle="modal" data-target="#modalKonfirmasi">
                                                        Konfirmasi
                                                    </button>
                                                <?php } ?>
                                            </div>
                                            <div class="btn-group mb-1">
                                                <button type="button" id="btnHapus" class="konfirmasiHapus btn btn-danger btn-sm" data-id="<?= $row->id_user ?>" data-toggle="modal" data-target="#modalHapus">
                                                    Hapus
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>


                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>


        <!-- Modal -->
        <div class="modal fade" id="modalKonfirmasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Apakah kamu yakin ingin mengkonfirmasi pengguna ?
                        <input value="" type="hidden" id="iduser">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-primary" id="konfirmasi">Ya</button>
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input id="nama_lengkap" type="text" class="form-control" placeholder="Contoh : Dinda Rahma" value="" required>
                        </div>
                        <div class="form-group">
                            <label>Nama Bank</label>
                            <input id="nama_bank" type="text" class="form-control" placeholder="Contoh : BRI Syariah " value="" required>
                        </div>
                        <div class="form-group mb-2">
                            <label>Bukti Transfer</label><br>
                            <img id="bukti" src="" height="250px">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Modal -->
        <div id="addPengguna" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Tambah</h5>
                    </div>
                    <div id="addPenggunaBody" class="modal-body">
                        <form id="create-new-user" class="mb-3" autocomplete="off">
                            <div class="form-row">
                                <h4> Informasi Akun</h4>
                                <div class="form-group col-md-12">
                                    <label for="domain" class="control-label">Nama Domain / URL Undangan</label>
                                    <label style="bottom: -12.3px;position: inherit;padding-left: 10px;color: #005CAA;font-weight: bold;display: table;margin-bottom: -1.4rem;   ">begather.id/</label>
                                    <input name="domain" type="text" class="form-control" placeholder="akudandia" style="padding-left: 84px; text-transform: lowercase;" value="" onkeyup="nospaces(this)" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="email" class="control-label">Email</label>
                                    <input id="email" type="email" name="email" class="form-control" placeholder="email" autocomplete="off" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="hp" class="control-label">Nomor HP</label>
                                    <input id="hp" name="hp" class="form-control" placeholder="Nomor Hp" onkeypress='validate(event)' autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-row">
                                <h4> Data Mempelai Pria</h4>
                                <div class="form-group col-md-6">
                                    <label for="nama_lengkap_pria" class="control-label">Nama Lengkap</label>
                                    <input id="nama_lengkap_pria" name="nama_lengkap_pria" class="form-control" placeholder="Nama Lengkap Pria" autocomplete="off" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="nama_panggilan_pria" class="control-label">Nama Panggilan</label>
                                    <input id="nama_panggilan_pria" name="nama_panggilan_pria" class="form-control" placeholder="Nama Panggilan Pria" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="nama_ayah_pria" class="control-label">Nama Ayah</label>
                                    <input id="nama_ayah_pria" name="nama_ayah_pria" class="form-control" placeholder="Nama Ayah Pria" autocomplete="off" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="nama_ibu_pria" class="control-label">Nama Ibu</label>
                                    <input id="nama_ibu_pria" name="nama_ibu_pria" class="form-control" placeholder="Nama Ibu Pria" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <h4> Data Mempelai Wanita</h4>
                                <div class="form-group col-md-6">
                                    <label for="nama_lengkap_wanita" class="control-label">Nama Lengkap</label>
                                    <input id="nama_lengkap_wanita" name="nama_lengkap_wanita" class="form-control" placeholder="Nama Lengkap wanita" autocomplete="off" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="nama_panggilan_wanita" class="control-label">Nama Panggilan</label>
                                    <input id="nama_panggilan_wanita" name="nama_panggilan_wanita" class="form-control" placeholder="Nama Panggilan wanita" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="nama_ayah_wanita" class="control-label">Nama Ayah</label>
                                    <input id="nama_ayah_wanita" name="nama_ayah_wanita" class="form-control" placeholder="Nama Ayah wanita" autocomplete="off" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="nama_ibu_wanita" class="control-label">Nama Ibu</label>
                                    <input id="nama_ibu_wanita" name="nama_ibu_wanita" class="form-control" placeholder="Nama Ibu wanita" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <h4> Akad Nikah</h4>
                                <div class="form-group col-md-4">
                                    <label for="tanggal_akad" class="control-label">Tanggal</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="datepicker">
                                        <input type="hidden" id="tanggal_akad" value="">
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="waktu_akad" class="control-label">Waktu / Jam</label>
                                    <input id="waktu_akad" name="waktu_akad" class="form-control" placeholder="10:00 Pagi" autocomplete="off" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="lokasi_akad" class="control-label">Tempat / Lokasi</label>
                                    <input id="lokasi_akad" name="lokasi_akad" class="form-control" placeholder="Masjid Istiqlal" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="alamat_akad" class="control-label">Alamat</label>
                                    <textarea name="alamat_akad" type="text" class="form-control" placeholder="Contoh : JL. Ahmad Yani No.1" required></textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <h4> Akad Nikah</h4>
                                <div class="form-group col-md-4">
                                    <label for="tanggal_resepsi" class="control-label">Tanggal</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="datepicker2">
                                        <input type="hidden" id="tanggal_resepsi" value="">
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="waktu_resepsi" class="control-label">Waktu / Jam</label>
                                    <input id="waktu_resepsi" name="waktu_resepsi" class="form-control" placeholder="02:00 Siang" autocomplete="off" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="lokasi_resepsi" class="control-label">Tempat / Lokasi</label>
                                    <input id="lokasi_resepsi" name="lokasi_resepsi" class="form-control" placeholder="Hotel Bintang 5" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="alamat_resepsi" class="control-label">Alamat</label>
                                    <textarea name="alamat_resepsi" type="text" class="form-control" placeholder="Contoh : JL. Ahmad Yani No.1" required></textarea>
                                </div>
                            </div>
                            <button id="submitPengguna" class="btn btn-primary" type="submit">
                                <span id="spinner" class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true" hidden></span>
                                Tambah
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="modalHapus" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Hapus Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Yakin ingin menghapus data ini?</p>
                        <input value="" type="hidden" id="iduser">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="hapusData">Hapus</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <script src="<?= base_url('assets/adminlte/bower_components/jquery/dist/jquery.min.js') ?>"></script>
        <script src="<?php echo base_url() ?>/assets/base/js/pikaday.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.all.min.js"></script>


        <!-- jQuery UI 1.11.4 -->
        <script>
            function lihat(limit) {
                $("#modalData").modal();
                var nama = document.getElementById("btnLihat-" + limit).getAttribute("data-nama");
                var bank = document.getElementById("btnLihat-" + limit).getAttribute("data-bank");
                var invoice = document.getElementById("btnLihat-" + limit).getAttribute("data-invoice");
                $('#nama_lengkap').val(nama);
                $('#nama_bank').val(bank);
                $('#bukti').attr('src', '<?= base_url() ?>/assets/bukti/' + invoice + '.png');
            }

            function tambahPengguna() {
                $("#addPengguna").modal();
            }

            $(document).ready(function() {
                moment.locale('id');
                $('#datepicker2').val(moment('2022/01/01').format('dddd, Do MMMM YYYY'));
                var picker = new Pikaday({
                    format: 'dddd, Do MMMM YYYY',
                    field: $('#datepicker2')[0],
                    onSelect: function() {
                        $('#tanggal_resepsi').val(this.getMoment().format('YYYY/MM/DD'));
                    }
                });
                $('#datepicker').val(moment('2022/01/01').format('dddd, Do MMMM YYYY'));
                var picker = new Pikaday({
                    format: 'dddd, Do MMMM YYYY',
                    field: $('#datepicker')[0],
                    onSelect: function() {
                        $('#tanggal_akad').val(this.getMoment().format('YYYY/MM/DD'));
                    }
                });
            })

            function validate(evt) {
                var theEvent = evt || window.event;

                // Handle paste
                if (theEvent.type === 'paste') {
                    key = event.clipboardData.getData('text/plain');
                } else {
                    // Handle key press
                    var key = theEvent.keyCode || theEvent.which;
                    key = String.fromCharCode(key);
                }
                var regex = /[0-9]|\./;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();
                }
            }

            $("#create-new-user").submit(function() {
                let tanggal_resepsi = $("#tanggal_resepsi").val();
                let tanggal_akad = $("#tanggal_akad").val();
                let data = $("#create-new-user").serialize() + '&tanggal_resepsi=' + tanggal_resepsi + '&tanggal_akad=' + tanggal_akad;
                $.ajax({
                    url: "<?= base_url('admin/tambah_pengguna') ?>",
                    method: "POST",
                    data: data,
                    async: true,
                    dataType: 'json',
                    success: function($hasil) {
                        if (!$hasil.error) {
                            Swal.fire(
                                $hasil.message,
                                '',
                                'success'
                            )
                            location.reload();
                        } else {
                            alert($hasil.message)
                        }
                    }
                });

                return false

            });

            function nospaces(t) {
                if (t.value.match(/\s/g)) {
                    t.value = t.value.replace(/\s/g, '');
                }
            }

            $('.konfirmasiBtn').on('click', function(event) {
                var id = $(this).data('id');
                $(".modal-body #iduser").val(id);
            });

            $('#konfirmasi').on('click', function(event) {
                var id = $('#iduser').val();
                $.ajax({
                    url: "<?= base_url('admin/konfirmasi') ?>",
                    method: "POST",
                    data: {
                        id: id
                    },
                    async: true,
                    dataType: 'html',
                    success: function($hasil) {
                        if ($hasil == 'sukses') {
                            location.reload();
                        } else {
                            alert('Gagal')
                        }
                    }
                });
            });

            $('.konfirmasiHapus').on('click', function(event) {
                var id = $(this).data('id');
                $(".modal-body #iduser").val(id);
            });

            $('#hapusData').on('click', function(event) {
                var id = $('#iduser').val();
                $.ajax({
                    url: "<?= base_url('admin/deletePengguna') ?>",
                    method: "POST",
                    data: {
                        id: id
                    },
                    async: true,
                    dataType: 'json',
                    success: function($hasil) {
                        if (!$hasil.error) {
                            Swal.fire(
                                $hasil.message,
                                '',
                                'success'
                            )
                            location.reload();
                        } else {
                            alert($hasil.message)
                        }
                    }
                });
            });
        </script>

        <!-- /.row -->
    </section>
</div>