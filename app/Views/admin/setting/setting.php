<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Setting
            <small>begather.id</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.row -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Setting Harga</h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <form id="update-harga">
                            <div class="form-group row">
                                <label for="harga" class="col-sm-2 col-form-label">Harga</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control-plaintext" name="harga" id="harga" value="<?= $setting->harga ?>">
                                </div>
                            </div>
                            <button id="editHarga" class="btn btn-primary" type="submit">
                                <span id="spinner" class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true" hidden></span>
                                Simpan
                            </button>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-xs-12">
                <!-- /.row -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Database</h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div style="padding-bottom: 10px;">
                            <button id="tambahPengguna" data-toggle="modal" data-target="#modalTruncateAll" type="button" class="btn btn-danger" onclick="return kosongkanSemuaData()"><i class="fa fa-minus-circle"></i> Kosongkan Semua Data</button>
                        </div>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Tabel</th>
                                    <th>Jumlah Data</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $limit = 0;
                                foreach ($table_db as $row) { ?>
                                    <tr>
                                        <td><?php echo $row->table ?></td>
                                        <td><?php echo $row->jumlah_row ?></td>
                                        <td> <button style="margin-left: 10px;" type="button" class="truncateBtn btn btn-danger btn-sm" data-table="<?= $row->table ?>" data-toggle="modal" data-target="#modalTruncate">
                                                Kosongkan Table
                                            </button></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>


                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-xs-12">
                <!-- /.row -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Kosongkan Folder</h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div style="padding-bottom: 10px;">
                            <form id="delete-path" class="" autocomplete="off">
                                <div class="form-row">
                                    <div class="form-group col-sm-6">
                                        <select class="form-control form-select-sm" id="path" name="path" aria-label="Default select example">
                                            <option selected disabled>Pilih Folder</option>
                                            <option value="users">Data Pengguna</option>
                                            <option value="logs">Log</option>
                                            <option value="bukti">Bukti Pembayaran</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <button type="button" class="btn btn-danger" onclick="modalHapusPath()"><i class="fa fa-minus-circle"></i> Hapus</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>

        <div class="modal" id="modalTruncate" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Kosongkan Tabel</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Yakin ingin menghapus semua data pada tabel ini?</p>
                        <input value="" type="hidden" id="table">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="truncateData">Hapus</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="modalTruncateAll" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Kosongkan Tabel</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Yakin ingin menghapus semua data?</p>
                        <input value="" type="hidden" id="table">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="truncateAllData">Hapus</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="modalDeletePath" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Kosongkan Folder</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Yakin ingin menghapus semua data?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="deletePath">Hapus</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?= base_url('assets/adminlte/bower_components/jquery/dist/jquery.min.js') ?>"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.0/dist/sweetalert2.all.min.js"></script>

        <script>
            $("#update-harga").submit(function() {
                $.ajax({
                    url: "<?= base_url('admin/update_harga') ?>",
                    method: "POST",
                    data: $("#update-harga").serialize(),
                    async: true,
                    dataType: 'json',
                    success: function($hasil) {
                        if (!$hasil.error) {
                            Swal.fire(
                                $hasil.message,
                                '',
                                'success'
                            )
                            location.reload();
                        } else {
                            alert($hasil.message)
                        }
                    }
                });

                return false

            });





            $('#truncateAllData').on('click', function(event) {
                $.ajax({
                    url: "<?= base_url('admin/truncateAllTable') ?>",
                    method: "POST",
                    async: true,
                    dataType: 'json',
                    success: function($hasil) {
                        if (!$hasil.error) {
                            Swal.fire(
                                $hasil.message,
                                '',
                                'success'
                            )
                            location.reload();
                        } else {
                            alert($hasil.message)
                        }
                    }
                });

                return false

            });

            $('.truncateBtn').on('click', function(event) {
                var table = $(this).data('table');
                $(".modal-body #table").val(table);
            });

            $('#truncateData').on('click', function(event) {
                var table = $('#table').val();
                $.ajax({
                    url: "<?= base_url('admin/truncateTable') ?>",
                    method: "POST",
                    data: {
                        table: table
                    },
                    async: true,
                    dataType: 'json',
                    success: function($hasil) {
                        if (!$hasil.error) {
                            Swal.fire(
                                $hasil.message,
                                '',
                                'success'
                            )
                            location.reload();
                        } else {
                            alert($hasil.message)
                        }
                    }
                });
            });


            function modalHapusPath() {
                if ($('#path').val() == null) {
                    alert("Pilih Folder")
                } else {
                    $('#modalDeletePath').modal('show');
                }
            }


            $("#deletePath").click(function() {
                $.ajax({
                    url: "<?= base_url('admin/deleteFolder') ?>",
                    method: "POST",
                    data: {
                        path: $('#path').val()
                    },
                    async: true,
                    dataType: 'json',
                    success: function($hasil) {
                        if (!$hasil.error) {
                            Swal.fire(
                                $hasil.message,
                                '',
                                'success'
                            )
                            location.reload();
                        } else {
                            alert($hasil.message)
                        }
                    },
                    error: function(error) {
                        alert('Sistem Error');
                    }
                });
                return false

            });
        </script>
        <!-- /.row -->
    </section>
</div>