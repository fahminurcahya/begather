<!doctype html>
<html lang="en">

<head>

    <?php foreach ($mempelai->getResult() as $row) { ?>
        <title><?php echo $row->nama_panggilan_pria . " & " . $row->nama_panggilan_wanita; ?> </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:title" content="<?php echo $row->nama_panggilan_pria . " & " . $row->nama_panggilan_wanita; ?>">
        <meta property="og:description" content="<?php
                                                    echo 'Hello ' . $invite . '! Kamu Di Undang..';
                                                    ?>">
        <meta property="og:url" content="<?php echo base_url() ?>">
    <?php } ?>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/themes/template2/assets/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/themes/template2/assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/themes/template2/assets/card.css">

    <script type="text/javascript" src="<?php echo base_url() ?>/assets/themes/template2/assets/jquery1.4.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>/assets/themes/template2/assets/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>/assets/themes/template2/assets/fancybox/jquery.fancybox-1.3.4.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>/assets/themes/template2/assets/card.js"></script>

    <title>Ourdream.id</title>
</head>

<body id="home" class="stop-scrolling">
    <?php foreach ($data->getResult() as $row) {
        $kunci = $row->kunci;
        $youtube = $row->video;
        $salam_pembuka = $row->salam_pembuka;
        $musiknya = "/assets/users/" . $kunci . "/musik.mp3";
        $maps = $row->maps;
    }
    ?>
    <?php foreach ($acara->getResult() as $row) {
        $tanggal_resepsi = $row->tanggal_resepsi;
    ?>
    <?php } ?>

    <audio loop src="<?php echo base_url() ?><?= $musiknya ?>" id="audio"></audio>
    <input id="tanggal-resepsi" type="text" value="<?php echo $tanggal_resepsi ?>" hidden>

    <!-- cover  -->
    <section class="cover-undangan text-center bg-sampul" id="cover-undangan" style="color: #f6a454;">
        <?php foreach ($mempelai->getResult() as $row) {  ?>

            <h1 class="display-3" data-aos="zoom-in-up">The Wedding Of</h1>
            <br>
            <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/kita.png" alt="image" width="200px" class="rounded-circle img-thumbnail" data-aos="fade-in">
            <br><br>

            <p class="lead" data-aos="zoom-in-up" data-aos-duration="2000"><?php echo $row->nama_panggilan_pria; ?> & <?php echo $row->nama_panggilan_wanita; ?></p>
            <br>
            <br>
            <br>
        <?php } ?>

        <button type="button" class="btn" style="background-color: #f6a454;" onclick="play()">Buka Undangan</button>
    </section>
    <!-- akhir cover  -->

    <!-- sampul  -->
    <section class="sampul text-center bg-sampul" id="sampul" style="color: #f6a454;">
        <?php foreach ($mempelai->getResult() as $row) {  ?>
            <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/kita.png" alt="image" width="200px" class="rounded-circle img-thumbnail" data-aos="fade-in">
            <br><br>
            <h1 class="display-3" data-aos="zoom-in-up">The Wedding Of</h1>
            <br>
            <p class="lead" data-aos="zoom-in-up" data-aos-duration="2000"><?php echo $row->nama_panggilan_pria; ?> & <?php echo $row->nama_panggilan_wanita; ?></p>
        <?php } ?>
    </section>
    <!-- akhir sampul  -->

    <!-- mempelai  -->
    <section id="mempelai" class="pt-5 pb-5 ps-3 pe-3">
        <?php foreach ($mempelai->getResult() as $row) {  ?>
            <div class="bingkai">
                <div id="bingkai-kiri-atas">
                    <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kiri-atas.png" width="300px" alt="image">
                </div>
                <div id="bingkai-kiri-bawah">
                    <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kanan-atas.png" width="300px" alt="image">
                </div>
                <div id="bingkai-kanan-atas">
                    <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kanan-atas.png" width="300px" alt="image">
                </div>
                <div id="bingkai-kanan-bawah">
                    <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kiri-atas.png" width="300px" alt="image">
                </div>
            </div>

            <div class="borid" data-aos="fade-in">
                <div class="container text-center frame">
                    <div class="row" data-aos="fade-down">
                        <div class="col-sm-12" id="font2">
                            Assalamualaikum warahmatullohi wabarokatuh
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;" data-aos="fade-down">
                        <div class="col-sm-12" id="font2">
                            Dengan memohon rahmat dan ridho Allah SWT,
                            Kami akan menyelenggarakan resepsi pernikahan Putra-Putri kami
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;" data-aos="zoom-in-up" data-aos-duration="2000">
                        <div class="col-sm-12">
                            <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/groom.png" alt="image" width="150px" class="rounded-circle img-thumbnail">
                        </div>
                        <div class="col-sm-12">
                            <h1><?php echo $row->nama_pria; ?></h1>
                        </div>
                        <div class="col-sm-12">
                            <?php echo "Putra " . $row->nama_ayah_pria . " dan " . $row->nama_ibu_pria  ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-3 mb-3" id="cinzel-b" data-aos="fade-down">
                            Dengan
                        </div>
                    </div>
                    <div class="row" data-aos="zoom-in-up" data-aos-duration="2000">
                        <div class="col-sm-12">
                            <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/bride.png" alt="image" width="150px" class="rounded-circle img-thumbnail">
                        </div>
                        <div class="col-sm-12">
                            <h1><?php echo $row->nama_wanita; ?></h1>
                        </div>
                        <div class="col-sm-12">
                            <?php echo "Putri " . $row->nama_ayah_wanita . " dan " . $row->nama_ibu_wanita  ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <hr>
                        </div>
                        <div class="col-2">

                        </div>
                        <div class="col-5">
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" id="cinzel-b">
                            <span id="resepsi-tanggal">
                        </div>
                    </div>
                    <div class="row">
                        <div class="container font2" id="font2">
                            <li id="cutd"><span id="days"></span>Hari</li>
                            <li id="cutd"><span id="hours"></span>Jam</li>
                            <li id="cutd"><span id="minutes"></span>Menit</li>
                            <li id="cutd"><span id="seconds"></span>Detik</li>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </section>
    <!-- akhir mempelai  -->

    <!-- acara  -->
    <section id="acara" class="pt-5 pb-5 ps-3 pe-3">
        <?php

        foreach ($acara->getResult() as $row) {
            $tanggal_akad =  $row->tanggal_akad;
            $tanggal_resepsi =  $row->tanggal_resepsi;
        ?>
            <div class="bingkai">
                <div id="bingkai-kiri-atas">
                    <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kiri-atas.png" width="300px" alt="image">
                </div>
                <div id="bingkai-kiri-bawah">
                    <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kanan-atas.png" width="300px" alt="image">
                </div>
                <div id="bingkai-kanan-atas">
                    <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kanan-atas.png" width="300px" alt="image">
                </div>
                <div id="bingkai-kanan-bawah">
                    <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kiri-atas.png" width="300px" alt="image">
                </div>
            </div>
            <div class="borid" data-aos="fade-in">
                <div class="container text-center frame">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card bg-transparent" data-aos="zoom-in-up" data-aos-duration="2000">
                                <div class="card-header">
                                    <h1>Akad Nikah</h1>
                                </div>
                                <div class="card-body">
                                    <div class="row text-start">
                                        <table>
                                            <tr>
                                                <td>Tanggal</td>
                                                <td>:</td>
                                                <td><?php echo $row->tanggal_akad; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Jam</td>
                                                <td>:</td>
                                                <td><?php echo $row->jam_akad; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tempat</td>
                                                <td>:</td>
                                                <td><?php echo $row->alamat_akad; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-sm-12">
                            <div class="card bg-transparent" data-aos="zoom-in-up" data-aos-duration="2000">
                                <div class="card-header">
                                    <h1>Resepsi</h1>
                                </div>
                                <div class="card-body">
                                    <div class="row text-start">
                                        <table>
                                            <tr>
                                                <td>Tanggal</td>
                                                <td>:</td>
                                                <td><?php echo $row->tanggal_resepsi; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Jam</td>
                                                <td>:</td>
                                                <td><?php echo $row->jam_akad; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tempat</td>
                                                <td>:</td>
                                                <td><?php echo $row->alamat_akad; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="row mt-5">
                            <?php if ($maps != "") {
                                $map = str_replace('width="400"', 'style="border:1px solid #888888; box-shadow: 5px 10px 18px #888888; border-radius: 25px;"', "$maps");
                            ?>
                                <div class="col-sm-12" data-aos="zoom-in-up" data-aos-duration="2000">
                                    <?php echo $map; ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </section>
    <!-- akhir acara  -->


    <!-- album  -->
    <section class="gallery-section section-padding pt-5 pb-5 ps-3 pe-3" id="gallery">
        <div class="bingkai">
            <div id="bingkai-kiri-atas">
                <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kiri-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kiri-bawah">
                <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kanan-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kanan-atas">
                <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kanan-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kanan-bawah">
                <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kiri-atas.png" width="300px" alt="image">
            </div>
        </div>
        <div class="borid" data-aos="fade-in">
            <div class="container-sm frame">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-title text-center mb-5 mt-5">
                            <h1 class="display-6">Gallery Foto Kami</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="gallery-grids">
                            <?php foreach ($album as $key => $data) {  ?>
                                <div class="grid" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="3000">
                                    <a id="example4" href="<?php echo base_url() ?>/assets/users/<?php echo $kunci . '/' . $data['album'] ?>.png" class="fancybox" data-fancybox-group="gall-1">
                                        <img src="<?php echo base_url() ?>/assets/users/<?php echo $kunci . '/' . $data['album']; ?>.png" alt>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php if ($youtube != "") {
                            $embed = str_replace("youtu.be", "www.youtube.com/embed", "$youtube");
                        ?>
                            <div class="youtube">
                                <iframe style="width: 280px; " src="<?php echo $embed; ?>" allow="fullscreen">
                                </iframe>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- akhir album  -->


    <!-- cerita  -->
    <section id="cerita" class="pt-5 pb-5 ps-3 pe-3">
        <div class="bingkai">
            <div id="bingkai-kiri-atas">
                <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kiri-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kiri-bawah">
                <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kanan-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kanan-atas">
                <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kanan-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kanan-bawah">
                <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kiri-atas.png" width="300px" alt="image">
            </div>
        </div>
        <div class="borid mb-2" data-aos="fade-in">
            <div class="container pb-5 frame">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h1>Cerita Kita</h1>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="main-timeline">
                                <?php
                                $no = 0;
                                foreach ($cerita as $key => $data) {
                                    $no++;
                                    if ($no % 2 == 0) { ?>
                                        <div class="timeline">
                                            <div class="timeline-icon"></div>
                                            <div class="timeline-content right" data-aos="zoom-in-up" data-aos-duration="2000">
                                                <span class="date"><?php echo $data['tanggal_cerita']; ?></span>
                                                <h4 class="title"><?php echo $data['judul_cerita']; ?></h4>
                                                <p class="description">
                                                    <?php echo $data['isi_cerita']; ?>
                                                </p>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="timeline">
                                            <div class="timeline-icon"></div>
                                            <div class="timeline-content" data-aos="zoom-in-up" data-aos-duration="2000">
                                                <span class="date"><?php echo $data['tanggal_cerita']; ?></span>
                                                <h4 class="title"><?php echo $data['judul_cerita']; ?></h4>
                                                <p class="description">
                                                    <?php echo $data['isi_cerita']; ?>
                                                </p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- akhir cerita  -->


    <!-- Ucapan  -->
    <section id="komentar" class="pt-5 pb-5 ps-3 pe-3">
        <div class="bingkai">
            <div id="bingkai-kiri-atas">
                <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kiri-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kiri-bawah">
                <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kanan-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kanan-atas">
                <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kanan-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kanan-bawah">
                <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kiri-atas.png" width="300px" alt="image">
            </div>
        </div>
        <div class="borid pb-5" data-aos="fade-in">
            <div class="container pb-5 frame">
                <div class="row">
                    <div class="col-sm-12 text-center" id="font2">
                        <h3>Beri Ucapan</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <form>
                            <div class="form-group row">
                                <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nama">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="pesan" class="col-sm-2 col-form-label">Pesan</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="pesan" rows="4"></textarea>
                                </div>
                            </div>
                            <legend class="col-form-label col-sm-2 float-sm-left pt-0">Kehadiran</legend>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="kehadiran" id="kehadiran1" value="option1" checked>
                                    <label class="form-check-label" for="kehadiran1">
                                        Hadir
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="kehadiran" id="kehadiran2" value="option2">
                                    <label class="form-check-label" for="kehadiran2">
                                        Tidak Hadir
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row pt-3">
                                <div class="col-sm-10">
                                    <button type="button" class="btn btn-primary">Kirim</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row text-center">
                    <div>
                        <hr>
                    </div>
                    <div class="col-sm-12 bodykomen">
                        <?php foreach ($komen as $key => $data) { ?>
                            <div class="komen">
                                <div class="col-12 komen-nama">
                                    <?= \esc($data['nama_komentar']); ?>
                                </div>
                                <div class="col-12 komen-isi">
                                    <?= \esc($data['isi_komentar']); ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- akhir ucapan  -->



    <!-- gift  -->
    <!-- <section id="gift">
        <div class="borid pb-5">
            <div class="container framepay" style="justify-content: center;">
                <div class="row">
                    <div class="col-sm-12 text-center mb-3" id="font2">
                        <h2 class="display-4">Kirim Hadiah</h2>
                    </div>
                </div>
                <div class="row p-2">
                    <div class="col-sm-12 preload">
                        <div class="creditcard">
                            <div class="front">
                                <svg version="1.1" id="cardfront" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                                    <g id="Front">
                                        <g id="CardBackground">
                                            <g id="Page-1_1_">
                                                <g id="amex_1_">
                                                    <path id="Rectangle-1_1_" class="grey grey" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                                            C0,17.9,17.9,0,40,0z" />
                                                </g>
                                            </g>
                                            <path class="greydark greydark" d="M750,431V193.2c-217.6-57.5-556.4-13.5-750,24.9V431c0,22.1,17.9,40,40,40h670C732.1,471,750,453.1,750,431z" />
                                        </g>
                                        <text transform="matrix(1 0 0 1 60.106 295.0121)" id="svgnumber" class="st2 st3 st4">0123 4567 8910 1112</text>
                                        <text transform="matrix(1 0 0 1 54.1064 428.1723)" id="svgname" class="st2 st5 st6">REY</text>
                                        <text transform="matrix(1 0 0 1 54.1074 389.8793)" class="st7 st5 st8">atas nama</text>
                                        <text transform="matrix(1 0 0 1 65.1054 241.5)" class="st7 st5 st8">Kirim Ke</text>
                                        <g>
                                            <text transform="matrix(1 0 0 1 479.3848 423.8095)" id="svgexpire" class="st2 st3 st4">BRI</text>
                                        </g>
                                        <g id="cchip">
                                            <g>
                                                <path class="st2" d="M168.1,143.6H82.9c-10.2,0-18.5-8.3-18.5-18.5V74.9c0-10.2,8.3-18.5,18.5-18.5h85.3
                                        c10.2,0,18.5,8.3,18.5,18.5v50.2C186.6,135.3,178.3,143.6,168.1,143.6z" />
                                            </g>
                                            <g>
                                                <g>
                                                    <rect x="82" y="70" class="st12" width="1.5" height="60" />
                                                </g>
                                                <g>
                                                    <rect x="167.4" y="70" class="st12" width="1.5" height="60" />
                                                </g>
                                                <g>
                                                    <path class="st12" d="M125.5,130.8c-10.2,0-18.5-8.3-18.5-18.5c0-4.6,1.7-8.9,4.7-12.3c-3-3.4-4.7-7.7-4.7-12.3
                                            c0-10.2,8.3-18.5,18.5-18.5s18.5,8.3,18.5,18.5c0,4.6-1.7,8.9-4.7,12.3c3,3.4,4.7,7.7,4.7,12.3
                                            C143.9,122.5,135.7,130.8,125.5,130.8z M125.5,70.8c-9.3,0-16.9,7.6-16.9,16.9c0,4.4,1.7,8.6,4.8,11.8l0.5,0.5l-0.5,0.5
                                            c-3.1,3.2-4.8,7.4-4.8,11.8c0,9.3,7.6,16.9,16.9,16.9s16.9-7.6,16.9-16.9c0-4.4-1.7-8.6-4.8-11.8l-0.5-0.5l0.5-0.5
                                            c3.1-3.2,4.8-7.4,4.8-11.8C142.4,78.4,134.8,70.8,125.5,70.8z" />
                                                </g>
                                                <g>
                                                    <rect x="82.8" y="82.1" class="st12" width="25.8" height="1.5" />
                                                </g>
                                                <g>
                                                    <rect x="82.8" y="117.9" class="st12" width="26.1" height="1.5" />
                                                </g>
                                                <g>
                                                    <rect x="142.4" y="82.1" class="st12" width="25.8" height="1.5" />
                                                </g>
                                                <g>
                                                    <rect x="142" y="117.9" class="st12" width="26.2" height="1.5" />
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <!-- sampul  -->
    <section class="sampul text-center bg-sampul" id="sampul" style="color: #f6a454;">
        <?php foreach ($mempelai->getResult() as $row) {  ?>
            <div class="bingkai">
                <div id="bingkai-kiri-atas">
                    <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kiri-atas.png" width="300px" alt="image">
                </div>
                <div id="bingkai-kiri-bawah">
                    <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kanan-atas.png" width="300px" alt="image">
                </div>
                <div id="bingkai-kanan-atas">
                    <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kanan-atas.png" width="300px" alt="image">
                </div>
                <div id="bingkai-kanan-bawah">
                    <img src="<?php echo base_url() ?>/assets/themes/template2/assets/img/kiri-atas.png" width="300px" alt="image">
                </div>
            </div>
            <h1 class="display-3">Kami Yang Berbahagia</h1>
            <br>
            <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/kita.png" alt="image" width="200px" class="rounded-circle img-thumbnail" data-aos="fade-in">
            <br><br>
            <p class="lead" style="font-size: 20pt;"><?php echo $row->nama_panggilan_pria; ?> & <?php echo $row->nama_panggilan_wanita; ?></p>
        <?php } ?>
    </section>
    <!-- akhir sampul  -->

    <!-- navigasi  -->
    <nav class="nav container">
        <div class="nav__menu" id="nav-menu">
            <ul class="nav__list" style="padding-right: 30px;">
                <li class="nav__item">
                    <a href="#home" class="nav__link sampul">
                        <i class='bx bx-home-alt nav__icon'></i>
                        <span class="nav__name">Home</span>
                    </a>
                </li>

                <li class="nav__item">
                    <a href="#mempelai" class="nav__link mempelai">
                        <i class='bx bx-user nav__icon'></i>
                        <span class="nav__name">Mempelai</span>
                    </a>
                </li>

                <li class="nav__item">
                    <a href="#acara" class="nav__link acara">
                        <i class='bx bx-book-alt nav__icon'></i>
                        <span class="nav__name">Acara</span>
                    </a>
                </li>

                <li class="nav__item">
                    <a href="#gallery" class="nav__link gallery">
                        <i class='bx bx-photo-album nav__icon'></i>
                        <span class="nav__name">Album</span>
                    </a>
                </li>
                <li class="nav__item">
                    <a href="#cerita" class="nav__link cerita">
                        <i class='bx bx-heart nav__icon'></i>
                        <span class="nav__name">Story</span>
                    </a>
                </li>

                <li class="nav__item">
                    <a href="#komentar" class="nav__link komentar">
                        <i class='bx bx-comment-detail nav__icon'></i>
                        <span class="nav__name">Ucapan</span>
                    </a>
                </li>


                <!-- <li class="nav__item">
                    <a href="#gift" class="nav__link">
                        <i class='bx bx-gift nav__icon'></i>
                        <span class="nav__name">Gift</span>
                    </a>
                </li> -->
            </ul>
        </div>
    </nav>
    <!-- akhir navigasi  -->



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>

    <script>
        AOS.init({
            // once : true,
            // duration: 2000,
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/gsap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.1.1/TextPlugin.min.js"></script>
    <script src="<?= base_url() ?>/assets/base/js/moment-with-locales.js"></script>

    <script>
        gsap.registerPlugin(TextPlugin);
    </script>
    <script>
        var pc1 = document.getElementById('cover-undangan');


        function play() {
            pc1.classList.toggle('hide');
            var audio = document.getElementById("audio");
            audio.play();
            $('body').removeClass('stop-scrolling')
            setTimeout(function() {
                pc1.style.display = 'none';
            }, 2000);
        }



        (function() {


            const second = 1000,
                minute = second * 60,
                hour = minute * 60,
                day = hour * 24;
            let today = new Date(),
                dd = String(today.getDate()).padStart(2, "0"),
                mm = String(today.getMonth() + 1).padStart(2, "0"),
                yyyy = today.getFullYear(),
                dayMonth = "01/10/";
            // birthday = dayMonth + '2022';
            let initial = $('#tanggal-resepsi').val().split(/\//);
            let resepsi = [initial[1], initial[2], initial[0]].join('/');
            moment.locale('id');

            document.getElementById("resepsi-tanggal").innerText = moment($('#tanggal-resepsi').val()).format('dddd, Do MMMM YYYY');
            today = mm + "/" + dd + "/" + yyyy;

            const countDown = new Date(resepsi).getTime()

            x = setInterval(function() {
                const now = new Date().getTime(),
                    distance = countDown - now;
                document.getElementById("days").innerText = Math.floor(distance / (day)),
                    document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
                    document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
                    document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);

                //do something later when date is reached
                if (distance < 0) {
                    document.getElementById("days").innerText = "0";
                    document.getElementById("hours").innerText = "0";
                    document.getElementById("minutes").innerText = "0";
                    document.getElementById("seconds").innerText = "0";
                    clearInterval(x);
                }
                //seconds
            }, 0)
        }());

        const sections = document.querySelectorAll("section");
        const navLi = document.querySelectorAll(".nav__menu ul li a");
        window.onscroll = () => {
            var current = "";

            sections.forEach((section) => {
                const sectionTop = section.offsetTop;
                if (pageYOffset >= sectionTop - 60) {
                    current = section.getAttribute("id");
                }
            });

            // if (current == "sampul") {
            //         gsap.to('.lead', {duration: 2, delay:1.0, text: "Rey & Dinda", ease: "power2"});
            // } else {
            //     document.querySelectorAll(".lead").innerText = ""
            // }

            navLi.forEach((a) => {
                a.classList.remove("active-link");
                if (a.classList.contains(current)) {
                    a.classList.add("active-link");
                }
            });
        };
    </script>
    <script>
        $("a#example4").fancybox({
            'opacity': true,
            'overlayShow': false,
            'transitionIn': 'elastic',
            'transitionOut': 'none'
        });
    </script>

</html>
