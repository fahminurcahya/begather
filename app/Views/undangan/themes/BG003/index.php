<!doctype html>
<html lang="en">
<?php include 'head.php'; ?>

<body id="home">

    <!--=============== Sampul ===============-->
    <?php include 'sampul.php'; ?>
    <?php include 'mempelai.php'; ?>
    <?php include 'quote.php'; ?>
    <?php include 'acara.php'; ?>
    <?php include 'galery.php'; ?>
    <?php include 'cerita.php'; ?>
    <?php include 'komentar.php'; ?>
    <?php include 'gift.php'; ?>





    <!--=============== CONTACTME ===============-->
    <section id="footer">
        <div style="text-align: center; color:#fff">Design by Begather</div>
    </section>
    </main>


    <!--=============== MAIN JS ===============-->
    <script src="assets/main.js"></script>
</body>

<!-- animasi aos  -->
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
    AOS.init({
        // once : true,
        // duration: 2000,
    });
    $(document).ready(function() {
        // $('.slider').slick({
        //     // autoplay: true,
        //     // autoplaySpeed: 2500,
        // });
    })



    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        lazyLoad: 'ondemand',
        fade: true,
        asNavFor: '.slider-thumbnails'
    });

    $('.slider-thumbnails').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        asNavFor: '.slider',
        dots: true,
        centerMode: true,
        focusOnSelect: true
    });

    // function untuk scroll
    $('.nav__link').click(function(e) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top,
                behavior: 'smooth'
            });
        } // End if
    })
</script>
<!-- animasi aos  -->

</html>