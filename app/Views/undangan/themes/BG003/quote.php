<section id="quotes" class="pt-5 pb-3 ps-1 pe-1">
    <div class="borid" data-aos="fade-in">
        <div class="container text-center  frame-quote">
            <div class="row">
                <div class="col-sm-12">
                    <p style="font-size: 12px; margin-top:10px">
                        <?= $quote ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>