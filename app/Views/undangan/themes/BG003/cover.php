   <!-- cover  -->
   <section class="cover-undangan text-center" id="cover-undangan" style="color: #5e6661;">
       <?php foreach ($mempelai->getResult() as $row) {  ?>

           <!-- image border cover -->
           <img class="mask-layer" src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/kita.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/kita.png'" alt="image" width="200px" data-aos="fade-in">
           <br><br>
           <h3 class="display-3" data-aos="zoom-in-up">The Wedding Celebrations of</h3>
           <br>
           <div class="nama" data-aos="zoom-in-up" data-aos-duration="2000">
               <p class="lead"><?php echo $row->nama_panggilan_pria; ?></p>
               <p class="firplay">&</p>
               <p class="lead"><?php echo $row->nama_panggilan_wanita; ?></p>
           </div>
           <br>
           <div id="tamu" style="line-height: 50%;">

               <p>Kepada Yth Bapak/Ibu/Saudara/i<br>
               <p id="kepada"></p>
               </p>
               <p style="font-size: 8pt;">mohon maaf apabila ada kesalahan nama dan gelar</p>
           </div>
       <?php } ?>
       <button type="button" class="btn buka" style="background-color: #5e6661; color:#cea656" onclick="play()">Buka Undangan</button>
   </section>
   <!-- akhir cover  -->