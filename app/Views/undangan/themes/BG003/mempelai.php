    <!-- mempelai  -->
    <section id="mempelai" class="pt-5 pb-5 ps-3 pe-3 bg-mempelai">
        <?php foreach ($mempelai->getResult() as $row) {  ?>

            <div class="bingkai">
                <div id="bingkai-kanan-atas" data-aos="fade-in">
                    <img src="<?php echo base_url() ?>/assets/themes/BG003/assets/img/kanan-atas.png" width="150px" alt="image">
                </div>
                <div id="bingkai-kiri-atas" data-aos="fade-in">
                    <img src="<?php echo base_url() ?>/assets/themes/BG003/assets/img/kanan-atas.png" width="150px" alt="image">
                </div>
                <div id="bingkai-kiri-bawah" data-aos="fade-in">
                    <img src="<?php echo base_url() ?>/assets/themes/BG003/assets/img/kiri-bawah.png" width="200px" alt="image">
                </div>
                <div id="bingkai-kanan-bawah" data-aos="fade-in" style="-webkit-transform: scaleX(-1); transform: scaleX(-1);">
                    <img src="<?php echo base_url() ?>/assets/themes/BG003/assets/img/kiri-bawah.png" width="200px" alt="image">
                </div>
            </div>
            <div class="borid" data-aos="fade-in">
                <div class="container text-center frame">
                    <div class="row" data-aos="fade-down">
                        <div class="col-sm-12" id="font2">
                            Assalamualaikum warahmatullohi wabarokatuh
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;" data-aos="fade-down">
                        <div class="col-sm-12" id="font2">
                            <?= $salam_pembuka ?>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;" data-aos="zoom-in-up" data-aos-duration="2000">
                        <div class="col-sm-12">
                            <img class="mask-layer-mempelai" src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/groom.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/male.png'" alt="image" width="150px" class="rounded-circle img-thumbnail">
                        </div>
                        <div class="col-sm-12">
                            <h2><?php echo $row->nama_pria; ?></h2>
                        </div>
                        <div class="col-sm-12" id="font2">
                            <?php echo "Putra " . $row->nama_ayah_pria . " dan " . $row->nama_ibu_pria  ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-3 mb-3" data-aos="fade-down">
                            <span style="color: #cea656;  font-style: bold; font-size:50pt;">&</span>
                        </div>
                    </div>
                    <div class="row" data-aos="zoom-in-up" data-aos-duration="2000">
                        <div class="col-sm-12">
                            <img class="mask-layer-mempelai" src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/bride.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/female.png'" alt="image" width="150px" class="rounded-circle img-thumbnail">
                        </div>
                        <div class="col-sm-12">
                            <h2><?php echo $row->nama_wanita; ?></h2>
                        </div>
                        <div class="col-sm-12" id="font2">
                            <?php echo "Putri " . $row->nama_ayah_wanita . " dan " . $row->nama_ibu_wanita  ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </section>
    <!-- akhir mempelai  -->