<section class="sampul text-center" id="sampul" style="color: #000000;">
    <?php foreach ($mempelai->getResult() as $row) {  ?>

        <div class="bingkai">
            <div id="bingkai-bawah" data-aos="fade-in">
                <img src="<?php echo base_url() ?>/assets/themes/BG003/assets/img/bawah.png" width="150px" alt="image">
            </div>
        </div>


        <div style="position: relative;text-align: center;" class="bg-sampul">

            <!-- image border cover -->
            <img class="mask-layer" src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/kita.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/kita.png'" alt="image" width="250px" data-aos="fade-in">

            <br><br>
            <h3 class="display-3" data-aos="zoom-in-up">The Wedding Celebrations of</h3>
            <br>
            <div class="nama" data-aos="zoom-in-up" data-aos-duration="2000">
                <p class="lead"><?php echo $row->nama_panggilan_pria; ?></p>
                <p class="firplay">&</p>
                <p class="lead"><?php echo $row->nama_panggilan_wanita; ?></p>
            </div>
            <br>
            <div class="row" data-aos="fade-down">
                <div class="col" id="font2" style="color: #5e6661;">
                    <span id="resepsi-tanggal">
                </div>
            </div>
            <div class="row" data-aos="fade-down">
                <div class="container font2" id="font2">
                    <li id="cutd">
                        <div class="count-down-box">
                            <div class="count-down" id="font2">
                                <span id="days"></span><span style="font-size:10pt;">Hari</span>
                            </div>
                        </div>
                    </li>
                    <li id="cutd">
                        <div class="count-down-box">
                            <div class="count-down" id="font2">
                                <span id="hours"></span><span style="font-size:10pt;">Jam</span>
                            </div>
                        </div>
                    </li>
                    <li id="cutd">
                        <div class="count-down-box">
                            <div class="count-down" id="font2">
                                <span id="minutes"></span><span style="font-size:10pt;">Menit</span>
                            </div>
                        </div>
                    </li>
                    <li id="cutd">
                        <div class="count-down-box">
                            <div class="count-down" id="font2">
                                <span id="seconds"></span><span style="font-size:10pt;">Detik</span>
                            </div>
                        </div>
                    </li>
                </div>
            </div>
        </div>
    <?php } ?>
</section>
<!-- akhir sampul  -->