<nav class="nav container" id="nav" style="text-align: center;">
    <?php foreach ($rules->getResult() as $row) {
        $gallery = $row->gallery;
        $ceritarule = $row->cerita;
    }
    ?>

    <div class="nav__menu" id="nav-menu" style="margin-bottom: 20px;">
        <ul class="nav__list">
            <li class="nav__item">
                <a href="#mempelai" class="nav__link">
                    <span class="icon-mempelai nav__icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                    <span class="nav__name">Mempelai</span>
                </a>
            </li>

            <li class="nav__item">
                <a href="#acara" class="nav__link">
                    <span class="icon-acara nav__icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span></span>
                    <span class="nav__name">Acara</span>
                </a>
            </li>
            <?php if ($gallery == '1') { ?>
                <li class="nav__item">
                    <a href="#gallery" class="nav__link">
                        <span class="icon-galerry nav__icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span></span>
                        <span class="nav__name">Gallery</span>
                    </a>
                </li>
            <?php } ?>

            <?php if ($ceritarule == '1') { ?>
                <li class="nav__item">
                    <a href="#cerita" class="nav__link">
                        <span class="icon-story nav__icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                        <span class="nav__name">Story</span>
                    </a>
                </li>
            <?php } ?>


            <li class="nav__item">
                <a href="#komentar" class="nav__link">
                    <span class="icon-ucapan nav__icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span></span>
                    <span class="nav__name">Ucapan</span>
                </a>
            </li>
            <li class="nav__item">
                <a href="#gift" class="nav__link">
                    <span class="icon-gift nav__icon"><span class="path1"></span><span class="path2"></span></span>
                    <span class="nav__name">Hadiah</span>
                </a>
            </li>
        </ul>
    </div>
</nav>