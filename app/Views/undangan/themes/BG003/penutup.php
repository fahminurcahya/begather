<section class="sampul text-center" id="sampul" style="color: #000000;">
    <?php foreach ($mempelai->getResult() as $row) {  ?>

        <div class="bingkai">
            <div id="bingkai-bawah" data-aos="fade-in">
                <img src="<?php echo base_url() ?>/assets/themes/BG003/assets/img/bawah.png" width="150px" alt="image">
            </div>
        </div>


        <div style="position: relative;text-align: center;" class="bg-sampul">

            <!-- image border cover -->
            <img class="mask-layer" src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/kita.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/kita.png'" alt="image" width="250px" data-aos="fade-in">

            <br><br>
            <p>Merupakan suatu kehormatan dan kebahagiaan bagi kami apabila Bapak/Ibu/Saudara/I berkenan hadir dan memberikan do’a restu kepada kedua mempelai.
                Atas kehadiran dan do’a restunya kami ucapkan terimakasih
                Wassalamu’alaikum Warahmatullahi Wabarakatuh.
                Kami yang berbahagia</p>
            <br>
        </div>
    <?php } ?>
</section>
<!-- akhir sampul  -->