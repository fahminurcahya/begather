<section id="galery" class="pt-5  ps-3 pe-3">
    <div class="container">
        <div class="slider">
            <?php foreach ($album as $key => $data) {  ?>
                <div>
                    <img data-lazy="<?php echo base_url() ?>/assets/users/<?php echo $kunci . '/' . $data['album']; ?>.png" alt="Image 1">
                </div>
            <?php } ?>
        </div>


        <div class="slider-thumbnails">
            <?php foreach ($album as $key => $data) {  ?>
                <div>
                    <img src="<?php echo base_url() ?>/assets/users/<?php echo $kunci . '/' . $data['album']; ?>.png" alt="Image 1">
                </div>
            <?php } ?>
        </div>
    </div>

</section>