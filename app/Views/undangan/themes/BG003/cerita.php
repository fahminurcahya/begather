    <!-- cerita  -->
    <section id="cerita" class="bg-mempelai">

        <div class="borid  pt-5 mb-2" data-aos="fade-in">
            <div class="container  pb-5 frame-cerita">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2>Cerita Kita</h1>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-12">
                        <div class="main-timeline">
                            <div class="timeline">
                                <?php
                                $no = 0;
                                foreach ($cerita as $key => $data) {
                                    $no++;
                                    if ($no % 2 == 0) { ?>
                                        <div class="timeline">
                                            <div class="timeline-icon"></div>
                                            <div class="timeline-content right" data-aos="zoom-in-up" data-aos-duration="2000">
                                                <span class="date"><?php echo $data['tanggal_cerita']; ?></span>
                                                <h4 class="title"><?php echo $data['judul_cerita']; ?></h4>
                                                <p class="description">
                                                    <?php echo $data['isi_cerita']; ?>
                                                </p>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="timeline">
                                            <div class="timeline-icon"></div>
                                            <div class="timeline-content" data-aos="zoom-in-up" data-aos-duration="2000">
                                                <span class="date"><?php echo $data['tanggal_cerita']; ?></span>
                                                <h4 class="title"><?php echo $data['judul_cerita']; ?></h4>
                                                <p class="description">
                                                    <?php echo $data['isi_cerita']; ?>
                                                </p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- akhir cerita  -->