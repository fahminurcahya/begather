    <!-- mempelai  -->
    <section id="mempelai" class="pt-5 pb-5 ps-3 pe-3 bg-mempelai">
        <?php foreach ($mempelai->getResult() as $row) {  ?>
            <div class="bingkai">
                <div id="bingkai-kiri-atas" data-aos="fade-in">
                    <img src="<?php echo base_url() ?>/assets/themes/BG002/assets/img/kiri-atas.png" width="200px" alt="image">
                </div>
                <div id="bingkai-kanan-atas" data-aos="fade-in">
                    <img src="<?php echo base_url() ?>/assets/themes/BG002/assets/img/kanan-atas.png" width="200px" alt="image">
                </div>
                <div id="bingkai-bawah" data-aos="fade-in">
                    <img src="<?php echo base_url() ?>/assets/themes/BG002/assets/img/tengah.png" width="300px" alt="image">
                </div>
            </div>
            <div class="borid" data-aos="fade-in">
                <div class="container text-center frame">
                    <div class="row" data-aos="fade-down">
                        <div class="col-sm-12" id="font2">
                            Assalamualaikum warahmatullohi wabarokatuh
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;" data-aos="fade-down">
                        <div class="col-sm-12" id="font2">
                            <?= $salam_pembuka ?>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;" data-aos="zoom-in-up" data-aos-duration="2000">
                        <div class="col-sm-12">
                            <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/groom.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/male.png'" alt="image" width="150px" class="rounded-circle img-thumbnail">
                        </div>
                        <div class="col-sm-12">
                            <h2><?php echo $row->nama_pria; ?></h2>
                        </div>
                        <div class="col-sm-12">
                            <?php echo "Putra " . $row->nama_ayah_pria . " dan " . $row->nama_ibu_pria  ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-3 mb-3" id="font2" data-aos="fade-down">
                            Dengan
                        </div>
                    </div>
                    <div class="row" data-aos="zoom-in-up" data-aos-duration="2000">
                        <div class="col-sm-12">
                            <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/bride.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/female.png'" alt="image" width="150px" class="rounded-circle img-thumbnail">
                        </div>
                        <div class="col-sm-12">
                            <h2><?php echo $row->nama_wanita; ?></h2>
                        </div>
                        <div class="col-sm-12">
                            <?php echo "Putri " . $row->nama_ayah_wanita . " dan " . $row->nama_ibu_wanita  ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <hr>
                        </div>
                        <div class="col-2">

                        </div>
                        <div class="col-5">
                            <hr>
                        </div>
                    </div>
                    <div class="row" data-aos="fade-down">
                        <div class="col" id="font2">
                            <span id="resepsi-tanggal">
                        </div>
                    </div>
                    <div class="row" data-aos="fade-down">
                        <div class="container font2" id="font2">
                            <li id="cutd"><span id="days"></span>Hari</li>
                            <li id="cutd"><span id="hours"></span>Jam</li>
                            <li id="cutd"><span id="minutes"></span>Menit</li>
                            <li id="cutd"><span id="seconds"></span>Detik</li>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

    </section>
    <!-- akhir mempelai  -->