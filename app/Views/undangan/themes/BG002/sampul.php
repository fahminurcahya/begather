<section class="sampul text-center bg-sampul" id="sampul" style="color: #000000;">
    <?php foreach ($mempelai->getResult() as $row) {  ?>

        <div class="bingkai">
            <div id="bingkai-kiri-atas" data-aos="fade-in">
                <img src="<?php echo base_url() ?>/assets/themes/BG002/assets/img/kiri-atas.png" width="200px" alt="image">
            </div>
            <div id="bingkai-kanan-atas" data-aos="fade-in">
                <img src="<?php echo base_url() ?>/assets/themes/BG002/assets/img/kanan-atas.png" width="200px" alt="image">
            </div>
            <div id="bingkai-bawah" data-aos="fade-in">
                <img src="<?php echo base_url() ?>/assets/themes/BG002/assets/img/tengah.png" width="300px" alt="image">
            </div>
        </div>
        <div style="position: relative;text-align: center;display:block; padding-top:150px">
            <img src="<?php echo base_url() ?>/assets/themes/BG002/assets/img/bunga1.png" class="cover-border" /><br>
            <!-- image border cover -->
            <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/kita.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/kita.png'" alt="image" class="cover-foto rounded-circle img-thumbnail" />
        </div>
        <br><br>
        <h1 class="display-3" data-aos="zoom-in-up" style="font-size:25pt ;">The Wedding Of</h1>
        <p class="lead" data-aos="zoom-in-up" data-aos-duration="2000"><?php echo $row->nama_panggilan_pria; ?> & <?php echo $row->nama_panggilan_wanita; ?></p>
    <?php } ?>

</section>
<!-- akhir sampul  -->