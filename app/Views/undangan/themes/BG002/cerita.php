    <!-- cerita  -->

    <section id="cerita" class="bg-mempelai">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path fill="#9e5a3f" fill-opacity="1" d="M0,128L21.8,149.3C43.6,171,87,213,131,213.3C174.5,213,218,171,262,176C305.5,181,349,235,393,229.3C436.4,224,480,160,524,160C567.3,160,611,224,655,224C698.2,224,742,160,785,154.7C829.1,149,873,203,916,218.7C960,235,1004,213,1047,202.7C1090.9,192,1135,192,1178,176C1221.8,160,1265,128,1309,112C1352.7,96,1396,96,1418,96L1440,96L1440,0L1418.2,0C1396.4,0,1353,0,1309,0C1265.5,0,1222,0,1178,0C1134.5,0,1091,0,1047,0C1003.6,0,960,0,916,0C872.7,0,829,0,785,0C741.8,0,698,0,655,0C610.9,0,567,0,524,0C480,0,436,0,393,0C349.1,0,305,0,262,0C218.2,0,175,0,131,0C87.3,0,44,0,22,0L0,0Z"></path>
        </svg>


        <div class="borid mb-2" data-aos="fade-in">
            <div class="container pb-5 frame-cerita">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2 style="text-align: center;">Cerita Kita</h2>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-12">
                        <div class="main-timeline">
                            <div class="timeline">
                                <?php
                                $no = 0;
                                foreach ($cerita as $key => $data) {
                                    $no++;
                                    if ($no % 2 == 0) { ?>
                                        <div class="timeline">
                                            <div class="timeline-icon"></div>
                                            <div class="timeline-content right" data-aos="zoom-in-up" data-aos-duration="2000">
                                                <span class="date"><?php echo $data['tanggal_cerita']; ?></span>
                                                <h4 class="title"><?php echo $data['judul_cerita']; ?></h4>
                                                <p class="description">
                                                    <?php echo $data['isi_cerita']; ?>
                                                </p>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="timeline">
                                            <div class="timeline-icon"></div>
                                            <div class="timeline-content" data-aos="zoom-in-up" data-aos-duration="2000">
                                                <span class="date"><?php echo $data['tanggal_cerita']; ?></span>
                                                <h4 class="title"><?php echo $data['judul_cerita']; ?></h4>
                                                <p class="description">
                                                    <?php echo $data['isi_cerita']; ?>
                                                </p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- akhir cerita  -->