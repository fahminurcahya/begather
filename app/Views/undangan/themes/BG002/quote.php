<section id="quotes" class="pt-3 pb-3 ps-1 pe-1">
    <div class="borid" data-aos="fade-in">
        <div class="container text-center  frame-quote">
            <div class="row">
                <div class="col-sm-12 mt-2">
                    <p style="font-size: 12px;">
                        <?= $quote ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>