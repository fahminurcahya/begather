   <!-- cover  -->
   <section class="cover-undangan text-center" id="cover-undangan" style="color: #9e5a3f;">
       <?php foreach ($mempelai->getResult() as $row) {  ?>
           <div class="bingkai">
               <div id="bingkai-kiri-atas" data-aos="fade-in">
                   <img src="<?php echo base_url() ?>/assets/themes/BG002/assets/img/kiri-atas.png" width="200px" alt="image">
               </div>
               <div id="bingkai-kanan-atas" data-aos="fade-in">
                   <img src="<?php echo base_url() ?>/assets/themes/BG002/assets/img/kanan-atas.png" width="200px" alt="image">
               </div>
           </div>
           <h1 class="display-3" data-aos="zoom-in-up">The Wedding Of</h1>
           <div style="position: relative;text-align: center;display:block;margin-top:15px">
               <img src="<?php echo base_url() ?>/assets/themes/BG002/assets/img/bunga1.png" class="cover-border" /><br>
               <!-- image border cover -->
               <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/kita.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/kita.png'" alt="image" class="cover-foto rounded-circle img-thumbnail" />
           </div> <br><br>
           <p class="lead" data-aos="zoom-in-up" data-aos-duration="2000"><?php echo $row->nama_panggilan_pria; ?> & <?php echo $row->nama_panggilan_wanita; ?></p>
           <br>
           <div id="tamu" style="line-height: 50%;">

               <p>Kepada Yth Bapak/Ibu/Saudara/i<br>
               <p id="kepada"></p>
               </p>
               <p style="font-size: 8pt;">mohon maaf apabila ada kesalahan nama dan gelar</p>
           </div>

       <?php } ?>
       <button type="button" class="btn buka" style="background-color: #9e5a3f; color:white" onclick="play()">Buka Undangan</button>
   </section>
   <!-- akhir cover  -->