<section class="text-center pt-5 bg-mempelai" id="penutup">
    <?php foreach ($mempelai->getResult() as $row) {  ?>

        <!-- <div class="bingkai">
        <div id="bingkai-kiri-atas" data-aos="fade-in">
            <img src="assets/img/kiri-atas.png" width="150px" alt="image">
        </div>
        <div id="bingkai-kanan-atas" data-aos="fade-in">
            <img src="assets/img/kanan-atas.png" width="150px" alt="image">
        </div>
    </div> -->
        <div data-aos="fade-in">
            <div class="container pb-5 frame">
                <div style="text-align: center;" class="mt-5">
                    <img src="<?php echo base_url() ?>/assets/themes/BG002/assets/img/bunga1.png" class="cover-border" /><br>
                    <!-- image border cover -->
                    <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/kita.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/kita.png'" class="cover-foto rounded-circle img-thumbnail" />
                </div>

                <br><br>
                <p>Merupakan suatu kehormatan dan kebahagiaan bagi kami apabila Bapak/Ibu/Saudara/I berkenan hadir dan memberikan do’a restu kepada kedua mempelai.
                    Atas kehadiran dan do’a restunya kami ucapkan terimakasih
                    Wassalamu’alaikum Warahmatullahi Wabarakatuh.
                    Kami yang berbahagia</p>
                <br>
                <p class="lead" data-aos="zoom-in-up" data-aos-duration="2000"><?php echo $row->nama_panggilan_pria; ?> & <?php echo $row->nama_panggilan_wanita; ?></p>
            </div>
        </div>
    <?php } ?>

</section>
<!-- akhir sampul  -->