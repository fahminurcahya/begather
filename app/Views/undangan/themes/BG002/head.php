<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?= base_url('assets/base/img/icon.jpeg') ?>">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/themes/BG002/assets/style-icon.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/themes/BG002/assets/menu.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/themes/BG002/assets/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/themes/BG002/assets/timeline.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/themes/BG002/assets/komen.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/themes/BG002/assets/style-galery.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/themes/BG002/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/themes/BG002/assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />



    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.1/font/bootstrap-icons.css">


    <script type="text/javascript" src="<?php echo base_url() ?>/assets/themes/BG002/assets/jquery1.4.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>/assets/themes/BG002/assets/fancybox/jquery.fancybox-1.3.4.js"></script>

    <title>begather.id</title>
</head>