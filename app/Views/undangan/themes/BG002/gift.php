<section id="gift" class=" bg-mempelai pb-5 ps-3 pe-3">
    <div class="bingkai">
        <div id="bingkai-kiri-bawah" data-aos="fade-in" data-aos-duration="3000">
            <img src="<?php echo base_url() ?>/assets/themes/BG002/assets/img/kiri-bawah.png" width="150px" alt="image">
        </div>
        <div id="bingkai-kanan-bawah" data-aos="fade-in" data-aos-duration="3000">
            <img src="<?php echo base_url() ?>/assets/themes/BG002/assets/img/kanan-bawah.png" width="150px" alt="image">
        </div>
    </div>
    <div data-aos="fade-in">
        <div class="container pb-5 frame">
            <h2 style="text-align: center"> Amplop Digital </h2>
            <br>
            <div class=" row">
                <div class="col-sm-12">
                    <?php foreach ($dompet->getResult() as $row) { ?>
                        <div id="card-gift" data-aos="zoom-in-up" data-aos-duration="2000">
                            <img src="<?php echo base_url() ?>/assets/themes/base/img/<?php echo $row->bank ?>.png" alt="image" width="100px">
                            <hr>
                            <p>Nomor Rekening <?php echo $row->nomor ?></p>
                            <p>a/n <?php echo $row->nama ?></p>
                            <button style="border-radius: 25px; background-color: #9e5a3f; color:#fff" type="button" class="btn" onclick="salinDompet(<?php echo $row->nomor ?>)"><i class="fa fa-copy"></i> Salin</button>
                        </div>
                        <br>
                    <?php } ?>
                    <?php foreach ($hadiah->getResult() as $row) { ?>
                        <div id="card-gift" data-aos="zoom-in-up" data-aos-duration="2000">
                            <p>Kirim Hadiah</p>
                            <hr>
                            <p><?php echo $row->alamat ?></p>
                            <p>Penerima <?php echo $row->penerima ?> (<?php echo $row->nomor_hp ?>)</p>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>


</section>