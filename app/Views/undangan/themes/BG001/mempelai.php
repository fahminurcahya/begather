<section id="mempelai" class="pt-5 pb-5 ps-3 pe-3">
    <?php foreach ($mempelai->getResult() as $row) {  ?>
        <div class="bingkai">
            <div id="bingkai-kiri-atas">
                <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kiri-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kiri-bawah">
                <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kanan-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kanan-atas">
                <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kanan-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kanan-bawah">
                <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kiri-atas.png" width="300px" alt="image">
            </div>
        </div>

        <div class="borid" data-aos="fade-in">
            <div class="container text-center frame">
                <div class="row" data-aos="fade-down">
                    <div class="col-sm-12" id="font2">
                        Assalamualaikum warahmatullohi wabarokatuh
                    </div>
                </div>
                <div class="row" style="margin-top: 20px;" data-aos="fade-down">
                    <div class="col-sm-12" id="font2">
                        <?= $salam_pembuka ?>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px;" data-aos="zoom-in-up" data-aos-duration="2000">
                    <div class="col-sm-12">
                        <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/groom.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/male.png'" alt="image" width="150px" class="rounded-circle img-thumbnail">
                    </div>
                    <div class="col-sm-12">
                        <h1 style="color:#c47832; font-size:20pt"><?php echo $row->nama_pria; ?></h1>
                    </div>
                    <div class="col-sm-12">
                        <?php echo "Putra " . $row->nama_ayah_pria . " dan " . $row->nama_ibu_pria  ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 mt-3 mb-3" id="cinzel-b" data-aos="fade-down">
                        Dengan
                    </div>
                </div>
                <div class="row" data-aos="zoom-in-up" data-aos-duration="2000">
                    <div class="col-sm-12">
                        <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/bride.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/female.png'" alt="image" width="150px" class="rounded-circle img-thumbnail">
                    </div>
                    <div class="col-sm-12">
                        <h1 style="color:#c47832; font-size:20pt"><?php echo $row->nama_wanita; ?></h1>
                    </div>
                    <div class="col-sm-12">
                        <?php echo "Putri " . $row->nama_ayah_wanita . " dan " . $row->nama_ibu_wanita  ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</section>