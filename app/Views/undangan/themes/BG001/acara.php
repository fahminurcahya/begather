<section id="acara" class="pt-5 pb-5 ps-3 pe-3">
    <?php
    foreach ($acara->getResult() as $row) {
        $tanggal_akad =  $row->tanggal_akad;
        $tanggal_resepsi =  $row->tanggal_resepsi;
    ?>
        <div class="bingkai">
            <div id="bingkai-kiri-atas">
                <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kiri-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kiri-bawah">
                <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kanan-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kanan-atas">
                <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kanan-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kanan-bawah">
                <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kiri-atas.png" width="300px" alt="image">
            </div>
        </div>
        <div class="borid" data-aos="fade-in">
            <div class="container text-center frame">
                <div class="row mt-2">
                    <div class="col-sm-12">
                        <div class="borid mb-3" data-aos="zoom-in-up">
                            <div class="container text-center  frame-acara-detail mb-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h2>Akad</h2>
                                        <hr>
                                        <div class="row text-start">
                                            <table>
                                                <tr>
                                                    <td>Tanggal</td>
                                                    <td>:</td>
                                                    <td><span id="view-tanggal-akad"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Jam</td>
                                                    <td>:</td>
                                                    <td><?php echo $row->jam_akad; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Tempat</td>
                                                    <td>:</td>
                                                    <td><?php echo $row->alamat_akad; ?></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="borid" data-aos="zoom-in-up">
                            <div class="container text-center  frame-acara-detail">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h2>Resepsi</h2>
                                        <hr>
                                        <div class="row text-start">
                                            <table>
                                                <tr>
                                                    <td>Tanggal</td>
                                                    <td>:</td>
                                                    <td><span id="view-tanggal-resepsi"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Jam</td>
                                                    <td>:</td>
                                                    <td><?php echo $row->jam_resepsi; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Tempat</td>
                                                    <td>:</td>
                                                    <td><?php echo $row->alamat_resepsi; ?></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt">
                    <?php if ($maps != "") {
                        $map = str_replace('width="400"', 'id="iframe_map" style="border:3px solid #888888; box-shadow: 5px 10px 18px #888888; border-radius: 25px; display:none"', "$maps");
                    ?>
                        <div class="col-sm-12" data-aos="zoom-in-up" data-aos-duration="2000">
                            <div>
                                <?php echo $map; ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="row mt-3">
                    <div class="justify-content-md-center col-lg-12">
                        <?php if ($maps != "") { ?>
                            <button type="button" class="btn" style="background-color: #c47832; color:#ffff; border:1px solid #888888; box-shadow: 5px 10px 18px #888888;" onclick="lihatMap()"><i class="fa fa-map-marker"></i> Lihat Map</button>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
</section>