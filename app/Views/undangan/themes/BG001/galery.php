<section class="gallery-section section-padding pt-5 pb-5 ps-3 pe-3" id="gallery">
    <div class="bingkai">
        <div id="bingkai-kiri-atas">
            <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kiri-atas.png" width="300px" alt="image">
        </div>
        <div id="bingkai-kiri-bawah">
            <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kanan-atas.png" width="300px" alt="image">
        </div>
        <div id="bingkai-kanan-atas">
            <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kanan-atas.png" width="300px" alt="image">
        </div>
        <div id="bingkai-kanan-bawah">
            <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kiri-atas.png" width="300px" alt="image">
        </div>
    </div>
    <div class="borid" data-aos="fade-in">
        <div class="container-sm frame">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-title text-center mb-5 mt-5">
                        <h1 class="display-6" style="color:#c47832;">Gallery Foto Kami</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="gallery-grids">
                        <?php foreach ($album as $key => $data) {  ?>
                            <div class="grid" data-aos="fade-up" data-aos-anchor-placement="center-bottom" data-aos-duration="2000">
                                <a id="example4" href="<?php echo base_url() ?>/assets/users/<?php echo $kunci . '/' . $data['album'] ?>.png" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url() ?>/assets/users/<?php echo $kunci . '/' . $data['album']; ?>.png" alt>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php if ($youtube != "") {
                        $embed = str_replace("youtu.be", "www.youtube.com/embed", "$youtube");
                    ?>
                        <div class="youtube" data-aos="fade-up" data-aos-anchor-placement="center-bottom" data-aos-duration="2000">
                            <iframe style="width: 280px; " src="<?php echo $embed; ?>" allow="fullscreen">
                            </iframe>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>