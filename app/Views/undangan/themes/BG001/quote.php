<section id="quotes" class="pt-5 pb-5 ps-3 pe-3">
    <div class="bingkai">
        <div id="bingkai-kiri-atas">
            <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kiri-atas.png" width="300px" alt="image">
        </div>
        <div id="bingkai-kiri-bawah">
            <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kanan-atas.png" width="300px" alt="image">
        </div>
        <div id="bingkai-kanan-atas">
            <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kanan-atas.png" width="300px" alt="image">
        </div>
        <div id="bingkai-kanan-bawah">
            <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kiri-atas.png" width="300px" alt="image">
        </div>
    </div>
    <div class="borid" data-aos="fade-in">
        <div class="container text-center  frame">
            <div class="row">
                <div class="col-sm-12" id="font" style="color:#c47832;">
                    <?= $quote ?>
                </div>
            </div>
        </div>
    </div>
</section>