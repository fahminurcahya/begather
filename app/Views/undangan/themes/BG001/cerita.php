<section id="cerita" class="pt-5 pb-5 ps-3 pe-3">
    <div class="bingkai">
        <div id="bingkai-kiri-atas">
            <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kiri-atas.png" width="300px" alt="image">
        </div>
        <div id="bingkai-kiri-bawah">
            <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kanan-atas.png" width="300px" alt="image">
        </div>
        <div id="bingkai-kanan-atas">
            <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kanan-atas.png" width="300px" alt="image">
        </div>
        <div id="bingkai-kanan-bawah">
            <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kiri-atas.png" width="300px" alt="image">
        </div>
    </div>
    <div class="borid mb-2" data-aos="fade-in">
        <div class="container pb-5 frame">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1 class="display-6" style="color:#c47832">Cerita Kita</h1>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-sm-12">
                    <div class="main-timeline">
                        <?php
                        $no = 0;
                        foreach ($cerita as $key => $data) {
                            $no++;
                            if ($no % 2 == 0) { ?>
                                <div class="timeline">
                                    <div class="timeline-icon"></div>
                                    <div class="timeline-content right" data-aos="zoom-in-up" data-aos-duration="2000">
                                        <span class="date"><?php echo $data['tanggal_cerita']; ?></span>
                                        <h4 class="title"><?php echo $data['judul_cerita']; ?></h4>
                                        <p class="description">
                                            <?php echo $data['isi_cerita']; ?>
                                        </p>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="timeline">
                                    <div class="timeline-icon"></div>
                                    <div class="timeline-content" data-aos="zoom-in-up" data-aos-duration="2000">
                                        <span class="date"><?php echo $data['tanggal_cerita']; ?></span>
                                        <h4 class="title"><?php echo $data['judul_cerita']; ?></h4>
                                        <p class="description">
                                            <?php echo $data['isi_cerita']; ?>
                                        </p>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>