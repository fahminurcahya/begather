<section class="sampul text-center bg-sampul" id="sampul" style="color: #f6a454;">
    <?php foreach ($mempelai->getResult() as $row) {  ?>
        <div class="bingkai">
            <div id="bingkai-kiri-atas">
                <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kiri-atas.png" width="300px" alt="image">
            </div>

            <div id="bingkai-kanan-atas">
                <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kanan-atas.png" width="300px" alt="image">
            </div>
            <div id="bingkai-kiri-bawah">
                <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kanan-atas.png" width="300px" alt="image">
            </div>

            <div id="bingkai-kanan-bawah">
                <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kiri-atas.png" width="300px" alt="image">
            </div>
        </div>
        <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/kita.png" alt="image" width="200px" onerror="this.src='<?php echo base_url() ?>/assets/base/img/kita.png'" class="rounded-circle img-thumbnail" data-aos="fade-in">
        <br><br>
        <h1 class="display-3" data-aos="zoom-in-up">The Wedding Of</h1>
        <br>
        <p class="lead" data-aos="zoom-in-up" data-aos-duration="2000"><?php echo $row->nama_panggilan_pria; ?>
            & <?php echo $row->nama_panggilan_wanita; ?></p>
        <div class="row">
            <div class="col-5">
                <hr>
            </div>
            <div class="col-2">

            </div>
            <div class="col-5">
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col" id="cinzel-b">
                <span id="resepsi-tanggal">
            </div>
        </div>
        <div class="row" data-aos="fade-down">
            <div class="container font2" id="font2">
                <li id="cutd">
                    <div class="count-down-box">
                        <div class="count-down" id="font2">
                            <span id="days"></span><span style="font-size:10pt;">Hari</span>
                        </div>
                    </div>
                </li>
                <li id="cutd">
                    <div class="count-down-box">
                        <div class="count-down" id="font2">
                            <span id="hours"></span><span style="font-size:10pt;">Jam</span>
                        </div>
                    </div>
                </li>
                <li id="cutd">
                    <div class="count-down-box">
                        <div class="count-down" id="font2">
                            <span id="minutes"></span><span style="font-size:10pt;">Menit</span>
                        </div>
                    </div>
                </li>
                <li id="cutd">
                    <div class="count-down-box">
                        <div class="count-down" id="font2">
                            <span id="seconds"></span><span style="font-size:10pt;">Detik</span>
                        </div>
                    </div>
                </li>
            </div>
        </div>
    <?php } ?>
</section>