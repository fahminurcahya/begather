<section id="gift" class="pt-5 pb-5 ps-3 pe-3">
    <div class="borid" data-aos="fade-in">
        <div class="container text-center frame">
            <div class="row mb-3">
                <div class="col-sm-12 text-center" id="font2">
                    <h1 class="display-6" style="color:#c47832">Kirim Hadiah</h1>
                    <hr>
                </div>
            </div>
            <div class=" row">
                <div class="col-sm-12">
                    <?php foreach ($dompet->getResult() as $row) { ?>
                        <div id="card-gift" data-aos="zoom-in-up" data-aos-duration="2000">
                            <img src="<?php echo base_url() ?>/assets/themes/base/img/<?php echo $row->bank ?>.png" alt="image" width="100px">
                            <hr>
                            <p>Nomor Rekening <?php echo $row->nomor ?></p>
                            <p>a/n <?php echo $row->nama ?></p>
                            <button style="border-radius: 25px; background-color: #c47832; color:#fff" type="button" class="btn" onclick="salinDompet(<?php echo $row->nomor ?>)"><i class="fa fa-copy"></i> Salin</button>
                        </div>
                        <br>
                    <?php } ?>
                    <?php foreach ($hadiah->getResult() as $row) { ?>
                        <div id="card-gift" data-aos="zoom-in-up" data-aos-duration="2000">
                            <p>Kirim Hadiah</p>
                            <hr>
                            <p><?php echo $row->alamat ?></p>
                            <p>Penerima <?php echo $row->penerima ?> (<?php echo $row->nomor_hp ?>)</p>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>