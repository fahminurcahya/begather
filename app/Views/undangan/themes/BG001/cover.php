<section class="cover-undangan text-center bg-sampul" id="cover-undangan" style="color: #f6a454;">
    <?php foreach ($mempelai->getResult() as $row) {  ?>
        <h1 class="display-3" data-aos="zoom-in-up">The Wedding Of</h1>
        <br>
        <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/kita.png" alt="image" onerror="this.src='<?php echo base_url() ?>/assets/base/img/kita.png'" width="180px" class="rounded-circle img-thumbnail" data-aos="fade-in">
        <br><br>
        <p class="lead" data-aos="zoom-in-up" data-aos-duration="2000"><?php echo $row->nama_panggilan_pria; ?> & <?php echo $row->nama_panggilan_wanita; ?></p>
        <br>
        <div id="tamu" style="line-height: 50%;">
            <p>Kepada Yth Bapak/Ibu/Saudara/i</p>
            <p id="kepada"></p>
            <p style="font-size: 8pt;">mohon maaf apabila ada kesalahan nama dan gelar</p>
        </div>

        <br>
    <?php } ?>
    <button type="button" class="btn buka" style="background-color: #c47832; color: #ffff" onclick="play()">Buka Undangan</button>
</section>