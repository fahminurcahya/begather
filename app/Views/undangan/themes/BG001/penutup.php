<section class="sampul text-center bg-sampul" id="penutup" style="color: #f6a454;">
    <?php foreach ($mempelai->getResult() as $row) {  ?>
        <div class="bingkai">
            <div id="bingkai-kiri-atas">
                <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kiri-atas.png" width="300px" alt="image">
            </div>

            <div id="bingkai-kanan-atas">
                <img src="<?php echo base_url() ?>/assets/themes/BG001/assets/img/kanan-atas.png" width="300px" alt="image">
            </div>

        </div>
        <h1 class="display-3">Kami Yang Berbahagia</h1>
        <br>
        <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/kita.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/kita.png'" alt="image" width="200px" class="rounded-circle img-thumbnail" data-aos="fade-in">
        <br><br>
        <p class="lead" style="font-size: 20pt;"><?php echo $row->nama_panggilan_pria; ?> & <?php echo $row->nama_panggilan_wanita; ?></p>
    <?php } ?>
</section>