<!doctype html>
<html lang="en">
<?php echo view('undangan/themes/BG002/head'); ?>


<body class="stop-scrolling">
    <div id="loading"></div>
    <div class="page" style="display: none ;">
        <div class="kc_fab_main_btn">
            <img src="<?php echo base_url() ?>/assets/themes/BG002/assets/img/soundon.png" onclick='toggleSound(this);' width=80%>
        </div>

        <?php foreach ($data->getResult() as $row) {
            $kunci = $row->kunci;
            $youtube = $row->video;
            $salam_pembuka = $row->salam_pembuka;
            $musiknya = "/assets/users/" . $kunci . "/musik.mp3";
            $maps = $row->maps;
            $quote = $row->quote;
        }

        ?>
        <?php foreach ($rules->getResult() as $row) {
            $gallery = $row->gallery;
            $ceritarule = $row->cerita;
            $lokasi = $row->lokasi;
            $akad = $row->akad;
            $resepsi = $row->resepsi;
            $setQuote = $row->quote;
        }


        ?>
        <?php foreach ($acara->getResult() as $row) {
            $tanggal_resepsi = $row->tanggal_resepsi;
            $tanggal_akad = $row->tanggal_akad;

        ?>
        <?php } ?>
        <audio loop src="<?php echo base_url() ?><?= $musiknya ?>" id="audio"></audio>
        <input id="tanggal-resepsi" type="text" value="<?php echo $tanggal_resepsi ?>" hidden>
        <input id="tanggal-akad" type="text" value="<?php echo $tanggal_akad ?>" hidden>


        <?php echo view('undangan/themes/BG002/cover', ['kunci' => $kunci]); ?>

        <div class="main" style="display: none ;">

            <!--=============== Sampul ===============-->
            <?php echo view('undangan/themes/BG002/sampul', ['kunci' => $kunci]); ?>

            <h2 style="text-align: center; color:#ffff"> Our Special Day </h2>
            <!--=============== Mempelai ===============-->
            <?php echo view('undangan/themes/BG002/mempelai', ['salam_pembuka' => $salam_pembuka]); ?>


            <!--=============== Quotes ===============-->
            <?php if ($setQuote == 1) { ?>
                <?php echo view('undangan/themes/BG002/quote', ['quote' => $quote]); ?>
            <?php } ?>



            <!--=============== Acara ===============-->
            <?php echo view('undangan/themes/BG002/acara', ['maps' => $maps]); ?>


            <!--=============== Galeri ===============-->
            <?php if ($gallery == 1) { ?>
                <?php echo view('undangan/themes/BG002/galery', ['youtube' => $youtube]); ?>
            <?php } ?>



            <!--=============== Cerita ===============-->
            <?php if ($ceritarule == '1') { ?>
                <?php echo view('undangan/themes/BG002/cerita'); ?>
            <?php } ?>


            <!-- <h2 style="text-align: center; color:#ffff"> Ucapkan Sesuatu </h2> -->

            <!--=============== Komentar ===============-->
            <?php echo view('undangan/themes/BG002/komentar'); ?>


            <!--=============== Gift ===============-->
            <?php echo view('undangan/themes/BG002/gift'); ?>


            <!--=============== Penutup ===============-->
            <?php echo view('undangan/themes/BG002/penutup'); ?>


            <!--=============== Navigasi ===============-->
            <?php echo view('undangan/themes/BG002/navbar', ['rules' => $rules]); ?>

            <div class="modal fade" id="prokes" tabindex="-1" role="dialog" aria-labelledby="prokesLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <img src="<?= base_url() ?>/assets/base/img/prokes.jpg" alt="image" width="100%" data-aos="fade-in">
                        </div>
                    </div>
                </div>
            </div>


            <!--=============== CONTACTME ===============-->
            <section id="footer">
                <div style="text-align: center; color:#fff">Design by Begather</div>
            </section>
        </div>
    </div>





    <!--=============== MAIN JS ===============-->
    <!-- <script src="assets/main.js"></script> -->
</body>

<!-- animasi aos  -->
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="<?= base_url() ?>/assets/base/js/moment-with-locales.js"></script>

<script>
    AOS.init({
        // once : true,
        // duration: 2000,
    });
    $(document).ready(function() {
        // $('.slider').slick({
        //     // autoplay: true,
        //     // autoplaySpeed: 2500,
        // });
    })


    function onReady(callback) {
        var intervalId = window.setInterval(function() {
            if (document.getElementsByTagName('body')[0] !== undefined) {
                window.clearInterval(intervalId);
                callback.call(this);
            }
        }, 5000);
    }

    function setVisible(selector, visible) {
        document.querySelector(selector).style.display = visible ? 'block' : 'none';
    }

    onReady(function() {
        setVisible('.page', true);
        setVisible('#loading', false);
    });



    var pc1 = document.getElementById('cover-undangan');
    let url_string = window.location.href
    let url = new URL(url_string);
    let to = url.searchParams.get("to");
    document.getElementById('kepada').innerHTML = to;

    function play() {
        setVisible('.main', true);
        pc1.classList.toggle('hide');
        var audio = document.getElementById("audio");
        audio.play();
        $('body').removeClass('stop-scrolling')
        setTimeout(function() {
            pc1.style.display = 'none';
            setTimeout(function() {
                $('#prokes').modal('show');
            }, 3000); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
        }, 2000);

        $('#nama').val(to);
    }

    // untuk copy nomor rek/hp
    function salinDompet($value) {
        /* Copy the text inside the text field */
        navigator.clipboard.writeText($value);

    }

    // untuk men switch mute/unmute
    function toggleSound(img) {
        let mus = document.getElementById("audio");
        if (mus.muted) {
            img.src = "<?php echo base_url() ?>/assets/themes/BG002/assets/img/soundon.png"
            mus.muted = false;
        } else {
            img.src = "<?php echo base_url() ?>/assets/themes/BG002/assets/img/soundoff.png"
            mus.muted = true;
        }
    }



    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        lazyLoad: 'ondemand',
        fade: true,
        asNavFor: '.slider-thumbnails'
    });

    $('.slider-thumbnails').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        asNavFor: '.slider',
        dots: true,
        centerMode: true,
        focusOnSelect: true
    });

    // function untuk scroll
    $('.nav__link').click(function(e) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top,
                behavior: 'smooth'
            });
        } // End if
    })

    // function untuk menghitung waktu mundur
    $(function() {
        const second = 1000,
            minute = second * 60,
            hour = minute * 60,
            day = hour * 24;
        let today = new Date(),
            dd = String(today.getDate()).padStart(2, "0"),
            mm = String(today.getMonth() + 1).padStart(2, "0"),
            yyyy = today.getFullYear(),
            dayMonth = "01/10/",
            birthday = dayMonth + '2022';

        let initial = $('#tanggal-resepsi').val().split(/\//);
        let resepsi = [initial[1], initial[2], initial[0]].join('/');
        moment.locale('id');

        document.getElementById("resepsi-tanggal").innerText = moment($('#tanggal-resepsi').val()).format('dddd, Do MMMM YYYY');
        document.getElementById("view-tanggal-resepsi").innerText = moment($('#tanggal-resepsi').val()).format('dddd, Do MMMM YYYY');
        document.getElementById("view-tanggal-akad").innerText = moment($('#tanggal-akad').val()).format('dddd, Do MMMM YYYY');

        today = mm + "/" + dd + "/" + yyyy;

        const countDown = new Date(resepsi).getTime()

        x = setInterval(function() {
            const now = new Date().getTime(),
                distance = countDown - now;
            document.getElementById("days").innerText = Math.floor(distance / (day)),
                document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
                document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
                document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);

            //do something later when date is reached
            if (distance < 0) {
                document.getElementById("days").innerText = "0";
                document.getElementById("hours").innerText = "0";
                document.getElementById("minutes").innerText = "0";
                document.getElementById("seconds").innerText = "0";
                clearInterval(x);
            }
            //seconds
        }, 0)
    }());

    function lihatMap() {
        window.open(document.getElementById("iframe_map").src);
    }

    // function untuk add komentar/submit ucapan
    $('#submitKomen').click(function() {
        $('#loading_').css('display', 'inline');
        $('#submitKomen').css('display', 'none');

        var nama = $('#nama').val();
        var komentar = $('textarea#komentar').val();
        var kehadiran = $('input[name="kehadiran"]:checked').val();

        $.ajax({
            type: 'POST',
            url: "<?= base_url('/add_komentar') ?>",
            data: {
                nama: nama,
                komentar: komentar,
                kehadiran: kehadiran
            },
            async: true,
            dataType: 'html',
            success: function(hasil) {
                console.log(json);
                var json = JSON.parse(hasil);
                var status = json.status;
                var nama = json.nama;
                var komentar = json.komentar;

                if (status == 'sukses') {

                    if (komentar != '') {
                        iconHadir = ''
                        if (kehadiran == 1) {
                            iconHadir = ' <i class="fa fa-check-circle" aria-hidden="true"></i>'
                        } else if (kehadiran == 0) {
                            iconHadir = ' <i class="fa fa-minus-circle" aria-hidden="true"></i>'
                        } else {
                            iconHadir = ' <i class="fa fa-question-circle" aria-hidden="true"></i>'
                        }
                        tagHtml = '<div class="comment mt-2  float-left">' +
                            '<h4 class="komen-nama" >' + nama + iconHadir +
                            '</h4> ' +
                            '<p style="font-size: 8pt;" class="komen-isi"><span style="opacity: 0.5;">Baru Saja</span> <br>' + komentar + '</p>' +
                            '</div><br>'
                        $('.bodykomen').prepend(tagHtml);

                        $(".komen:hidden").slice(0, 100).slideDown();
                    }
                    // $("html, body").animate({
                    //     scrollTop: $('#komentar').scrollTop
                    // }, 1000);
                    $("#loadMore").fadeOut('slow');
                    $('#loading_').css('display', 'none');
                    $('#submitKomen').css('display', 'block');
                    $('#submitKomen').attr('disabled', true);
                } else {
                    alert('gagal');
                }

            }
        });

    });
</script>

<!-- animasi aos  -->

</html>