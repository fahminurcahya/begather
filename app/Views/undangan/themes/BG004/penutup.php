<section class="sampul text-center bg-sampul" id="penutup" style="color: #f6a454;">
    <?php foreach ($mempelai->getResult() as $row) {  ?>
        <div style="position: relative;text-align: center;display:block;margin-top:15px">
            <br>
            <img src="<?php echo base_url() ?>/assets/themes/BG004/assets/img/bingkai.png" class="cover-border" /><br>
            <!-- image border cover -->
            <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/kita.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/kita.png'" class="cover-foto img-thumbnail" />
        </div> <br><br><br>
        <p style="font-size:10pt ;">Merupakan suatu kehormatan dan kebahagiaan bagi kami apabila Bapak/Ibu/Saudara/I berkenan hadir dan memberikan do’a restu kepada kedua mempelai.
            Atas kehadiran dan do’a restunya kami ucapkan terimakasih
            Wassalamu’alaikum Warahmatullahi Wabarakatuh.
            Kami yang berbahagia</p>
        <br>
        <p class="lead" data-aos="zoom-in-up" data-aos-duration="2000"><?php echo $row->nama_panggilan_pria; ?><span><img src="<?php echo base_url() ?>/assets/themes/BG004/assets/img/love.png" width="50px" alt=""></span> <?php echo $row->nama_panggilan_wanita; ?></p>
    <?php } ?>
</section>