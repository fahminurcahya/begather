<section class="gallery-section section-padding  pb-5 ps-3 pe-3" id="gallery">
    <div class="borid" data-aos="fade-in">
        <div class="container-sm frame">
            <div class="row mb-3">
                <div class="col-xs-12">
                    <div class="col-sm-12 text-center">
                        <h2 style="text-align: center; color: #ffff">Gallery Foto Kami</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="gallery-grids">
                        <?php foreach ($album as $key => $data) {  ?>
                            <div class="grid" data-aos="fade-up" data-aos-anchor-placement="center-bottom" data-aos-duration="2000">
                                <a id="example4" href="<?php echo base_url() ?>/assets/users/<?php echo $kunci . '/' . $data['album'] ?>.png" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url() ?>/assets/users/<?php echo $kunci . '/' . $data['album']; ?>.png" alt>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php if ($youtube != "") {
                        $embed = str_replace("youtu.be", "www.youtube.com/embed", "$youtube");
                    ?>
                        <div class="youtube" data-aos="fade-up" data-aos-anchor-placement="center-bottom" data-aos-duration="2000">
                            <iframe style="width: 280px; " src="<?php echo $embed; ?>" allow="fullscreen">
                            </iframe>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $("a#example4").fancybox({
        'opacity': true,
        'overlayShow': true,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic'
    });
</script>