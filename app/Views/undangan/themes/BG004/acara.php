   <!-- acara  -->
   <section id="acara" class="pt-2 pb-2 ps-3 pe-3 bg-mempelai">
       <?php
        foreach ($acara->getResult() as $row) {
            $tanggal_akad =  $row->tanggal_akad;
            $tanggal_resepsi =  $row->tanggal_resepsi;
        ?>
           <div class="bingkai">
               <div id="bingkai-bawah" data-aos="fade-in">
                   <img src="<?php echo base_url() ?>/assets/themes/BG004/assets/img/bawah.png" width="300px" alt="image">
               </div>
           </div>
           <div class="borid" data-aos="fade-in">

               <div class="container text-center frame">
                   <h2>Save The Date</h2>
                   <div class="row mt-2">
                       <div class="col-sm-12">
                           <div class="borid mb-3" data-aos="zoom-in-left">
                               <div class="container text-center  frame-acara">
                                   <div class="row">
                                       <div class="col-sm-12">
                                           <h2>Akad</h2>
                                           <span id="view-tanggal-akad"></span><br>
                                           <?php echo $row->jam_akad; ?><br>
                                           <?php echo $row->alamat_akad; ?>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <div class="borid" data-aos="zoom-in-left">
                               <div class="container text-center  frame-acara">
                                   <div class="row">
                                       <div class="col-sm-12">
                                           <h2>Resepsi</h2>
                                           <span id="view-tanggal-resepsi"></span><br>
                                           <?php echo $row->jam_resepsi; ?><br>
                                           <?php echo $row->alamat_resepsi; ?>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <div class="row mt">
                               <?php if ($maps != "") {
                                    $map = str_replace('width="400"', 'id="iframe_map" style="border:3px solid #888888; box-shadow: 5px 10px 18px #888888; border-radius: 25px; display:none"', "$maps");
                                ?>
                                   <div class="col-sm-12" data-aos="zoom-in-up" data-aos-duration="2000">
                                       <div>
                                           <?php echo $map; ?>
                                       </div>
                                   </div>
                               <?php } ?>
                           </div>
                           <div class="row mt-3" data-aos="zoom-in-left">
                               <div class="justify-content-md-center col-lg-12">
                                   <?php if ($maps != "") { ?>
                                       <button type="button" class="btn" style="background-color: #9e5a3f; border:1px solid #888888; color:#fff; box-shadow: 5px 10px 18px #888888;" onclick="lihatMap()"><i class="fa fa-map-marker"></i> Lihat Map</button>

                                   <?php } ?>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           <?php } ?>
   </section>
   <!-- akhir acara  -->