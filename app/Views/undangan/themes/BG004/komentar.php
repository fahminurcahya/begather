<section id="komentar" class="bg-komentar ps-2 pe-3">
    <div class="bingkai">
        <div id="bingkai-kiri" data-aos="fade-in">
            <img src="<?php echo base_url() ?>/assets/themes/BG004/assets/img/kiri.png" width="150px" alt="image">
        </div>
        <div id="bingkai-kanan" data-aos="fade-in">
            <img src="<?php echo base_url() ?>/assets/themes/BG004/assets/img/kanan.png" width="150px" alt="image">
        </div>
    </div>
    <div class="pb-5" data-aos="fade-in">
        <div class="container pb-5 frame">
            <h2 style="text-align: center;">Ucapkan Sesuatu</h2>
            <br>
            <div class="row">
                <div class="col-sm-12 text-center" id="font2">
                    <p style="color:#ac4e40 ;"> Berikan ucapan terbaik untuk kedua mempelai </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form>
                        <div class="form-group row">
                            <div class="col-sm-10 mb-2">
                                <input type="text" class="form-control" style="background-color:#eae4e1 ;" id="nama" name="nama" placeholder="Nama">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10 mb-2">
                                <textarea class="form-control" id="komentar" style="background-color:#eae4e1 ;" name="komentar" rows="4" placeholder="Ucapan"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="kehadiran" id="kehadiran1" value="1" checked>
                                <label class="form-check-label" for="kehadiran1">
                                    Hadir
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="kehadiran" id="kehadiran2" value="0">
                                <label class="form-check-label" for="kehadiran2">
                                    Tidak Hadir
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="kehadiran" id="kehadiran3" value="2">
                                <label class="form-check-label" for="kehadiran3">
                                    Ragu-Ragu
                                </label>
                            </div>
                        </div>
                        <div class="form-group row pt-3">
                            <div class="col-sm-10">
                                <button type="button" id="submitKomen" class="btn" style="background-color: #9e5a3f; color:#fff"><i class="fa fa-send"></i> Kirim</button>
                                <img src="https://begather.id/assets/base/img/loadinga.svg" height="30px" style="display:none;" id="loading_">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div>
                    <hr>
                </div>
                <div class="col-sm-12 bodykomen">
                    <?php $nomor = 0;
                    foreach ($komen as $key => $data) {
                        $nomor++; ?>
                        <div class="komen">
                            <div class="comment mt-2  float-left">
                                <h4 class="komen-nama"> <?= \esc($data['nama_komentar']); ?>
                                    <?php if ($data['kehadiran'] == 1) { ?>
                                        <i class="fa fa-check-circle" aria-hidden="true"></i>
                                    <?php } else if ($data['kehadiran'] == 0) { ?>
                                        <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                    <?php } else { ?>
                                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                                    <?php } ?>
                                </h4> <br>
                                <p style="font-size: 8pt;" class="komen-isi">
                                    <span style="opacity: 0.5;"><?= \esc($data['created_at']); ?></span> <br>
                                    <?= \esc($data['isi_komentar']); ?>
                                </p>
                            </div>
                        </div>
                        <br>
                    <?php } ?>
                </div>
                <div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</section>