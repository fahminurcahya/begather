   <!-- cover  -->
   <section class="cover-undangan text-center pt-4" id="cover-undangan" style="color: #000000;">
       <?php foreach ($mempelai->getResult() as $row) {  ?>

           <!-- image border cover -->
           <h1 class="display-3" data-aos="zoom-in-up">The Wedding Of</h1>
           <br><br>

           <div style="position: relative;text-align: center;display:block;margin-top:15px">
               <img src="<?php echo base_url() ?>/assets/themes/BG004/assets/img/bingkai.png" class="cover-border" /><br>
               <!-- image border cover -->
               <img src="<?= base_url() ?>/assets/users/<?= $kunci; ?>/kita.png" onerror="this.src='<?php echo base_url() ?>/assets/base/img/kita.png'" class="cover-foto img-thumbnail" />
           </div>
           <br><br>
           <br>
           <p class="lead" data-aos="zoom-in-up" data-aos-duration="2000"><?php echo $row->nama_panggilan_pria; ?><span><img src="<?php echo base_url() ?>/assets/themes/BG004/assets/img/love.png" width="50px" alt=""></span> <?php echo $row->nama_panggilan_wanita; ?></p>
           </p>
           <br>
           <div id="tamu" style="line-height: 50%;">

               <p>Kepada Yth Bapak/Ibu/Saudara/i<br>
               <p id="kepada"></p>
               </p>
               <p style="font-size: 8pt;">mohon maaf apabila ada kesalahan nama dan gelar</p>
           </div>
       <?php } ?>
       <button type="button" class="btn buka" style="background-color: #ffff; color:#ac7864" onclick="play()">Buka Undangan</button>
   </section>
   <!-- akhir cover  -->