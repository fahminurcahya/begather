<section id="gift" class=" bg-mempelai pb-5 ps-3 pe-3">
    <div class="bingkai">
        <div id="bingkai-kiri" data-aos="fade-in">
            <img src="<?php echo base_url() ?>/assets/themes/BG004/assets/img/kiri.png" width="150px" alt="image">
        </div>
        <div id="bingkai-kanan" data-aos="fade-in">
            <img src="<?php echo base_url() ?>/assets/themes/BG004/assets/img/kanan.png" width="150px" alt="image">
        </div>
        <div id="bingkai-bawah" data-aos="fade-in">
            <img src="<?php echo base_url() ?>/assets/themes/BG004/assets/img/bawah.png" width="300px" alt="image">
        </div>
    </div>
    <div data-aos="fade-in">
        <div class="container pb-5 frame">
            <h2 style="text-align: center"> Amplop Digital </h2>
            <br>
            <div class=" row">
                <div class="col-sm-12">
                    <?php foreach ($dompet->getResult() as $row) { ?>
                        <div id="card-gift" data-aos="zoom-in-up" data-aos-duration="2000">
                            <img src="<?php echo base_url() ?>/assets/themes/base/img/<?php echo $row->bank ?>.png" alt="image" width="100px">
                            <hr>
                            <p style="color:#ac4e40 ;">Nomor Rekening <?php echo $row->nomor ?></p>
                            <p style="color:#ac4e40 ;">a/n <?php echo $row->nama ?></p>
                            <button style="border-radius: 25px; background-color: #ac4e40; color:#fff" type="button" class="btn" onclick="salinDompet(<?php echo $row->nomor ?>)"><i class="fa fa-copy"></i> Salin</button>
                        </div>
                        <br>
                    <?php } ?>
                    <?php foreach ($hadiah->getResult() as $row) { ?>
                        <div id="card-gift" data-aos="zoom-in-up" data-aos-duration="2000">
                            <p style="color:#ac4e40 ;">Kirim Hadiah</p>
                            <hr>
                            <p style="color:#ac4e40 ;"><?php echo $row->alamat ?></p>
                            <p style="color:#ac4e40 ;">Penerima <?php echo $row->penerima ?> (<?php echo $row->nomor_hp ?>)</p>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

</section>