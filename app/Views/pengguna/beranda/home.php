<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php foreach ($tema->getResult() as $row) { ?>
                            <div class="col-md-4">
                                <div class="box box-info box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title"><?= $row->nama_theme ?></h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box-body" style="text-align: center;">
                                        <img alt="image" width="280px" src="<?php echo base_url() ?>/assets/themes/<?= $row->nama_theme ?>/preview.png">
                                        <br><br>
                                        <div class="btn-group">
                                            <p class="mt-2 mr-2"><a href="<?= base_url('order/' . $row->kode_theme) ?>" class="btn btn-success btn-sm">Pesan</a></p>
                                            <p class="mt-2"><a href="<?= base_url('demo/' . $row->nama_theme) ?>" class="btn btn-primary btn-sm">Demo</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
