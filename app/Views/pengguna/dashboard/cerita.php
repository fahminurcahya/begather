<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Cerita
            <small>begather.id</small>
        </h1>
        <ol class="breadcrumb">
            <a href="<?= SITE_UNDANGAN ?>/<?= $order[0]->domain ?>" type="button" class="btn btn-primary">Lihat Web</a>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cerita</h3>
                    </div>
                    <div class="card-body">
                        <form role="form" method="post" action="<?php echo base_url('user/update_cerita'); ?>">
                            <div class="box-body">
                                <div id="konten-cerita">
                                    <?php
                                    $jml_cerita = count($cerita);
                                    for ($i = 0; $i < $jml_cerita; $i++) {
                                    ?>
                                        <div id="cerita<?php echo $i + 1 ?>">

                                            <a style="color: #2c3e50;margin-bottom:0px;font-size: 20px;font-weight: 600;display: flex;">#<?php echo $i + 1 ?></a>
                                            <a id="<?php echo $i + 1 ?>" class="btn btn-sm btn_remove" style="background-color: #dc3545;padding: 5px;font-size: 12px;border-radius: 5px;color:#fff">Hapus</a>
                                            <div class="form-group">
                                                <label for="tanggal_cerita">Tanggal Cerita</label>
                                                <input name="tanggal_cerita[]" type="text" class="form-control" placeholder="Contoh : 20 Februari 2020" value="<?= $cerita[$i]->tanggal_cerita ?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="judul_cerita">Judul Cerita</label>
                                                <input name="judul_cerita[]" type="text" class="form-control" placeholder="Contoh : Ta'aruf" value="<?= $cerita[$i]->judul_cerita  ?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Isi Cerita</label>
                                                <textarea name="isi_cerita[]" type="text" class="form-control" placeholder="Maximal 500 Karakter" maxlength="500" rows="4" required><?= $cerita[$i]->isi_cerita ?></textarea>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <a id="addCerita" class="btn btn-primary " style="color:#fff">Tambah Cerita</a>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary btn-order btn-order-secondary btn-block">Simpan</a>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
    </section>
</div>

<script>
    var i = <?php echo $jml_cerita ?>;

    $(document).on('click', '.btn_remove', function() {

        var button_id = $(this).attr("id");

        $('#cerita' + button_id + '').remove();
        i--;

        if (i == 0) {
            $("..form-control").prop('required', true);
        }

    });

    $('#addCerita').click(function() {
        i++;
        $('#konten-cerita').append('<div id="cerita' + i + '">' +
            '<a style = "color: #2c3e50;margin-bottom:0px;font-size: 20px;font-weight: 600;display: flex;" > # ' + i + ' <div /a>' +
            '<a id="' + i + '" class="btn btn-sm btn_remove" style="background-color: #dc3545;padding: 5px;font-size: 12px;border-radius: 5px;color:#fff">Hapus</a>' +
            '<div class="form-group">' +
            '<label for="tanggal_cerita">Tanggal Cerita</label>' +
            '<input name="tanggal_cerita[]" type="text" class="form-control" placeholder="Contoh : 20 Februari 2020" required>' +
            '</div>' +
            '<div class="form-group">' +
            '<label for="judul_cerita">Judul Cerita</label>' +
            '<input name="judul_cerita[]" type="text" class="form-control" placeholder="Contoh : Taaruf" required>' +
            '</div>' +
            '<div class="form-group">' +
            '<label>Isi Cerita</label>' +
            '<textarea name="isi_cerita[]" type="text" class="form-control" placeholder="Maximal 500 Karakter" maxlength="500" rows="4" required></textarea>' +
            '</div></div>');
        $(".form-control").prop('required', false);
    });
</script>
