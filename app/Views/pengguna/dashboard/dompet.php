<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dompet Digital
            <small>begather.id</small>
        </h1>
        <ol class="breadcrumb">
        </ol>
    </section>
    <div><br></div>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Dompet Digital</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div style="padding-bottom: 10px;">
                            <button id="tambahDompet" type="button" class="btn btn-success" onclick="return tambahDompet()"><i class="fa fa-plus-circle"></i> Tambah</button>

                        </div>
                        <table id="dataTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama Bank</th>
                                    <th>No Rekening</th>
                                    <th>Atas Nama</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($dompet as $row) {
                                ?>
                                    <tr>
                                        <td><?= \esc($row->bank) ?></td>
                                        <td><?= \esc($row->nomor) ?></td>
                                        <td><?= \esc($row->nama) ?></td>
                                        <td>
                                            <button data-id="<?= $row->id ?>" class="btn btn-sm btn-danger hapusDompet" data-toggle="modal" data-target="#modalHapusDompet">Hapus</button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Hadiah</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div style="padding-bottom: 10px;">
                            <button id="tambahHadiah" type="button" class="btn btn-success" onclick="return tambahHadiah()"><i class="fa fa-plus-circle"></i> Tambah</button>

                        </div>
                        <table id="dataTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Alamat</th>
                                    <th>No Hp</th>
                                    <th>Penerima</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($hadiah as $row) {
                                ?>
                                    <tr>
                                        <td><?= \esc($row->alamat) ?></td>
                                        <td><?= \esc($row->nomor_hp) ?></td>
                                        <td><?= \esc($row->penerima) ?></td>
                                        <td>
                                            <button data-id="<?= $row->id ?>" class="btn btn-sm btn-danger hapusHadiah" data-toggle="modal" data-target="#modalHapusHadiah">Hapus</button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<!-- Modal -->
<div class="modal fade" id="addDompet" tabindex="-1" role="dialog" aria-labelledby="addDompetLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Tambah Data
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="create-new-dompet">
                    <div class="form-group">
                        <label for="bank">Nama Bank/Dompet Digital</label>
                        <select class="form-control" id="bank" name="bank">
                            <option value="BCA">BCA</option>
                            <option value="BRI">BRI</option>
                            <option value="BNI">BNI</option>
                            <option value="MANDIRI">MANDIRI</option>
                            <option value="PERMATA">PERMATA</option>
                            <option value="DANA">DANA</option>
                            <option value="LINK AJA">LINK AJA</option>
                            <option value="OVO">OVO</option>
                            <option value="GOPAY">GOPAY</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nomor">Nomor</label>
                        <input type="text" name="nomor" class="form-control" id="nomor" placeholder="Ro Rekening / Hp">
                    </div>
                    <div class="form-group">
                        <label for="nama">Atas Nama</label>
                        <input type="text" name="nama" class="form-control" id="nama" placeholder="Fulan">
                    </div>
                    <button id="submitDompet" class="btn btn-primary" type="submit">
                        Tambah
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addHadiah" tabindex="-1" role="dialog" aria-labelledby="addHadiahLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Tambah Data
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="create-new-hadiah">
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea class="form-control" id="alamat" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="nomor_hp">Nomor Hp</label>
                        <input type="text" name="nomor_hp" class="form-control" id="nomor_hp" placeholder="No Hp">
                    </div>
                    <div class="form-group">
                        <label for="penerima">Penerima</label>
                        <input type="text" name="penerima" class="form-control" id="penerima" placeholder="Fulan">
                    </div>
                    <button id="submitDompet" class="btn btn-primary" type="submit">
                        Tambah
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalHapusDompet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin menghapus data ini ?
                <input type="hidden" name="idDompet" id="idDompet" value="" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm text-danger" id="hapusBtnDompet">Hapus</button>
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalHapusHadiah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin menghapus data ini ?
                <input type="hidden" name="idHadiah" id="idHadiah" value="" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm text-danger" id="hapusBtnHadiah">Hapus</button>
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>


<script>
    function tambahDompet() {
        if (<?= $totalDompet ?> < 2) {
            document.getElementById("create-new-dompet").reset();
            $("#addDompet").modal();
        } else {
            alert('Maksimal 2')
        }
    }

    function tambahHadiah() {
        if (<?= $totalHadiah ?> < 1) {
            document.getElementById("create-new-hadiah").reset();
            $("#addHadiah").modal();
        } else {
            alert('Maksimal 1')
        }
    }


    $("#create-new-dompet").submit(function() {
        let data = $("#create-new-dompet").serialize();
        $.ajax({
            url: "<?= base_url('user/tambah_dompet') ?>",
            method: "POST",
            data: data,
            async: true,
            dataType: 'json',
            success: function($hasil) {
                if (!$hasil.error) {
                    location.reload();
                } else {
                    alert($hasil.message)
                }
            }
        });
        return false
    });

    $("#create-new-hadiah").submit(function() {
        let alamat = $('textarea#alamat').val();
        let nomor_hp = $('#nomor_hp').val();
        let penerima = $('#penerima').val();
        $.ajax({
            url: "<?= base_url('user/tambah_hadiah') ?>",
            method: "POST",
            data: {
                alamat: alamat,
                nomor_hp: nomor_hp,
                penerima: penerima
            },
            async: true,
            dataType: 'json',
            success: function($hasil) {
                if (!$hasil.error) {
                    location.reload();
                } else {
                    alert($hasil.message)
                }
            }
        });
        return false
    });


    $('.hapusDompet').on('click', function(event) {
        var idDompet = $(this).data('id');
        $(".modal-body #idDompet").val(idDompet);
    });
    $('.hapusHadiah').on('click', function(event) {
        var idHadiah = $(this).data('id');
        $(".modal-body #idHadiah").val(idHadiah);
    });

    $('#hapusBtnDompet').on('click', function(event) {
        var idDompet = $('#idDompet').val();
        $.ajax({
            url: "<?= base_url('user/hapus_dompet') ?>",
            method: "POST",
            data: {
                id: idDompet
            },
            async: true,
            dataType: 'html',
            success: function($hasil) {
                if ($hasil == 'sukses') {
                    location.reload();
                } else {
                    alert($hasil)
                }
            }
        });
    });

    $('#hapusBtnHadiah').on('click', function(event) {
        var idHadiah = $('#idHadiah').val();
        $.ajax({
            url: "<?= base_url('user/hapus_hadiah') ?>",
            method: "POST",
            data: {
                id: idHadiah
            },
            async: true,
            dataType: 'html',
            success: function($hasil) {
                if ($hasil == 'sukses') {
                    location.reload();
                } else {
                    alert($hasil)
                }
            }
        });
    });
</script>