<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Mempelai
            <small>begather.id</small>
        </h1>
        <ol class="breadcrumb">
            <a href="<?= SITE_UNDANGAN ?>/<?= $order[0]->domain ?>" type="button" class="btn btn-primary">Lihat Web</a>
        </ol>
    </section>
    <div><br></div>
    <section class="content">
        <?php
        $kunci = $data[0]->kunci;
        $fotogroom = "/assets/users/" . $kunci . "/groom.png";
        $fotobride = "/assets/users/" . $kunci . "/bride.png";
        $fotosampul = "/assets/users/" . $kunci . "/kita.png";
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cover/Sampul</h3>
                    </div>
                    <div class="upload-area-bg" style="text-align: center;">
                        <div class="col">
                            <div class="row">
                                <div class="col-12">
                                    <div class="upload-area" style="height: 100%;padding: 5px 5px;">
                                        <img src="<?= base_url() ?><?= $fotosampul ?>" id="profile-pic-sampul" style='border-radius: 5px;height: 200px;width: 200px;'>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form role="form">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputFile">File input</label>
                                <input type="file" class="file-upload" id="sampul" accept="image/*" id="exampleInputFile">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Mempelai Pria</h3>
                    </div>

                    <div class="upload-area-bg" style="text-align: center;">
                        <div class="col">
                            <div class="row">
                                <div class="col-12">
                                    <div class="upload-area" style="height: 100%;padding: 5px 5px;">
                                        <img src="<?php echo base_url() ?><?= $fotogroom ?>" id="profile-pic-groom" style='border-radius: 5px;height: 200px;width: 200px;'>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form role="form" action="">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="namaLengkapPria">Nama Lengkap</label>
                                <input type="text" class="form-control" id="namaLengkapPria" placeholder="Nama Lengkap" value="<?= $mempelai[0]->nama_pria ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="namaPria">Nama Panggilan</label>
                                <input type="text" class="form-control" id="namaPria" placeholder="Nama Panggilan" value="<?= $mempelai[0]->nama_panggilan_pria ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="namaAyahPria">Nama Ayah</label>
                                <input type="text" class="form-control" id="namaAyahPria" placeholder="Nama Ayah" value="<?= $mempelai[0]->nama_ayah_pria ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="namaIbuPria">Nama Ibu</label>
                                <input type="text" class="form-control" id="namaIbuPria" placeholder="Nama Ibu" value="<?= $mempelai[0]->nama_ibu_pria ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">File input</label>
                                <input type="file" id="groom" class="file-upload" accept="image/*" id="exampleInputFile">
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalPria">Simpan</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Mempelai Wanita</h3>
                    </div>
                    <div class="upload-area-bg" style="text-align: center;">
                        <div class="col">
                            <div class="row">
                                <div class="col-12">
                                    <div class="upload-area" style="height: 100%;padding: 5px 5px;">
                                        <img src="<?php echo base_url() ?><?= $fotobride ?>" id="profile-pic-bride" style='border-radius: 5px;height: 200px;width: 200px;'>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form role="form">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="namaLengkapWanita">Nama Lengkap</label>
                                <input type="text" class="form-control" id="namaLengkapWanita" placeholder="Nama Lengkap" value="<?= $mempelai[0]->nama_wanita ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="namaWanita">Nama Panggilan</label>
                                <input type="text" class="form-control" id="namaWanita" placeholder="Nama Panggilan" value="<?= $mempelai[0]->nama_panggilan_wanita ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="namaAyahWanita">Nama Ayah</label>
                                <input type="text" class="form-control" id="namaAyahWanita" placeholder="Nama Ayah" value="<?= $mempelai[0]->nama_ayah_wanita ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="namaIbuWanita">Nama Ibu</label>
                                <input type="text" class="form-control" id="namaIbuWanita" placeholder="Nama Ibu" value="<?= $mempelai[0]->nama_ibu_wanita ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">File input</label>
                                <input type="file" class="file-upload" id="bride" accept="image/*" id="exampleInputFile">
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalWanita">Simpan</a>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>
</div>
<div class="modal" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Foto Mempelai</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div id="resizer"></div>
                <hr>
                <button class="btn btn-block btn-primary" id="upload">
                    Upload</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalWanita" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin menyimpan perubahan ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="simpanWanita">Ya</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalPria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin menyimpan perubahan ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="simpanPria">Ya</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>/assets/base/js/croppie.min.js"></script>
<script>
    $(document).ready(function() {
        /** croppie shareurcodes.com **/
        var croppie = null;
        var el = document.getElementById('resizer');

        $.base64ImageToBlob = function(str) {
            /** extract content type and base64 payload from original string **/
            var pos = str.indexOf(';base64,');
            var type = str.substring(5, pos);
            var b64 = str.substr(pos + 8);

            /* decode base64 */
            var imageContent = atob(b64);

            /* create an ArrayBuffer and a view (as unsigned 8-bit) */
            var buffer = new ArrayBuffer(imageContent.length);
            var view = new Uint8Array(buffer);

            /* fill the view, using the decoded base64 */
            for (var n = 0; n < imageContent.length; n++) {
                view[n] = imageContent.charCodeAt(n);
            }

            /* convert ArrayBuffer to Blob */
            var blob = new Blob([buffer], {
                type: type
            });

            return blob;
        }

        $.getImage = function(input, croppie) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    croppie.bind({
                        url: e.target.result,
                    });
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        var fotonyasiapa = '';
        $(".file-upload").on("change", function(event) {
            $("#myModal").modal();
            fotonyasiapa = $(this).attr("id");
            console.log("foto_" + fotonyasiapa);
            /* Initailize croppie instance and assign it to global variable */
            croppie = new Croppie(el, {
                viewport: {
                    width: 200,
                    height: 200,
                    type: 'square'
                },
                boundary: {
                    width: 250,
                    height: 250
                },
                enableOrientation: true
            });
            $.getImage(event.target, croppie);
        });

        $("#upload").on("click", function() {
            croppie.result('base64').then(function(base64) {
                $("#myModal").modal("hide");
                $("#profile-pic").attr("src", "/images/ajax-loader.gif");

                var url = "<?php echo base_url('user/update_foto_mempelai') ?>";
                var formData = new FormData();
                formData.append("foto_" + fotonyasiapa, $.base64ImageToBlob(base64));
                formData.append("kunci", "<?= $kunci ?>");
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        console.log(data);
                        if (data == "uploadedbride") {
                            $("#profile-pic-bride").attr("src", base64);
                        } else if (data == "uploadedgroom") {
                            $("#profile-pic-groom").attr("src", base64);
                        } else if (data == "uploadedsampul") {
                            $("#profile-pic-sampul").attr("src", base64);
                        } else {
                            $("#profile-pic").attr("src", "/images/icon-cam.png");
                            console.log(data['profile_picture']);
                        }
                    },
                    error: function(error) {
                        console.log(error);
                        $("#profile-pic").attr("src", "/images/icon-cam.png");
                    }
                });
            });
        });

        /* To Rotate Image Left or Right */
        $(".rotate").on("click", function() {
            croppie.rotate(parseInt($(this).data('deg')));
        });

        $('#myModal').on('hidden.bs.modal', function(e) {
            /* This function will call immediately after model close */
            /* To ensure that old croppie instance is destroyed on every model close */
            setTimeout(function() {
                croppie.destroy();
            }, 100);
        });
    });

    $('#simpanWanita').on('click', function(event) {
        var datanyaSiapa = 'wanita';
        var nama = $('#namaLengkapWanita').val();
        var nama_panggilan = $('#namaWanita').val();
        var nama_ayah = $('#namaAyahWanita').val();
        var nama_ibu = $('#namaIbuWanita').val();
        console.log(nama);
        $.ajax({
            url: "<?= base_url('user/update_mempelai') ?>",
            method: "POST",
            data: {
                nama: nama,
                nama_panggilan: nama_panggilan,
                nama_ayah: nama_ayah,
                nama_ibu: nama_ibu,
                datanyaSiapa: datanyaSiapa
            },
            async: true,
            dataType: 'html',
            success: function($hasil) {
                if ($hasil == 'sukses') {
                    location.reload();
                }
            }
        });

    });

    $('#simpanPria').on('click', function(event) {

        var datanyaSiapa = 'pria';
        var nama = $('#namaLengkapPria').val();
        var nama_panggilan = $('#namaPria').val();
        var nama_ayah = $('#namaAyahPria').val();
        var nama_ibu = $('#namaIbuPria').val();

        $.ajax({
            url: "<?= base_url('user/update_mempelai') ?>",
            method: "POST",
            data: {
                nama: nama,
                nama_panggilan: nama_panggilan,
                nama_ayah: nama_ayah,
                nama_ibu: nama_ibu,
                datanyaSiapa: datanyaSiapa
            },
            async: true,
            dataType: 'html',
            success: function($hasil) {
                if ($hasil == 'sukses') {
                    location.reload();
                }
            }
        });

    });
</script>
