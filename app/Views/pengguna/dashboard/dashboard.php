<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard
            <small>begather.id</small>
        </h1>
        <ol class="breadcrumb">
            <a href="<?= SITE_UNDANGAN ?>/<?= $order[0]->domain ?>" type="button" class="btn btn-primary">Lihat Web</a>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php
                $durasi = '+1 days';
                $tglDaftar = $pembayaran[0]->created_at;
                $tglExp = strtotime($durasi, strtotime($tglDaftar));
                $tglExpFormated = date("d-m-Y H:i A", $tglExp);

                ?>
                <?php if ($pembayaran[0]->status == "2") : ?>
                    <div class="callout callout-success">
                        Pembayaran anda telah kami terima
                    </div>
                <?php elseif ($pembayaran[0]->status == "1") : ?>
                    <div class="callout callout-info">
                        Pembayaran Akan Kami Cek Secara Manual
                    </div>
                <?php else : ?>
                    <div class="callout callout-warning">
                        Selesaikan Pembayaran Anda Sebelum <?= $tglExpFormated ?>
                    </div>
                <?php endif ?>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Invoice</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <b>Invoice #007612</b><br>
                        <div class="col-12 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Undangan Digital</td>
                                        <td><?= rupiah($pembayaran[0]->total) ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->



                        <br><br>


                        <!-- this row will not appear when printing -->
                        <?php if ($pembayaran[0]->status != "2") : ?>
                            <div class="no-print">
                                <div class="col-12">
                                    <button type="button" data-toggle="modal" data-target="#modalKonfirmasi" class="btn btn-success float-right"><i class="fa fa-credit-card"></i> Upload bukti pembayaran
                                    </button>
                                    <button type="button" data-toggle="modal" data-target="#metodePembayaran" class="btn btn-info float-right"><i class="fa fa-credit-card"></i> Metode Pembayaran
                                    </button>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="metodePembayaran" tabindex="-1" role="dialog" aria-labelledby="metodePembayaranLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="metodePembayaranLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php if ($pembayaran[0]->total != "0") : ?>
                    <div class="col-12">
                        <p class="lead">Payment Methods:</p>
                        <img src="<?= base_url('assets/adminlte/dist/img/credit/visa.png') ?>" alt="Visa">
                        <img src="<?= base_url('assets/adminlte/dist/img/credit/mastercard.png') ?>" alt="Mastercard">
                        <img src="<?= base_url('assets/adminlte/dist/img/credit/american-express.png') ?>" alt="American Express">
                        <img src="<?= base_url('assets/adminlte/dist/img/credit/paypal2.png') ?>" alt="Paypal">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Metode</th>
                                    <th>Atas Nama</th>
                                    <th>Nomor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Bank BRI</td>
                                    <td>Fahmi Nurcahya</td>
                                    <td></td>

                                </tr>
                                <tr>
                                    <td>Bank BCA</td>
                                    <td>Fahmi Nurcahya</td>
                                    <td></td>


                                </tr>
                                <tr>
                                    <td>OVO</td>
                                    <td>Fahmi Nurcahya</td>
                                    <td></td>

                                </tr>
                                <tr>
                                    <td>DANA</td>
                                    <td>Fahmi Nurcahya</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <?php else : ?>
                    <div class="col-12">
                        <p class="lead">Payment Methods:</p>
                        <img src="<?= base_url('assets/adminlte/dist/img/credit/visa.png') ?>" alt="Visa">
                        <img src="<?= base_url('assets/adminlte/dist/img/credit/mastercard.png') ?>" alt="Mastercard">
                        <img src="<?= base_url('assets/adminlte/dist/img/credit/american-express.png') ?>" alt="American Express">
                        <img src="<?= base_url('assets/adminlte/dist/img/credit/paypal2.png') ?>" alt="Paypal">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Metode</th>
                                    <th>Atas Nama</th>
                                    <th>Nomor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Konfirmasi Whatsapp</td>
                                    <td>Admin 1</td>
                                    <td>085718456591</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <?php endif ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="modalKonfirmasi" tabindex="-1" role="dialog" aria-labelledby="modalKonfirmasiLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalKonfirmasiLabel">Konfirmasi Pembayaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('user/konfirmasi'); ?>">
                <div class="modal-body">
                    <div class="col mt-2">
                        <label>Nama Lengkap</label>
                        <input name="nama_lengkap" type="text" class="form-control" placeholder="Contoh : Nadya" value="" required>
                    </div>
                    <div class="col mt-2">
                        <label>Nama Bank</label>
                        <input name="nama_bank" type="text" class="form-control" placeholder="Contoh : BRI " value="" required>
                    </div>
                    <div class="col mt-2 mb-2">
                        <label>Bukti Transfer (max 2MB)</label>
                        <input type="file" id="bukti" name="bukti">
                    </div>
                    <input type="hidden" value="<?= $pembayaran[0]->invoice ?>" name="invoice">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary" id="simpanKonfimasi">Konfirmasi</button>
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>