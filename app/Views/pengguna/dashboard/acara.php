<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Acara
            <small>begather.id</small>
        </h1>
        <ol class="breadcrumb">
            <a href="<?= SITE_UNDANGAN ?>/<?= $order[0]->domain ?>" type="button" class="btn btn-primary">Lihat Web</a>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Akad Nikah</h3>
                    </div>
                    <form role="form" action="">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Tanggal:</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker">
                                    <input type="hidden" id="tanggal_akad" value="<?= $acara[0]->tanggal_akad ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="waktu_akad">Waktu/Jam</label>
                                <input type="text" class="form-control" id="waktu_akad" placeholder="Contoh : 08.00 Pagi" value="<?= $acara[0]->jam_akad ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="lokasi_akad">Tempat/Lokasi</label>
                                <input type="text" class="form-control" id="lokasi_akad" placeholder="Contoh : Kediaman Mempelai Wanita" value="<?= $acara[0]->tempat_akad ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="alamat_akad">Alamat</label>
                                <input type="text" class="form-control" id="alamat_akad" placeholder="Contoh : JL. Ahmad Yani No.1" value="<?= $acara[0]->alamat_akad ?>" required>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAkad">Simpan</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Resepsi Nikah</h3>
                    </div>
                    <form role="form" action="">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Tanggal:</label>

                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="datepicker2">
                                    <input type="hidden" id="tanggal_resepsi" value="<?= $acara[0]->tanggal_resepsi ?>">
                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group">
                                <label for="waktu_resepsi">Waktu/Jam</label>
                                <input type="text" class="form-control" id="waktu_resepsi" placeholder="Contoh : 08.00 Pagi" value="<?= $acara[0]->jam_resepsi ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="lokasi_resepsi">Tempat/Lokasi</label>
                                <input type="text" class="form-control" id="lokasi_resepsi" placeholder="Contoh : Kediaman Mempelai Wanita" value="<?= $acara[0]->tempat_resepsi ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="alamat_resepsi">Alamat</label>
                                <input type="text" class="form-control" id="alamat_resepsi" placeholder="Contoh : JL. Ahmad Yani No.1" value="<?= $acara[0]->alamat_resepsi ?>" required>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalResepsi">Simpan</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Map</h3>
                    </div>
                    <form role="form" action="">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Google Map Link</label>
                                <textarea id="maps" class="form-control" rows="5" placeholder="Enter ..." required><?= $data[0]->maps ?></textarea>
                            </div>
                            <a href="<?php echo base_url('maps'); ?>" style="margin-top: 105px;color: #2c3e50;position: relative;top:3px;color:#17a2b8;"><i class="lni-question-circle" style="color:#17a2b8;"></i>&nbsp Cara Menambahkan Maps</a>

                        </div>
                        <div class="box-footer">
                            <a href="" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalMaps">Simpan</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
</div>

<!-- Modal -->
<div class="modal fade" id="modalAkad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin menyimpan perubahan ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-primary" id="simpanAkad">Ya</button>
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalResepsi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin menyimpan perubahan ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-primary" id="simpanResepsi">Ya</button>
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalMaps" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin menyimpan perubahan ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="simpanMaps">Ya</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>/assets/base/js/pikaday.js"></script>

<script>
    $(document).ready(function() {
        moment.locale('id');
        $('#datepicker').val(moment('<?= $acara[0]->tanggal_akad ?>').format('dddd, Do MMMM YYYY'));
        var picker = new Pikaday({

            field: $('#datepicker')[0],
            format: 'dddd, Do MMMM YYYY',
            onSelect: function() {
                $('#tanggal_akad').val(this.getMoment().format('YYYY/MM/DD'));
            }
        });

        $('#datepicker2').val(moment('<?= $acara[0]->tanggal_resepsi ?>').format('dddd, Do MMMM YYYY'));
        var picker = new Pikaday({
            format: 'dddd, Do MMMM YYYY',
            field: $('#datepicker2')[0],
            onSelect: function() {
                $('#tanggal_resepsi').val(this.getMoment().format('YYYY/MM/DD'));
            }
        });
    });

    $('#simpanAkad').on('click', function(event) {
        var datanyaSiapa = 'akad';
        var tanggal = $('#tanggal_akad').val();
        var waktu = $('#waktu_akad').val();
        var lokasi = $('#lokasi_akad').val();
        var alamat = $('#alamat_akad').val();
        $.ajax({
            url: "<?= base_url('user/update_acara') ?>",
            method: "POST",
            data: {
                tanggal: tanggal,
                waktu: waktu,
                lokasi: lokasi,
                alamat: alamat,
                datanyaSiapa: datanyaSiapa
            },
            async: true,
            dataType: 'html',
            success: function($hasil) {
                if ($hasil == 'sukses') {
                    location.reload();
                } else {
                    alert('Data gagal di update')
                }
            }
        });

    });

    $('#simpanResepsi').on('click', function(event) {

        var datanyaSiapa = 'resepsi';
        var tanggal = $('#tanggal_resepsi').val();
        var waktu = $('#waktu_resepsi').val();
        var lokasi = $('#lokasi_resepsi').val();
        var alamat = $('#alamat_resepsi').val();

        $.ajax({
            url: "<?= base_url('user/update_acara') ?>",
            method: "POST",
            data: {
                tanggal: tanggal,
                waktu: waktu,
                lokasi: lokasi,
                alamat: alamat,
                datanyaSiapa: datanyaSiapa
            },
            async: true,
            dataType: 'html',
            success: function($hasil) {
                if ($hasil == 'sukses') {
                    location.reload();
                } else {
                    alert('Data gagal di update')
                }
            }
        });

    });

    $('#simpanMaps').on('click', function(event) {
        var maps = $('#maps').val();
        $.ajax({
            url: "<?= base_url('user/update_maps') ?>",
            method: "POST",
            data: {
                maps: maps
            },
            async: true,
            dataType: 'html',
            success: function($hasil) {
                if ($hasil == 'sukses') {
                    location.reload();
                } else {
                    alert('Data gagal di update')
                }
            }
        });

    });
</script>
