<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Tamu Undangan
            <small>begather.id</small>
        </h1>
        <ol class="breadcrumb">
        </ol>
    </section>
    <div><br></div>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php
                $session = session();
                $errors = $session->getFlashdata('errors');
                if ($errors != null) : ?>
                    <div class="alert alert-danger" role="alert" id="ikierror">
                        <span class="mb-0">
                            <strong>Error!<strong>
                                    <?php
                                    foreach ($errors as $err) {
                                        echo $err;
                                    }
                                    ?>
                        </span>
                    </div>
                <?php endif ?>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Tamu Undangan</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div style="padding-bottom: 10px;">
                            <button id="tambahTamu" type="button" class="btn btn-success" onclick="return tambahTamu()"><i class="fa fa-plus-circle"></i> Tambah</button>
                            <button id="DownloadTplTamu" type="button" class="btn btn-success"><i class="fa fa-download "></i> Download template</button>
                            <button id="uploadTamu" type="button" class="btn btn-success" onclick="return uploadTamu()"><i class="fa fa-upload"></i> Upload Tamu</button>
                            <button id="uploadTamu" type="button" class="btn btn-success" onclick="return kirimGenerate()"><i class="fa fa-paper-plane"></i> Kirim Generate Link</button>
                        </div>
                        <table id="dataTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama Tamu</th>
                                    <th>Url Tamu</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($tamu as $row) {
                                ?>
                                    <tr>
                                        <td><?= \esc($row->nama_tamu) ?></td>
                                        <td><?= \esc($row->url_tamu) ?></td>
                                        <td>
                                            <button data-id="<?= $row->id ?>" class="btn btn-sm btn-danger hapus" data-toggle="modal" data-target="#modalHapus">Hapus</button>
                                            <button data-id="<?= $row->id ?>" class="btn btn-sm btn-info hapus" onclick="return kirim('<?= $row->url_tamu ?>')">Kirim</button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<!-- Modal -->
<div class="modal fade" id="addTamu" tabindex="-1" role="dialog" aria-labelledby="addTamuLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="create-new-tamu">
                    <div class="form-group">
                        <label for="bank">Nama Tamu</label>
                        <input type="text" name="namatamu" class="form-control" id="namatamu" placeholder="Nama Tamu">
                    </div>
                    <button id="submitTamu" class="btn btn-primary" type="submit">
                        Tambah
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="uplTamu" tabindex="-1" role="dialog" aria-labelledby="addTamuLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="create-upload-tamu" method="post" action="/user/upload_tamu" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputFile">File Upload</label>
                        <input type="file" class="file-upload" id="upload_tamu" name="upload_tamu" accept=".xls, .xlsx">
                    </div>
                    <button id="submitUplTamu" class="btn btn-primary" type="submit">
                        Upload
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalHapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin menghapus tamu ini ?
                <input type="hidden" name="idTamu" id="idTamu" value="" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm text-danger" id="hapusBtn">Hapus</button>
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script>
    function tambahTamu() {
        document.getElementById("create-new-tamu").reset();
        $("#addTamu").modal();
    }

    function uploadTamu() {
        document.getElementById("create-upload-tamu").reset();
        $("#uplTamu").modal();
    }

    function kirim($url) {
        urll = $url;
        url = urll.replaceAll("+", "%2B");
        console.log(url);
        message = 'Assalamu%27alaikum+Warahmatullahi+Wabarakatuh.%0D%0A%0D%0AMaha+suci+Allah+yang+telah+menjadikan+segala+sesuatu+lebih+indah+dan+sempurna.%0D%0A%0D%0AIzinkan+kami+mengundang+Bapak%2FIbu%2FSahabat+sekalian+untuk+dapat+menghadiri+acara+pernikahan+kami.%0D%0A%0D%0ALink+undangan+%3A%0D%0A%0D%0A' + url + '%0D%0A%0D%0AKehadiran%2C+doa+dan+restu+anda+semua+adalah+kado+terindah+bagi+kami.+Tiada+yang+dapat+kami+ungkapkan+selain+rasa+terima+kasih+dari+hati+yang+tulus+dan+dalam.%0D%0A%0D%0A%0D%0ANB%3A+karena+masih+dalam+masa+pandemi+covid-19+dan+tanpa+mengurangi+rasa+hormat%2C+undangan+ini+kami+sebar+melalui+pesan+digital.%0D%0ATetap+WAJIB+menerapkan+Protokol+Kesehatan%0D%0AMenggunakan+Masker+dan+Handsanitizer+saat+acara+berlangsung.+%0D%0A%0D%0ATerima+Kasih.'
        window.open('https://api.whatsapp.com/send?text=' + message);
    }

    $("#create-new-tamu").submit(function() {
        let data = $("#create-new-tamu").serialize();
        $.ajax({
            url: "<?= base_url('user/tambah_tamu') ?>",
            method: "POST",
            data: data,
            async: true,
            dataType: 'json',
            success: function($hasil) {
                if (!$hasil.error) {
                    location.reload();
                } else {
                    alert($hasil.message)
                }
            }
        });
        return false
    });


    $('.hapus').on('click', function(event) {
        var idTamu = $(this).data('id');
        $(".modal-body #idTamu").val(idTamu);
    });

    $('#hapusBtn').on('click', function(event) {
        var idTamu = $('#idTamu').val();
        $.ajax({
            url: "<?= base_url('user/hapus_tamu') ?>",
            method: "POST",
            data: {
                id: idTamu
            },
            async: true,
            dataType: 'html',
            success: function($hasil) {
                if ($hasil == 'sukses') {
                    location.reload();
                } else {
                    alert($hasil)
                }
            }
        });
    });

    $('#DownloadTplTamu').on('click', function(event) {
        $.ajax({
            url: "<?= base_url('user/download_tpl_tamu') ?>",
            method: "POST",
            async: true,
            dataType: 'html',
            success: function($hasil) {
                window.open($hasil, "_blank");
            }
        });
    });

    function kirimGenerate() {
        message = ''
        $.ajax({
            url: "<?= base_url('user/get_tamu') ?>",
            method: "POST",
            async: true,
            dataType: 'json',
            success: function($hasil) {
                if (!$hasil.error) {
                    generate($hasil['data'], $hasil['domain'])
                } else {
                    alert('Error')
                }
            }
        });

    }

    function generate(data, domain) {
        message = 'Klick%20Link%20Untuk%20Generate%20Undangan%0A%0A';
        messagebody = ''
        for (i = 0; i < data.length; i++) {
            messagebody = messagebody + data[i].nama_tamu + '%20%3D%3E%20begather.id%2Fsend%2F%3Fdomain%3D' + domain + '%26' + data[i].url_tamu.substring(data[i].url_tamu.indexOf('?') + 1) +
                '%0A'
        }
        message = message + messagebody;
        message = message.replaceAll("+", "%2B");
        console.log(message)
        window.open('https://api.whatsapp.com/send?text=' + message);
    }
</script>
