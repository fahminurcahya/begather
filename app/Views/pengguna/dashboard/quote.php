<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Kata Mutiara
            <small>begather.id</small>
        </h1>
        <ol class="breadcrumb">
            <a href="<?= SITE_UNDANGAN ?>/<?= $order[0]->domain ?>" type="button" class="btn btn-primary">Lihat Web</a>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Kata Mutiara</h3>
                    </div>
                    <form role="form" action="">
                        <div class="box-body">
                            <div class="form-group">
                                <textarea id="quote" class="form-control" rows="5" placeholder="Enter ..." required><?= $data[0]->quote ?></textarea>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalQuote">Simpan</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
</div>

<!-- Modal -->
<div class="modal fade" id="modalAkad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin menyimpan perubahan ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-primary" id="simpanAkad">Ya</button>
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalResepsi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin menyimpan perubahan ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-primary" id="simpanResepsi">Ya</button>
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalQuote" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin menyimpan perubahan ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="simpanQuote">Ya</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>/assets/base/js/pikaday.js"></script>

<script>
    $('#simpanQuote').on('click', function(event) {
        var quote = $('#quote').val();
        $.ajax({
            url: "<?= base_url('user/update_quote') ?>",
            method: "POST",
            data: {
                quote: quote
            },
            async: true,
            dataType: 'html',
            success: function($hasil) {
                if ($hasil == 'sukses') {
                    location.reload();
                } else {
                    alert('Data gagal di update')
                }
            }
        });

    });
</script>
