<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tampilan
            <small>begather.id</small>
        </h1>
        <ol class="breadcrumb">
            <a href="<?= SITE_UNDANGAN ?>/<?= $order[0]->domain ?>" type="button" class="btn btn-primary">Lihat Web</a>
        </ol>
    </section>
    <div><br></div>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Pengguna</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <div class="box box-success box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?= $order[0]->nama_theme ?></h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div style="text-align: center; opacity:60%;" class="box-body">
                                    <img alt="image" width="280px" src="<?php echo base_url() ?>/assets/themes/<?= $order[0]->nama_theme ?>/preview.png">
                                    <br><br>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success" disabled>Active</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php foreach ($tema->getResult() as $row) {
                            if ($row->nama_theme == $order[0]->nama_theme) continue; ?>
                            <div class="col-md-4">
                                <div class="box box-info box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title"><?= $row->nama_theme ?></h3>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box-body" style="text-align: center;">
                                        <img alt="image" width="280px" src="<?php echo base_url() ?>/assets/themes/<?= $row->nama_theme ?>/preview.png">
                                        <br><br>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-success" onclick="ubahTema(<?= $row->id ?>)">Pilih</button>
                                            <a href="<?= SITE_UTAMA . '/demo/' . $row->nama_theme ?>" class="button btn btn-info">Demo</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalTema" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin mengubah tema ?
                <input type="hidden" name="idTema" id="idTema" value="" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-primary" id="pilihBtn" onclick="pilihBtn()">Ya</button>
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<script>
    function ubahTema($id) {
        $("#modalTema").modal();
        $("#idTema").val($id);
    }

    function pilihBtn() {
        var idtema = $('#idTema').val();
        $.ajax({
            url: "<?= base_url('user/ganti_tema') ?>",
            method: "POST",
            data: {
                id: idtema
            },
            async: true,
            dataType: 'html',
            success: function($hasil) {
                if ($hasil == 'sukses') {
                    location.reload();
                } else {
                    alert('Gagal Mengganti Tema')
                    location.reload();
                }
            }
        });
    }
</script>
