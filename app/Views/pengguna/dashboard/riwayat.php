<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Riwayat Pengunjung
            <small>begather.id</small>
        </h1>
        <ol class="breadcrumb">
        </ol>
    </section>
    <div><br></div>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-6 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3><?php if ($total_pengunjung_today == '') echo '0';
                                    else echo $total_pengunjung_today  ?></h3>

                                <p>Total Pengunjung Hari Ini</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-6 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3><?= $total_pengunjung ?></h3>

                                <p>Total Pengunjing</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pengunjung 7 hari terakhir</h3>
                    </div>
                    <div class="box-body">
                        <div class="chart-area">
                            <canvas id="myAreaChart" style="height:300px"></canvas>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Pengguna</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="dataTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Nama</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($pengunjung as $row) {
                                ?>
                                    <tr>
                                        <td><?= date("d M Y", strtotime($row->created_at)) ?></td>
                                        <td><?= $row->nama_pengunjung ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>



<script>
    var jumlah = [];
    var tanggal = [];
    moment.locale('id');
    var namaBulan = moment().format('MMMM');

    <?php foreach ($total_mingguan as $row) { ?>
        jumlah.push(<?= $row->jumlah ?>);
        tanggal.push(<?= $row->tanggal ?> + ' ' + namaBulan);
    <?php } ?>
</script>
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable({
            "ordering": false
        }); // ID From dataTable
        $('#dataTableHover').DataTable(); // ID From dataTable with Hover
    });
</script>
<script src="<?= base_url('assets/chart/chart-area.js') ?>"></script>
