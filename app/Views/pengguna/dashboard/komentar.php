<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Komentar/ucapan
            <small>begather.id</small>
        </h1>
        <ol class="breadcrumb">
        </ol>
    </section>
    <div><br></div>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-3 col-xs-3">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3><?= $total_komentar_today ?></h3>

                                <p>Total Ucapan Hari Ini</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-3">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3><?= $total_komentar ?></h3>

                                <p>Total Ucapan</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-3">
                        <!-- small box -->
                        <div class="small-box bg-blue">
                            <div class="inner">
                                <h3><?= $total_hadir ?></h3>
                                <p>Terkonfirmasi Hadir</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-3">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3><?= $total_tidak_hadir ?></h3>
                                <p>Terkonfirmasi Tidak Hadir/Ragu</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                </div>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Pengguna</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="dataTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Ucapan</th>
                                    <th>Kehadiran</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($komentar as $row) {
                                ?>
                                    <tr>
                                        <td><?= \esc($row->nama_komentar) ?></td>
                                        <td><?= \esc($row->isi_komentar) ?></td>
                                        <td>
                                            <?php if ($row->kehadiran == 0) : ?>
                                                Tidak Hadir
                                            <?php elseif ($row->kehadiran == 1) : ?>
                                                Hadir
                                            <?php else : ?>
                                                Ragu-Ragu
                                            <?php endif ?>
                                        </td>
                                        <td>
                                            <button data-id="<?= $row->id ?>" class="btn btn-sm btn-danger hapus" data-toggle="modal" data-target="#modalHapus">Hapus</button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<!-- Modal -->
<div class="modal fade" id="modalHapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin menghapus komentar ini ?
                <input type="hidden" name="idKomentar" id="idKomentar" value="" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm text-danger" id="hapusBtn">Hapus</button>
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('.hapus').on('click', function(event) {
        var idKomentar = $(this).data('id');
        $(".modal-body #idKomentar").val(idKomentar);
    });

    $('#hapusBtn').on('click', function(event) {
        var idkomentar = $('#idKomentar').val();
        $.ajax({
            url: "<?= base_url('user/hapus_komentar') ?>",
            method: "POST",
            data: {
                id: idkomentar
            },
            async: true,
            dataType: 'html',
            success: function($hasil) {
                if ($hasil == 'sukses') {
                    location.reload();
                } else {
                    alert($hasil)
                }
            }
        });
    });
</script>
